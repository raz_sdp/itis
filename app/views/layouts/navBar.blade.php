<!-- no script starting
     execute only when javascript disabled -->
<noscript>
  <style type="text/css">
    #main-content { display:none; }
  </style>  
  <a href="{{ URL::to('/showallcourses') }}">ITIS-DB</a>
  <br/>
  <a href="{{ URL::to('/showallcourses') }}">{{ trans('localization.Courses') }}</a>
  <br/>
  <a href="{{ URL::to('/help') }}">{{ trans('localization.Help') }}</a>
  <br/>
  <a href="{{ URL::to('/studyplan') }}">{{ trans('localization.Study Plan') }}</a>
  <br/>
  @if(Session::get(Auth::id()) <= Roole::getRoleId('Assistant'))
    <a href="{{ URL::to('/showallmodules') }}">{{ trans('localization.Modules') }}</a>
  @endif
  <br/>
  @if(Session::get(Auth::id()) >  Roole::getRoleId('Admin'))
    <a href="{{ URL::to('/showallcontacts') }}">{{trans('localization.Contacts')}}</a>
  <br/>
  @ielsef(Session::get(Auth::id()) <= Roole::getRoleId('Admin'))
    <a href="{{ URL::to('/showallusers') }}">{{ trans('localization.Users') }}</a>
    <br/>
    <a href="{{ URL::to('/showlogs') }}">{{ trans('localization.Logs') }}</a>
  @endif
  <br/>
  @if(sizeof(Roole::getRooles()) > 2)
    {{ Form::open(array('url' => 'viewAs', 'method' => 'post', 'class' => 'form-inline')) }}
        {{ Form::select('viewAs', Roole::getRooles(),
                         null, array('class' =>'form-control input-sm')) }}
      <button type="submit" class="btn btn-default btn-sm">
        <span class = "glyphicon glyphicon-refresh"></span>&nbsp;
      </button>     
      {{ Form::close() }}
  @endif
  <br/>
  {{--*/ $editUrl = "/edituser/".Auth::id() /*--}}
  <a href="{{ URL::to($editUrl) }}">                
    {{ trans('localization.Edit Your Profile') }}
  </a>
  <br/>
  <a href="{{ URL::to('/logout') }}">                
    {{ trans('localization.Logout') }}
  </a>
  <a href="{{ URL::to('/changeViewMode') }}">                
    <!-- {{ trans('localization.Logout') }} -->
   Change View Mode
  </a>
  <br/>  
</noscript>
<!-- no script ending -->
<nav id="myNavbar" class="navbar navbar-default" role="navigation">
  <div class="container" id="main-content">

    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbarCollapse">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="{{ URL::to('/showallcourses') }}">&nbsp;ITIS-DB</a>
    </div>

    <div class="collapse navbar-collapse" id="navbarCollapse">
      <ul class="nav navbar-nav">

        <li class="lihover">
          <a href="{{ URL::to('/showallcourses')}}" style="color: #1C1B19">
            <span class="glyphicon glyphicon-book"></span>
            {{ trans('localization.Courses') }}
          </a>
        </li>

        @if(Session::get(Auth::id()) <= Roole::getRoleId('Student'))
          <li class="lihover">
            <a href="{{ URL::to('/studyplan') }}" style="color: #1C1B19">
              <span class="glyphicon glyphicon-road"></span>
              {{ trans('localization.Study Plan') }}
            </a>
          </li>
        @endif

        @if(Session::get(Auth::id()) <= Roole::getRoleId('Assistant'))
          <li class="lihover">
            <a href="{{ URL::to('/showallmodules') }}" style="color: #1C1B19">
              <span class="glyphicon glyphicon-film"></span>
              {{ trans('localization.Modules') }}
            </a>
          </li>        
        @endif

        @if(Session::get(Auth::id()) > Roole::getRoleId('Admin'))
          <li class="lihover">
            <a href="{{ URL::to('/showallcontacts') }}" style="color: #1C1B19">
              <span class="glyphicon glyphicon-user"></span>
              {{ trans('localization.Contacts') }}
            </a>
          </li>        
        @endif

        @if(Session::get(Auth::id()) <= Roole::getRoleId('Admin'))
          <li class="lihover">
            <a href="{{ URL::to('/showallusers') }}" style="color: #1C1B19">
              <span class="glyphicon glyphicon-user"></span>
              {{ trans('localization.Users') }}
            </a>
          </li>

          <li class="lihover">
            <a href="{{ URL::to('/showlogs') }}" style="color: #1C1B19">
              <span class="glyphicon glyphicon-time"></span>
              {{ trans('localization.Logs') }}
            </a>
          </li>          
        @endif

        <li class="lihover">
          <a href="{{ URL::to('/help') }}" style="color: #1C1B19">
            <span class="glyphicon glyphicon-flag"></span>
            {{ trans('localization.Help') }}
          </a>
        </li> 

        <li class="lihover">
          <a href="{{ URL::to('/changeViewMode') }}" style="color: #1C1B19">
          @if (HelperActions::getViewMode())
            <span class="glyphicon glyphicon-th"></span>
          @else
            <span class="glyphicon glyphicon-th-list"></span>
          @endif          
          </a>
        </li>          

        @if(sizeof(Roole::getRooles()) > 2)
          <li style = "padding-top: 9px;padding-left: 15px;padding-right: 15px;" class="lihover"> 
            {{ Form::open(array('url' => 'viewAs', 'method' => 'post', 'class' => 'form-inline')) }}
              <div class="form-group">
                {{ Form::select('viewAs', Roole::getRooles(),
                                 null, array('class' =>'form-control input-sm')) }}
              </div>
              <button type="submit" class="btn btn-default btn-sm">
                <span class = "glyphicon glyphicon-refresh"></span>&nbsp;
              </button>     
            {{ Form::close() }}
          </li>
        @endif

        <li class="lihover">
          <a href="{{ URL::to('/changeLanguage/') }}" style="color: #1C1B19">
            {{--*/ $icon = 'twiter_bootstrap/icon/'.User::getUserLanguageWithInvertion("languageOf".Auth::id()).'.png' /*--}}
            {{ HTML::image($icon) }}
            <!-- {{User::getUserLanguageWithInvertion("languageOf".Auth::id())}} -->
          </a>
        </li>          
      </ul>

      <ul class="nav navbar-nav navbar-right">
        <li class="dropdown lihover">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"  style="color: #1C1B19">
            {{ User::userMitName(Auth::id()); }}
            <span class="caret"></span>
          </a>
          <ul class="dropdown-menu" role="menu">
            <li>
              {{--*/ $editUrl = "/edituser/".Auth::id()."/0" /*--}}
              <a href="{{ URL::to($editUrl) }}">
                <span class="glyphicon glyphicon-edit"></span>
                &nbsp;{{ trans('localization.Edit Your Profile') }}
              </a>
            </li>
            <li class="divider"></li>
            <li>
              <a href="{{ URL::to('/logout') }}">
                <span class="glyphicon glyphicon-off"></span>
                &nbsp;{{ trans('localization.Logout') }}
              </a>
            </li>
          </ul>
        </li>
      </ul>
    </div>
  </div>
</nav>