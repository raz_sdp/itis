@extends('layouts.main')

@section('content')
@include('layouts.navBar')
<div class = "col-sm-3"></div>
<div class = "col-sm-6 alert alert-danger" role="alert">
	<h3>Request Denied :(</h3>
</div>
<div class = "col-sm-3"></div>
@stop