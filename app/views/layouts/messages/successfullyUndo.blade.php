@extends('layouts.main')

@section('content')
@include('layouts.navBar')
<div class = "col-sm-3"></div>
<div class = "col-sm-6 alert alert-success" role="alert">
	<h3>Successfully Undo your Changes :)</h3>
</div>
<div class = "col-sm-3"></div>
@stop