@extends('layouts.main')

@section('content')
@include('layouts.navBar')
<div class = "col-sm-2"> </div>
<div class = "col-sm-8" style = "padding-top: 150px;">
 	<div class="well">
 			<h3>
 				Upload CSV 
 				<span class="glyphicon glyphicon-cloud-upload"></span>
 			</h3>
    	{{ Form::open(array('url'=>'/upload','method'=>'POST', 'files'=>true)) }}

    	<div style = "padding-top: 15px;">
				<table class="table">
					<tr>
						<td>
	        		{{ Form::file('toImport', array('class'=>'btn btn-lg btn-primary')) }}
						</td>
						<td>
			      	{{ Form::submit('Import', array('class'=>'btn btn-lg btn-success')) }}
					  	<p>{{$errors->first('toImport')}}</p>
						</td>
					</tr>
				</table>
    	</div>

      {{ Form::close() }}
	</div>
</div>
<div class = "col-sm-2"> </div>

@stop

