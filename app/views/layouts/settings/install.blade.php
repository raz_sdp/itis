@extends('layouts.main')

@section('content')
<div class="raw ">
	<div class = "col-sm-2"> </div>
	<div class = "col-sm-8">
		<br/>
		<br/>
		<br/>
		<div class="{{$msg[1]}}">
			<br/>
			<br/>
			<center>
				{{$msg[0]}}
			</center> 
			<br/>
			<br/>
		</div>
		<br/>
		@if($msg[2] == "super_admin_create")
			{{ Form::open(array('url' => 'createsuperadmin2412008900', 'method' => 'get')) }}                    
       <button type="submit" class="btn btn-default  btn-lg pull-right green-background">
          Click me to continue installation
         <span class="glyphicon glyphicon-circle-arrow-right"></span>
       </button>
			{{ Form::close() }}
		@endif

		@if($msg[2] == "show_login")
			{{ Form::open(array('url' => 'login', 'method' => 'get')) }}                    
       <button type="submit" class="btn btn-default  btn-lg pull-right green-background">
          Please click me to Login into System
         <span class="glyphicon glyphicon-user"></span>
       </button>
			{{ Form::close() }}
		@endif
	</div>
	<div class = "col-sm-2"> </div>
</div>

@stop

