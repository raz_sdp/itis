<html>
	<head>
		<title>
			ITIS-DB
		</title>
		
		@show
		<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <!-- CSS are placed here -->
    {{ HTML::style('twiter_bootstrap/css/bootstrap.css') }}
    {{ HTML::style('twiter_bootstrap/css/jquery-ui.css') }}
    {{ HTML::style('twiter_bootstrap/css/itis_style.css') }}

    <!-- Scripts are placed here -->
    {{ HTML::script('twiter_bootstrap/js/jquery-2.1.3.min.js') }}
    {{ HTML::script('twiter_bootstrap/js/bootstrap.min.js') }}
    {{ HTML::script('twiter_bootstrap/js/jquery.validate.js') }}
    {{ HTML::script('twiter_bootstrap/js/jquery-ui.js') }}
    {{ HTML::script('twiter_bootstrap/js/underscore-min.js') }}
    {{ HTML::script('twiter_bootstrap/js/itis.js') }}
	</head>
    <script type="text/javascript">
        var ROOT;
        if (location.origin == 'http://localhost' || localhost.origin == 'http://127.0.0.1') {
            ROOT = location.origin + '/itis_db/';
        } else {
            ROOT = location.origin + '/';
        }
     </script>
	<body>
			@yield('content')
	</body>
</html>