@extends('layouts.main')
@section('content')
@include('layouts.navBar')
{{ HTML::script('twiter_bootstrap/js/custom.js') }}
<script type="text/javascript">
    var modules_1 =
    <?php echo json_encode($modules_1)?>
</script>
<script type="text/javascript">
    var modules_2 =
    <?php echo json_encode($modules_2)?>
</script>
<script type="text/javascript">
    var modules_1_new = <?php echo json_encode($modules_1)?>
</script>
<script type="text/javascript">
    var modules_2_new = <?php echo json_encode($modules_2)?>
</script>
<script type="text/javascript">
    var user_id = '';
</script>
<div class="alert alert-danger js-alert-danger"
     style="display:none; text-align: center; margin-top: 10px; padding-top: 15px">
    Please select any Course.
</div>
<div class="container" xmlns="http://www.w3.org/1999/html">
<div role="tabpanel" class="js-main-content">

<!-- Nav tabs -->
<ul class="nav nav-tabs js-nav" role="tablist">
    <li role="presentation" id="tab_1" <?php if ($tab == 1) echo "class=active" ?>><a
            href="#home" aria-controls="home" role="tab"
            data-toggle="tab">Semester 1</a></li>
    <li role="presentation" id="tab_2" <?php if ($tab == 2) echo "class=active" ?>><a
            href="#profile" aria-controls="profile" role="tab"
            data-toggle="tab">Semester 2</a></li>
    <li role="presentation" id="tab_3" <?php if ($tab == 3) echo "class=active" ?>><a
            href="#messages" aria-controls="messages"
            role="tab" data-toggle="tab">Semester 3</a>
    </li>
    <li role="presentation" id="tab_4" <?php if ($tab == 4) echo "class=active" ?>><a
            href="#settings" aria-controls="settings"
            role="tab" data-toggle="tab">Semester 4</a>
    </li>
    <li role="presentation" id="tab_5">
        <a href="#plan_details" aria-controls="plan_details" role="tab" data-toggle="tab">Plan Details</a>
    </li>
    <!--<li role="presentation" id="tab_6">
        <a href="#finish" aria-controls="finish" role="tab" data-toggle="tab">Finish</a>
    </li>-->
</ul>

<!-- Tab panes -->
<div class="tab-content my-tab">
<!-- <div class="alert alert-danger js-alert-danger"
     style="display:none; text-align: center; margin-top: 10px; padding-top: 15px">
    Please select any Course.
</div> -->
<div role="tabpanel" class="tab-pane <?php if ($tab == 1) echo "active"; ?>" id="home">
<!--<div class="col-sm-3">
    <form class="form-inline" method="post">
        <div class="form-group">
            {{ Form::select('noofsemester', array(null => 'No Of Semester',
            '1' => '1',
            '2' => '2',
            '3' => '3',
            '4' => '4',
            '5' => '5',
            '6' => '6',
            '7' => '7',
            '8' => '8',
            '9' => '9' ),
            null, array('class' =>'form-control input-sm')) }}
        </div>
    </form>
</div>-->
<div class="course_menu">
<form class="form-inline" method="post" action="<?php echo url() ?>/editstudyplan/<?php echo Input::segment(2) ?>"
      id="study-plan">
    <div class="form-group">
        <input type="hidden" name="tab" value="1">
        <select name="semester_1" class="form-control input-sm js-semester" id="sem_1_ses">
            <option
                value="SoSe 2012" <?php if ($semester_1_name == 'SoSe 2012') echo "selected=selected" ?>>
                2012 SS
            </option>
            <option
                value="WiSe 2012/13" <?php if ($semester_1_name == 'WiSe 2012/13') echo "selected=selected" ?>>
                2012 WS
            </option>
            <option
                value="SoSe 2013" <?php if ($semester_1_name == 'SoSe 2013') echo "selected=selected" ?>>
                2013 SS
            </option>
            <option
                value="WiSe 2013/14" <?php if ($semester_1_name == 'WiSe 2013/14') echo "selected=selected" ?>>
                2013 WS
            </option>
            <option
                value="SoSe 2014" <?php if ($semester_1_name == 'SoSe 2014') echo "selected=selected" ?>>
                2014 SS
            </option>
            <option
                value="WiSe 2014/15" <?php if ($semester_1_name == 'WiSe 2014/15') echo "selected=selected" ?>>
                2014 WS
            </option>
            <option
                value="SoSe 2015" <?php if ($semester_1_name == 'SoSe 2015') echo "selected=selected" ?>>
                2015 SS
            </option>
            <option
                value="WiSe 2015/16" <?php if ($semester_1_name == 'WiSe 2015/16') echo "selected=selected" ?>>
                2015 WS
            </option>
            <option
                value="SoSe 2016" <?php if ($semester_1_name == 'SoSe 2016') echo "selected=selected" ?>>
                2016 SS
            </option>
            <option
                value="WiSe 2016/17" <?php if ($semester_1_name == 'WiSe 2016/17') echo "selected=selected" ?>>
                2016 WS
            </option>
            <option
                value="SoSe 2017" <?php if ($semester_1_name == 'SoSe 2017') echo "selected=selected" ?>>
                2017 SS
            </option>
            <option
                value="WiSe 2017/18" <?php if ($semester_1_name == 'WiSe 2017/18') echo "selected=selected" ?>>
                2017 WS
            </option>
            <option
                value="SoSe 2018" <?php if ($semester_1_name == 'SoSe 2018') echo "selected=selected" ?>>
                2018 SS
            </option>
            <option
                value="WiSe 2018/19" <?php if ($semester_1_name == 'WiSe 2018/19') echo "selected=selected" ?>>
                2018 WS
            </option>
        </select>
        {{ Form::button('All Courses in this semester', ['class' => 'btn btn-primary btn-sm all-course-btn']) }}
    </div>
</form>
<form id="form_1">
    <table class="table table-bordered">
        <tr>
            <th>Category</th>
            <th>Module</th>
            <th>Exam ID</th>
            <th>Course<span class="red">(*)</span></th>
        </tr>
        <?php
        ################################################ Dynamic table create for courses ###################################################
        if (!empty($data)) {
            foreach ($data as $key => $modules) {
                #BaseController::_setTrace($modules);
                ?>
                <tr>
                    <td class="category">
                        <?php
                        echo strlen($modules[0][0]['category_name']) > 60 ? HelperActions::makeMultipleLineString($modules[0][0]['category_name'], 40) : $modules[0][0]['category_name'];
                        ?>
                    </td>
                    <td class="category">
                        <?php
                        $items = array();
                        foreach ($modules as $item) {
                            $module_name = strlen($item[0]['module']) > 60 ? HelperActions::makeMultipleLineString($item[0]['module'], 40) : $item[0]['module'];
                            array_push($items, $module_name);
                            //echo '<span class="category">'.$item[0]['module'].'</span><hr>';
                        }
                        echo implode('<hr>', $items);
                        ?>
                    </td>
                    <td>
                        <?php
                        foreach ($modules as $key_exam => $item) {
                            $hr1 = '';
                            $hr2 = '';
                            if ($key_exam < count($modules) - 1) $hr1 = '<hr>';
                            foreach ($item as $key_item => $new) {
                                if ($key_item < count($item) - 1) $hr2 = '<hr>';
                                echo $new['moduleexamid'];
                                echo $hr2;
                            }
                            echo $hr1;
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        foreach ($modules as $key2 => $item) {
                            $hr = '';
                            if ($key2 < count($modules) - 1) $hr = '<hr>';
                            ?>
                            <table class="table table-bordered inner-table">
                                <?php
                                foreach ($item as $new) {
                                    ?>
                                    <tr class="info">
                                        <td rel="<?php echo $new['id']; ?>" class="course_td">
                                            <?php
                                            $present_1 = BaseController::search($course_sem_1, 'course_id', $new['id']);
                                            if (!empty($present_1)) {
                                                ?>
                                                <input checked type="checkbox" name="course" id="course_1" class="course_1" required
                                                       value="<?php echo $new['module_id'] ?>">
                                            <?php
                                            } else {
                                                ?>
                                                <input type="checkbox" name="course" id="course_1" class="course_1" required
                                                       value="<?php echo $new['module_id'] ?>">
                                            <?php
                                            }
                                            ?>
                                            <?php
                                            echo strlen($new['course']) > 60 ? HelperActions::makeMultipleLineString($new['course'], 40) : $new['course'];
                                            ?>
                                        </td>
                                        <td rel="<?php echo $new['credits']; ?>" class="text-center">
                                            <?php
                                            echo $new['credits'];
                                            ?>
                                        </td>
                                    </tr>
                                <?php
                                }
                                ?>
                            </table>
                            <?php
                            echo $hr;
                        }
                        ?>
                    </td>
                </tr>
            <?php
            }
        } else {
            ?>
            <tr class="warning">
                <td>No Result Found.</td>
                <td>No Result Found.</td>
                <td>No Result Found.</td>
                <td>No Result Found.</td>
            </tr>
        <?php
        }
        ?>
    </table>
</form>
<table class="table table-bordered">
    <tr>
        <td>
            <div><strong>Soft skill(optional)</strong></div>
            <div class="js-soft-skill-1">
                <?php
                if (!empty($my_softskill_sem_1)) {
                    foreach ($my_softskill_sem_1 as $skill_key => $value) {
                        ?>
                        <div class="row js-single-skill-1">
                            <div class="col-xs-3" b>
                                <!-- {{ Form::select('site_1[]', Site::makeSiteArray(),
                                null, array('class' =>'form-control','id'=> 'site_1')) }} -->
                                <?php
                                $sites = Site::makeSiteArray();
                                #BaseController::_setTrace($sites);
                                ?>
                                <select name="site_1[]" class="form-control" id="site_1">
                                    <?php
                                    foreach ($sites as $key => $site) {
                                        if ($key == $value['site_id']) {
                                            ?>
                                            <option value="<?php echo $key ?>" selected><?php echo $site ?></option>
                                        <?php
                                        } else {
                                            ?>
                                            <option value="<?php echo $key ?>"><?php echo $site ?></option>
                                        <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="col-xs-6">
                                <input type="text" class="form-control" placeholder="Course Title" id="soft_skill_1"
                                       name="soft_skill_1[]" value="<?php echo $value['soft_skills_name'] ?>">
                            </div>
                            <div class="col-xs-2">
                                <select id="credit_1" class="form-control" name="credit_1[]">
                                    <option value="">Credit</option>
                                    <?php
                                    for ($i = 1; $i <= 8; $i++) {
                                        if ($i == $value['credit']) {
                                            ?>
                                            <option selected value="<?php echo $i ?>"><?php echo $i ?> </option>
                                        <?php
                                        } else {
                                            ?>
                                            <option value="<?php echo $i ?>"><?php echo $i ?> </option>
                                        <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="col-xs-1">
                                <?php
                                if ($skill_key == 0) {
                                    ?>
                                    {{ Form::button('Add', ['class' => 'btn btn-primary btn-sm add-btn js-add-new-btn-1']) }}
                                <?php
                                } else {
                                    ?>
                                    {{ Form::button('Remove', ['class' => 'btn btn-primary btn-sm add-btn js-remove-btn-1']) }}
                                <?php
                                }
                                ?>
                            </div>
                        </div>
                    <?php
                    }
                } else {
                    ?>
                    <div class="row js-single-skill-1">
                        <div class="col-xs-3" b>
                            {{ Form::select('site_1[]', Site::makeSiteArray(),
                            null, array('class' =>'form-control','id'=> 'site_1')) }}
                        </div>
                        <div class="col-xs-6">
                            <input type="text" class="form-control" placeholder="Course Title" id="soft_skill_1"
                                   name="soft_skill_1[]">
                        </div>
                        <div class="col-xs-2">
                            <select id="credit_1" class="form-control" name="credit_1[]">
                                <option value="">Credit</option>
                                <?php
                                for ($i = 1; $i <= 8; $i++) {
                                    ?>
                                    <option value="<?php echo $i ?>"><?php echo $i ?> </option>
                                <?php
                                }
                                ?>
                            </select>
                        </div>
                        <div class="col-xs-1">
                            {{ Form::button('Add', ['class' => 'btn btn-primary btn-sm add-btn js-add-new-btn-1']) }}
                        </div>
                    </div>
                <?php
                }
                ?>

            </div>
        </td>
    </tr>
</table>
<div class="pull-right">
    <p>{{ Form::button('Save', ['class' => 'btn btn-primary btn-sm save_btn', 'id' => 'js-edit-1']) }}</p>
</div>
</div>
<div class="selected-course-list" style="display:none;">
    <p>{{ Form::button('Back to the Course', ['class' => 'btn btn-primary btn-sm js-back-course']) }}</p>
</div>
</div>
<div role="tabpanel" class="tab-pane <?php if ($tab == 2) echo "active"; ?>" id="profile">
<div class="course_menu">
<form class="form-inline" method="post" action="<?php echo url() ?>/editstudyplan/<?php echo Input::segment(2) ?>"
      id="study-plan">
    <div class="form-group">
        <input type="hidden" name="tab" value="2">
        <select name="semester_2" class="form-control input-sm js-semester" id="sem_2_ses">
            <option
                value="SoSe 2012" <?php if ($semester_2_name == 'SoSe 2012') echo "selected=selected" ?>>
                2012 SS
            </option>
            <option
                value="WiSe 2012/13" <?php if ($semester_2_name == 'WiSe 2012/13') echo "selected=selected" ?>>
                2012 WS
            </option>
            <option
                value="SoSe 2013" <?php if ($semester_2_name == 'SoSe 2013') echo "selected=selected" ?>>
                2013 SS
            </option>
            <option
                value="WiSe 2013/14" <?php if ($semester_2_name == 'WiSe 2013/14') echo "selected=selected" ?>>
                2013 WS
            </option>
            <option
                value="SoSe 2014" <?php if ($semester_2_name == 'SoSe 2014') echo "selected=selected" ?>>
                2014 SS
            </option>
            <option
                value="WiSe 2014/15" <?php if ($semester_2_name == 'WiSe 2014/15') echo "selected=selected" ?>>
                2014 WS
            </option>
            <option
                value="SoSe 2015" <?php if ($semester_2_name == 'SoSe 2015') echo "selected=selected" ?>>
                2015 SS
            </option>
            <option
                value="WiSe 2015/16" <?php if ($semester_2_name == 'WiSe 2015/16') echo "selected=selected" ?>>
                2015 WS
            </option>
            <option
                value="SoSe 2016" <?php if ($semester_2_name == 'SoSe 2016') echo "selected=selected" ?>>
                2016 SS
            </option>
            <option
                value="WiSe 2016/17" <?php if ($semester_2_name == 'WiSe 2016/17') echo "selected=selected" ?>>
                2016 WS
            </option>
            <option
                value="SoSe 2017" <?php if ($semester_2_name == 'SoSe 2017') echo "selected=selected" ?>>
                2017 SS
            </option>
            <option
                value="WiSe 2017/18" <?php if ($semester_2_name == 'WiSe 2017/18') echo "selected=selected" ?>>
                2017 WS
            </option>
            <option
                value="SoSe 2018" <?php if ($semester_2_name == 'SoSe 2018') echo "selected=selected" ?>>
                2018 SS
            </option>
            <option
                value="WiSe 2018/19" <?php if ($semester_2_name == 'WiSe 2018/19') echo "selected=selected" ?>>
                2018 WS
            </option>
        </select>
        {{ Form::button('All Courses in this semester', ['class' => 'btn btn-primary btn-sm all-course-btn']) }}
    </div>
</form>
<form id="form_2">
    <table class="table table-bordered">
        <tr>
            <th>Category</th>
            <th>Module</th>
            <th>Exam ID</th>
            <th>Course<span class="red">(*)</span></th>
        </tr>
        <?php
        ################################################ Dynamic table create for courses ###################################################
        if (!empty($semester_2_data)) {
            foreach ($semester_2_data as $key => $modules) {
                #BaseController::_setTrace($modules);
                ?>
                <tr>
                    <td class="category">
                        <?php
                        echo strlen($modules[0][0]['category_name']) > 60 ? HelperActions::makeMultipleLineString($modules[0][0]['category_name'], 40) : $modules[0][0]['category_name'];
                        ?>
                    </td>
                    <td class="category">
                        <?php
                        $items = array();
                        foreach ($modules as $item) {
                            $module_name = strlen($item[0]['module']) > 60 ? HelperActions::makeMultipleLineString($item[0]['module'], 40) : $item[0]['module'];
                            array_push($items, $module_name);
                        }
                        echo implode('<hr>', $items);
                        ?>
                    </td>
                    <td>
                        <?php
                        foreach ($modules as $key_exam => $item) {
                            $hr1 = '';
                            $hr2 = '';
                            if ($key_exam < count($modules) - 1) $hr1 = '<hr>';
                            foreach ($item as $key_item => $new) {
                                if ($key_item < count($item) - 1) $hr2 = '<hr>';
                                echo $new['moduleexamid'];
                                echo $hr2;
                            }
                            echo $hr1;
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        foreach ($modules as $key2 => $item) {
                            $hr = '';
                            if ($key2 < count($modules) - 1) $hr = '<hr>';
                            ?>
                            <table class="table table-bordered inner-table">
                                <?php
                                foreach ($item as $new) {
                                    ?>
                                    <tr class="info">
                                        <td rel="<?php echo $new['id']; ?>" class="course_td">
                                            <?php
                                            $is_present = BaseController::search($course_sem_2, 'course_id', $new['id']);
                                            if (!empty($is_present)) {
                                                ?>
                                                <input checked type="checkbox" name="course" id="course_2" class="course_2" required
                                                       value="<?php echo $new['module_id'] ?>">
                                            <?php
                                            } else {
                                                ?>
                                                <input type="checkbox" name="course" id="course_2" class="course_2" required
                                                       value="<?php echo $new['module_id'] ?>">
                                            <?php
                                            }
                                            ?>
                                            <?php echo strlen($new['course']) > 60 ? HelperActions::makeMultipleLineString($new['course'], 40) : $new['course']; ?>
                                        </td>
                                        <td rel="<?php echo $new['credits'] ?>" class="text-center">
                                            <?php
                                            echo $new['credits'];
                                            ?>
                                        </td>
                                    </tr>
                                <?php
                                }
                                ?>
                            </table>
                            <?php
                            echo $hr;
                        }
                        ?>
                    </td>
                </tr>
            <?php
            }
        } else {
            ?>
            <tr class="warning">
                <td>No Result Found.</td>
                <td>No Result Found.</td>
                <td>No Result Found.</td>
                <td>No Result Found.</td>
            </tr>
        <?php
        }
        ?>
    </table>
</form>
<table class="table table-bordered">
    <tr>
        <td>
            <div><strong>Soft skill(optional)</strong></div>
            <div class="js-soft-skill-2">
                <?php
                if (!empty($my_softskill_sem_2)) {
                    foreach ($my_softskill_sem_2 as $skill_key => $value) {
                        ?>
                        <div class="row js-single-skill-2">
                            <div class="col-xs-3" b>
                                <!-- {{ Form::select('site_1[]', Site::makeSiteArray(),
                                null, array('class' =>'form-control','id'=> 'site_1')) }} -->
                                <?php
                                $sites = Site::makeSiteArray();
                                #BaseController::_setTrace($sites);
                                ?>
                                <select name="site_2[]" class="form-control" id="site_2">
                                    <?php
                                    foreach ($sites as $key => $site) {
                                        if ($key == $value['site_id']) {
                                            ?>
                                            <option value="<?php echo $key ?>" selected><?php echo $site ?></option>
                                        <?php
                                        } else {
                                            ?>
                                            <option value="<?php echo $key ?>"><?php echo $site ?></option>
                                        <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="col-xs-6">
                                <input type="text" class="form-control" placeholder="Course Title" id="soft_skill_2"
                                       name="soft_skill_2[]" value="<?php echo $value['soft_skills_name'] ?>">
                            </div>
                            <div class="col-xs-2">
                                <select id="credit_2" class="form-control" name="credit_2[]">
                                    <option value="">Credit</option>
                                    <?php
                                    for ($i = 1; $i <= 8; $i++) {
                                        if ($i == $value['credit']) {
                                            ?>
                                            <option selected value="<?php echo $i ?>"><?php echo $i ?> </option>
                                        <?php
                                        } else {
                                            ?>
                                            <option value="<?php echo $i ?>"><?php echo $i ?> </option>
                                        <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="col-xs-1">
                                <?php
                                if ($skill_key == 0) {
                                    ?>
                                    {{ Form::button('Add', ['class' => 'btn btn-primary btn-sm add-btn js-add-new-btn-2']) }}
                                <?php
                                } else {
                                    ?>
                                    {{ Form::button('Remove', ['class' => 'btn btn-primary btn-sm add-btn js-remove-btn-2']) }}
                                <?php
                                }
                                ?>
                            </div>
                        </div>
                    <?php
                    }
                } else {
                    ?>
                    <div class="row js-single-skill-2">
                        <div class="col-xs-3">
                            {{ Form::select('site_2[]', Site::makeSiteArray(),
                            null, array('class' =>'form-control','id'=> 'site_2')) }}
                        </div>
                        <div class="col-xs-6">
                            <input type="text" class="form-control" placeholder="Course Title" id="soft_skill_2"
                                   name="soft_skill_2[]">
                        </div>
                        <div class="col-xs-2">
                            <select id="credit_2" class="form-control" name="credit_2[]">
                                <option value="">Credit</option>
                                <?php
                                for ($i = 1; $i <= 8; $i++) {
                                    ?>
                                    <option value="<?php echo $i ?>"><?php echo $i ?> </option>
                                <?php
                                }
                                ?>
                            </select>
                        </div>
                        <div class="col-xs-1">
                            {{ Form::button('Add', ['class' => 'btn btn-primary btn-sm add-btn js-add-new-btn-2']) }}
                        </div>
                    </div>
                <?php
                }
                ?>
            </div>
        </td>
    </tr>
</table>
<table class="table table-bordered">
    <tr>
        <td>
            <div class="form-group">
                <label for="exampleInputEmail1">Project Title</label>
                <input type="text" class="form-control" id="semester-2-project"
                       value="<?php echo !empty($my_research_project) && $my_research_project['semester'] == 2 ? $my_research_project['research_projects_name'] : '' ?>"
                       placeholder="Enter Your Project Title, It's optional">
            </div>
        </td>
    </tr>
</table>

<div class="pull-right">
    <p>{{ Form::button('Save', ['class' => 'btn btn-primary btn-sm save_btn', 'id' => 'js-edit-2']) }}</p>
</div>
</div>
<div class="selected-course-list" style="display:none;">

</div>
</div>
<div role="tabpanel" class="tab-pane <?php if ($tab == 3) echo "active"; ?>" id="messages">
<div class="course_menu">
<form class="form-inline" method="post" action="<?php echo url() ?>/editstudyplan/<?php echo Input::segment(2) ?>" id="study-plan">
    <div class="form-group">
        <input type="hidden" name="tab" value="3">
        <select name="semester_3" class="form-control input-sm js-semester" id="sem_3_ses">
            <option
                value="SoSe 2012" <?php if ($semester_3_name == 'SoSe 2012') echo "selected=selected" ?>>
                2012 SS
            </option>
            <option
                value="WiSe 2012/13" <?php if ($semester_3_name == 'WiSe 2012/13') echo "selected=selected" ?>>
                2012 WS
            </option>
            <option
                value="SoSe 2013" <?php if ($semester_3_name == 'SoSe 2013') echo "selected=selected" ?>>
                2013 SS
            </option>
            <option
                value="WiSe 2013/14" <?php if ($semester_3_name == 'WiSe 2013/14') echo "selected=selected" ?>>
                2013 WS
            </option>
            <option
                value="SoSe 2014" <?php if ($semester_3_name == 'SoSe 2014') echo "selected=selected" ?>>
                2014 SS
            </option>
            <option
                value="WiSe 2014/15" <?php if ($semester_3_name == 'WiSe 2014/15') echo "selected=selected" ?>>
                2014 WS
            </option>
            <option
                value="SoSe 2015" <?php if ($semester_3_name == 'SoSe 2015') echo "selected=selected" ?>>
                2015 SS
            </option>
            <option
                value="WiSe 2015/16" <?php if ($semester_3_name == 'WiSe 2015/16') echo "selected=selected" ?>>
                2015 WS
            </option>
            <option
                value="SoSe 2016" <?php if ($semester_3_name == 'SoSe 2016') echo "selected=selected" ?>>
                2016 SS
            </option>
            <option
                value="WiSe 2016/17" <?php if ($semester_3_name == 'WiSe 2016/17') echo "selected=selected" ?>>
                2016 WS
            </option>
            <option
                value="SoSe 2017" <?php if ($semester_3_name == 'SoSe 2017') echo "selected=selected" ?>>
                2017 SS
            </option>
            <option
                value="WiSe 2017/18" <?php if ($semester_3_name == 'WiSe 2017/18') echo "selected=selected" ?>>
                2017 WS
            </option>
            <option
                value="SoSe 2018" <?php if ($semester_3_name == 'SoSe 2018') echo "selected=selected" ?>>
                2018 SS
            </option>
            <option
                value="WiSe 2018/19" <?php if ($semester_3_name == 'WiSe 2018/19') echo "selected=selected" ?>>
                2018 WS
            </option>
        </select>
        {{ Form::button('All Courses in this semester', ['class' => 'btn btn-primary btn-sm all-course-btn']) }}
    </div>
</form>
<form id="form_3">
    <table class="table table-bordered">
        <tr>
            <th>Category</th>
            <th>Module</th>
            <th>Exam ID</th>
            <th>Course</th>
        </tr>
        <?php
        ################################################ Dynamic table create for courses ###################################################
        if (!empty($semester_3_data)) {
            foreach ($semester_3_data as $key => $modules) {
                #BaseController::_setTrace($modules);
                ?>
                <tr>
                    <td class="category">
                        <?php
                        echo strlen($modules[0][0]['category_name']) > 60 ? HelperActions::makeMultipleLineString($modules[0][0]['category_name'], 40) : $modules[0][0]['category_name'];
                        ?>
                    </td>
                    <td class="category">
                        <?php
                        $items = array();
                        foreach ($modules as $item) {
                            $module_name = strlen($item[0]['module']) > 60 ? HelperActions::makeMultipleLineString($item[0]['module'], 40) : $item[0]['module'];
                            array_push($items, $module_name);
                        }
                        echo implode('<hr>', $items);
                        ?>
                    </td>
                    <td>
                        <?php
                        foreach ($modules as $key_exam => $item) {
                            $hr1 = '';
                            $hr2 = '';
                            if ($key_exam < count($modules) - 1) $hr1 = '<hr>';
                            foreach ($item as $key_item => $new) {
                                if ($key_item < count($item) - 1) $hr2 = '<hr>';
                                echo $new['moduleexamid'];
                                echo $hr2;
                            }
                            echo $hr1;
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        foreach ($modules as $key2 => $item) {
                            $hr = '';
                            if ($key2 < count($modules) - 1) $hr = '<hr>';
                            ?>
                            <table class="table table-bordered inner-table">
                                <?php
                                foreach ($item as $new) {
                                    ?>
                                    <tr class="info">
                                        <td rel="<?php echo $new['id'] ?>" class="course_td">
                                            <?php
                                            $present = BaseController::search($course_sem_3, 'course_id', $new['id']);
                                            if (!empty($present)) {
                                                ?>
                                                <input checked type="checkbox" name="course" class="course_3" id="course_3"
                                                       value="<?php echo $new['module_id'] ?>">
                                            <?php
                                            } else {
                                                ?>
                                                <input type="checkbox" name="course" class="course_3" id="course_3"
                                                       value="<?php echo $new['module_id'] ?>">
                                            <?php
                                            }
                                            echo strlen($new['course']) > 60 ? HelperActions::makeMultipleLineString($new['course'], 40) : $new['course'];
                                            ?>
                                        </td>
                                        <td rel="<?php echo $new['credits'] ?>" class="text-center">
                                            <?php
                                            echo $new['credits'];
                                            ?>
                                        </td>
                                    </tr>
                                <?php
                                }
                                ?>
                            </table>
                            <?php
                            echo $hr;
                        }
                        ?>
                    </td>
                </tr>
            <?php
            }
        } else {
            ?>
            <tr class="warning">
                <td>No Result Found.</td>
                <td>No Result Found.</td>
                <td>No Result Found.</td>
                <td>No Result Found.</td>
            </tr>
        <?php
        }
        ?>
    </table>
</form>
<table class="table table-bordered">
    <tr>
        <td>
            <div><strong>Soft skill(Optional)</strong></div>
            <div class="js-soft-skill-3">
                <?php
                if (!empty($my_softskill_sem_3)) {
                    foreach ($my_softskill_sem_3 as $skill_key => $value) {
                        ?>
                        <div class="row js-single-skill-3">
                            <div class="col-xs-3" b>
                                <!-- {{ Form::select('site_1[]', Site::makeSiteArray(),
                                null, array('class' =>'form-control','id'=> 'site_1')) }} -->
                                <?php
                                $sites = Site::makeSiteArray();
                                #BaseController::_setTrace($sites);
                                ?>
                                <select name="site_3[]" class="form-control" id="site_3">
                                    <?php
                                    foreach ($sites as $key => $site) {
                                        if ($key == $value['site_id']) {
                                            ?>
                                            <option value="<?php echo $key ?>" selected><?php echo $site ?></option>
                                        <?php
                                        } else {
                                            ?>
                                            <option value="<?php echo $key ?>"><?php echo $site ?></option>
                                        <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="col-xs-6">
                                <input type="text" class="form-control" placeholder="Course Title" id="soft_skill_3"
                                       name="soft_skill_3[]" value="<?php echo $value['soft_skills_name'] ?>">
                            </div>
                            <div class="col-xs-2">
                                <select id="credit_3" class="form-control" name="credit_3[]">
                                    <option value="">Credit</option>
                                    <?php
                                    for ($i = 1; $i <= 8; $i++) {
                                        if ($i == $value['credit']) {
                                            ?>
                                            <option selected value="<?php echo $i ?>"><?php echo $i ?> </option>
                                        <?php
                                        } else {
                                            ?>
                                            <option value="<?php echo $i ?>"><?php echo $i ?> </option>
                                        <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="col-xs-1">
                                <?php
                                if ($skill_key == 0) {
                                    ?>
                                    {{ Form::button('Add', ['class' => 'btn btn-primary btn-sm add-btn js-add-new-btn-3']) }}
                                <?php
                                } else {
                                    ?>
                                    {{ Form::button('Remove', ['class' => 'btn btn-primary btn-sm add-btn js-remove-btn-3']) }}
                                <?php
                                }
                                ?>
                            </div>
                        </div>
                    <?php
                    }
                } else {
                    ?>
                    <div class="row js-single-skill-3">
                        <div class="col-xs-3">
                            {{ Form::select('site_3[]', Site::makeSiteArray(),
                            null, array('class' =>'form-control','id'=> 'site_3')) }}
                        </div>
                        <div class="col-xs-6">
                            <input type="text" class="form-control" placeholder="Course Title" id="soft_skill_3"
                                   name="soft_skill_3[]">
                        </div>
                        <div class="col-xs-2">
                            <select id="credit_3" class="form-control" name="credit_3[]">
                                <option value="">Credit</option>
                                <?php
                                for ($i = 1; $i <= 8; $i++) {
                                    ?>
                                    <option value="<?php echo $i ?>"><?php echo $i ?> </option>
                                <?php
                                }
                                ?>

                            </select>
                        </div>
                        <div class="col-xs-1">
                            {{ Form::button('Add', ['class' => 'btn btn-primary btn-sm add-btn js-add-new-btn-3']) }}
                        </div>
                    </div>
                <?php
                }
                ?>
            </div>
        </td>
    </tr>
</table>
<table class="table table-bordered">
    <tr>
        <td>
            <div class="form-group">
                <label for="exampleInputEmail1">Project Title</label>
                <input type="text" class="form-control" id="project_name_1" placeholder="Enter Your Project Title"
                       value="<?php echo !empty($my_research_project) && $my_research_project['semester'] == 3 ? $my_research_project['research_projects_name'] : '' ?>">
            </div>
        </td>
    </tr>
</table>


<div class="pull-right">
    <p>{{ Form::button('Save', ['class' => 'btn btn-primary btn-sm save_btn', 'id' => 'js-edit-3']) }}</p>
</div>
</div>
<div class="selected-course-list" style="display:none;">

</div>
</div>
<div role="tabpanel" class="tab-pane <?php if ($tab == 4) echo "active"; ?>" id="settings">
    <!--<form class="form-inline" method="post" action="studyplan" id="study-plan">
        <div class="form-group">
            <input type="hidden" name="tab" value="4">
            <select name="semester_4" class="form-control input-sm js-semester">
                <option
                    value="SoSe 2012" <?php /*if (!empty($semester_4_data[0][0]) && $semester_4_data[0][0]['semester'] == 'SoSe 2012') echo "selected=selected" */ ?>>
                    2012 SS
                </option>
                <option
                    value="WiSe 2012/13" <?php /*if (!empty($semester_4_data[0][0]) && $semester_4_data[0][0]['semester'] == 'WiSe 2012/13') echo "selected=selected" */ ?>>
                    2012 WS
                </option>
                <option
                    value="SoSe 2013" <?php /*if (!empty($semester_4_data[0][0]) && $semester_4_data[0][0]['semester'] == 'SoSe 2013') echo "selected=selected" */ ?>>
                    2013 SS
                </option>
                <option
                    value="WiSe 2013/14" <?php /*if (!empty($semester_4_data[0][0]) && $semester_4_data[0][0]['semester'] == 'WiSe 2013/14') echo "selected=selected" */ ?>>
                    2013 WS
                </option>
                <option
                    value="SoSe 2014" <?php /*if (!empty($semester_4_data[0][0]) && $semester_4_data[0][0]['semester'] == 'SoSe 2014') echo "selected=selected" */ ?>>
                    2014 SS
                </option>
                <option
                    value="WiSe 2014/15" <?php /*if (!empty($semester_4_data[0][0]) && $semester_4_data[0][0]['semester'] == 'WiSe 2014/15') echo "selected=selected" */ ?>>
                    2014 WS
                </option>
                <option
                    value="SoSe 2015" <?php /*if (!empty($semester_4_data[0][0]) && $semester_4_data[0][0]['semester'] == 'SoSe 2015') echo "selected=selected" */ ?>>
                    2015 SS
                </option>
                <option
                    value="WiSe 2015/16" <?php /*if (!empty($semester_4_data[0][0]) && $semester_4_data[0][0]['semester'] == 'WiSe 2015/16') echo "selected=selected" */ ?>>
                    2015 WS
                </option>
                <option
                    value="SoSe 2016" <?php /*if (!empty($semester_4_data[0][0]) && $semester_4_data[0][0]['semester'] == 'SoSe 2016') echo "selected=selected" */ ?>>
                    2016 SS
                </option>
                <option
                    value="WiSe 2016/17" <?php /*if (!empty($semester_4_data[0][0]) && $semester_4_data[0][0]['semester'] == 'WiSe 2016/17') echo "selected=selected" */ ?>>
                    2016 WS
                </option>
                <option
                    value="SoSe 2017" <?php /*if (!empty($semester_4_data[0][0]) && $semester_4_data[0][0]['semester'] == 'SoSe 2017') echo "selected=selected" */ ?>>
                    2017 SS
                </option>
                <option
                    value="WiSe 2017/18" <?php /*if (!empty($semester_4_data[0][0]) && $semester_4_data[0][0]['semester'] == 'WiSe 2017/18') echo "selected=selected" */ ?>>
                    2017 WS
                </option>
                <option
                    value="SoSe 2018" <?php /*if (!empty($semester_4_data[0][0]) && $semester_4_data[0][0]['semester'] == 'SoSe 2018') echo "selected=selected" */ ?>>
                    2018 SS
                </option>
                <option
                    value="WiSe 2018/19" <?php /*if (!empty($semester_4_data[0][0]) && $semester_4_data[0][0]['semester'] == 'WiSe 2018/19') echo "selected=selected" */ ?>>
                    2018 WS
                </option>
            </select>
        </div>
    </form>  -->
    <table class="table table-bordered">
        <tr>
            <td>
                <form id="form_4">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Theses Title</label>
                        <input type="text" class="form-control" id="project_name_2"
                               value="<?php echo !empty($my_master_theses) ? $my_master_theses : '' ?>"
                               placeholder="Enter Your Theses Title">
                    </div>
                </form>
            </td>
        </tr>
    </table>

    <div class="pull-right">
        <p>{{ Form::button('Save', ['class' => 'btn btn-primary btn-sm save_btn', 'id' => 'js-edit-4']) }}</p>
    </div>
</div>
<div role="tabpanel" class="tab-pane <?php if ($tab == 5) echo "active"; ?>" id="plan_details">
    <div class="row" style="padding-bottom: 10px">
        <?php
        $mentors = array('Prof. Dr. Wolfgang Nejdl', 'Prof. Dr. Wolf-Tilo Balke', 'Prof. Dr. Sven Hartmann', 'Prof. Dr. Dieter Hogrefe');
        $mentor_name = StudyPlan::getIndividualPlan(Input::segment(2))['mentor_name'];
        if (empty($mentor_name)) {
            echo '<div class="alert alert-success js-alert-info" style="text-align: center;background-color: #d9edf7;padding: 10px;">Mentor name hasn\'t been selected.</div>';
        }
        ?>
        <div class="col-sm-12 text-center"><u><strong>Individual Study Plan for the Master’s Program “Internet
                    Technologies
                    and Information Systems“(ITIS)</strong></u></div>
        <?php
        $user = User::getUserInfo(Input::segment(2));
        ?>
        <div class="col-sm-3"><strong>Student Name:</strong></div>
        <div class="col-sm-9"><?php echo $user['title'] . '. ' . $user['first_name'] . ' ' . $user['last_name'] ?></div>
        <div class="col-sm-3"><strong>Hannover (!) Matriculation Number:</strong></div>
        <div class="col-sm-9">xxxxxxxxxxxxxxxx</div>
        <div class="col-sm-3"><strong>University:</strong></div>
        <div class="col-sm-9"><?php echo $user['university'] ?></div>
        <div class="col-sm-3"><strong>E-Mail:</strong></div>
        <div class="col-sm-9"><?php echo $user['email'] ?></div>
        <div class="col-sm-3"><strong>Mentor Name:</strong></div>
        <div class="col-sm-4">
            <select class="form-control" id="mentor">
                <option value="">Select One</option>
                <?php
                foreach ($mentors as $key => $mentor) {
                    if ($mentor == $mentor_name)
                        echo '<option value="' . $mentor . '" selected>' . $mentor . '</option>';
                    else
                        echo '<option value="' . $mentor . '">' . $mentor . '</option>';
                }
                ?>
            </select>
        </div>
    </div>
    <table class="table table-bordered js-plan-details">
        <script type="text/javascript">
            var modules_all = <?php echo json_encode($modules_all)?>
        </script>
        <script type="text/javascript">
            var semester_1 = <?php echo json_encode($course_sem_1)?>
        </script>
        <script type="text/javascript">
            var semester_1_new = <?php echo json_encode($course_sem_1)?>
        </script>
        <script type="text/javascript">
            var semester_2 = <?php echo json_encode($course_sem_2)?>
        </script>
        <script type="text/javascript">
            var semester_2_new = <?php echo json_encode($course_sem_2)?>
        </script>
        <script type="text/javascript">
            var semester_3_course = <?php echo json_encode($course_sem_3)?>
        </script>
        <script type="text/javascript">
            var semester_3_new = <?php echo json_encode($course_sem_3)?>
        </script>

        <script type="text/javascript">
            var skill_1 = <?php echo json_encode($my_softskill_sem_1)?>
        </script>
        <script type="text/javascript">
            var skill_2 = <?php echo json_encode($my_softskill_sem_2)?>
        </script>
        <script type="text/javascript">
            var skill_3 = <?php echo json_encode($my_softskill_sem_3)?>
        </script>

        <script type="text/javascript">
            var session_1 = <?php echo json_encode($session_1)?>
        </script>
        <script type="text/javascript">
            var session_2 = <?php echo json_encode($session_2)?>
        </script>
        <script type="text/javascript">
            var session_3 = <?php echo json_encode($session_3)?>
        </script>

        <script type="text/javascript">
            var credit_1 = <?php echo json_encode($credit_1)?>
        </script>
        <script type="text/javascript">
            var credit_2 = <?php echo json_encode($credit_2)?>
        </script>
        <script type="text/javascript">
            var credit_3 = <?php echo json_encode($credit_3)?>
        </script>
        <script type="text/javascript">
            var semester_2_project = "<?php echo $my_project_2?>";
            var semester_3 = "<?php echo $my_project_3?>";
            var semester_4 = "<?php echo $my_master_theses?>";
        </script>

        <script type="text/javascript">
            localStorage.clear();
            localStorage.setItem('semester_1', JSON.stringify(semester_1_new));
            localStorage.setItem('session_1', JSON.stringify(session_1));
            localStorage.setItem('credits_1', JSON.stringify(credit_1));
            localStorage.setItem('modules_1', JSON.stringify(modules_1_new));
            localStorage.setItem('semester_2', JSON.stringify(semester_2_new));
            localStorage.setItem('session_2', JSON.stringify(session_2));
            localStorage.setItem('credits_2', JSON.stringify(credit_2));
            localStorage.setItem('modules_2', JSON.stringify(modules_2_new));
            localStorage.setItem('semester_3', JSON.stringify(semester_3_new));
            localStorage.setItem('session_3', JSON.stringify(session_3));
            localStorage.setItem('credits_3', JSON.stringify(credit_3));
            //console.log(localStorage);
        </script>
        <!--    Dynamic Plan details table created here from Js -->
    </table>

    <div class="pull-right">
        <p>{{ Form::button('Save', ['class' => 'btn btn-primary btn-sm', 'id' => 'js-edit-5']) }}</p>
    </div>
</div>
<div role="tabpanel" class="tab-pane <?php if ($tab == 6) echo "active"; ?>" id="finish">

</div>
</div>
</div>
</div>
<div class="col-sm-1"></div>
<div class="overlay">
    <div class="please_wait">Loading</div>
    <div id="loader_icon"></div>
</div>
</div>
<style type="text/css">
    .navbar{
         margin-bottom: 0px !important;
    }
</style>
@stop
