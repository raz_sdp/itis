@extends('layouts.main')
@section('content')
@include('layouts.navBar')
{{ HTML::script('twiter_bootstrap/js/custom.js') }}
<script type="text/javascript">
    localStorage.clear();
</script>
<script type="text/javascript">
    var modules_1 = <?php echo json_encode($modules_1)?>
</script>
<script type="text/javascript">
    var modules_2 = <?php echo json_encode($modules_2)?>
</script>
<script type="text/javascript">
    var modules_1_new = <?php echo json_encode($modules_1)?>
</script>
<script type="text/javascript">
    var modules_2_new = <?php echo json_encode($modules_2)?>
</script>
<script type="text/javascript">
    var user_id = '';
</script>

<?php 
function convertSemesterName($value) {   
    switch($value) {
        case 'SoSe 2012':
            return '2012 SS';
        case 'WiSe 2012/13':
            return '2012 WS';
        case 'SoSe 2013':
            return '2013 SS';
        case 'WiSe 2013/14':
            return '2013 WS';
        case 'SoSe 2014':
            return '2014 SS';
        case 'WiSe 2014/15':
            return '2014 WS';
        case 'SoSe 2015':
            return '2015 SS';
        case 'WiSe 2015/16':
            return '2015 WS';
        case 'SoSe 2016':
            return '2016 SS';
        case 'WiSe 2016/17':
            return '2016 WS';
        case 'SoSe 2017':
            return '2017 SS';
        case ' WiSe 2017/18':
            return '2017 WS';
        case 'SoSe 2018':
            return '2018 SS';
        case 'WiSe 2018/19':
            return '2018 WS';
    }
}
?>

<?php
$sessionRole = UsersRoole::getRooleId(Auth::id());
$role_id = $sessionRole['roole_id'];
if ($role_id == Roole::getRoleId('Studiendekan')) {
    ?>
    @include('elements.superadmin')
<?php
} else if ($role_id == Roole::getRoleId('Admin')) {
    ?>
<?php
} else if ($role_id == Roole::getRoleId('Professor')) {

} else if ($role_id == Roole::getRoleId('Student')) {
    $is_exist = StudyPlan::getIndividualPlan(Auth::id());
    if (empty($is_exist)) {
        $mentor_display = "display: none;";
        $msg = "Please select any Course.";    
    } else{
        $mentor_display = "display: none;";
        if(empty($is_exist['mentor_name'])){
            $mentor_display = "display: block;";
        }    
        $msg = "Please contact with administration for selecting your mentor name.";
    }
    ?>

    <div class="alert alert-danger js-alert-danger"
         style="<?php echo $mentor_display;?> text-align: center; margin-top: 10px; padding-top: 15px">
        <?php echo $msg;?>
    </div>

    <div class="container" xmlns="http://www.w3.org/1999/html">
    <?php
    if (empty($is_exist)) {
        $display = $is_first ? 'display:block;' : 'display:none;';
        $display_tab_con = $is_first ? 'display:none;' : 'display:block;';
        ?>
        <div class="col-md-12 js-instruction" style="<?php echo $display?>">
            <h4 class="text-center"><u>Study Plan Requirements</u></h4>

            <p>
                The ITIS Master Program requires a minimum of 120 credits to be earned over four semesters, consisting
                of the following parts:
            </p>
            <ol>
                <li class=""><strong>Theoretical Foundations:</strong> In this section you have to complete at least 10
                    credits or more.
                </li>
                <li class=""><strong>Research Focus: </strong> In this section you have to complete at least 10 credits
                    or more.
                </li>
                <li class=""><strong>Networking and communication: </strong>In this section you have to complete at
                    least 10 credits or more.
                </li>
                <li class=""><strong>Business and law: </strong>In this section you have to complete at least 5 credits
                    or more.
                </li>
                <li class=""><strong>Soft skills: </strong>In this section you have to complete at least 5 credits or
                    more.
                </li>
                <li class=""><strong>Research Project(s): </strong>This section consists of two parts</li>
                <ol type="a">
                    <li><strong>Individual Research Project(s): </strong>30 Credits</li>
                    <li><strong>Additional Modules on Research Focus: </strong>15-20 Credits</li>
                </ol>
                <li class=""><strong>Master Thesis: </strong>30 Credits</li>
            </ol>
            <hr>
            <div class="col-md-5 pull-right"><b>Total: 120 Credits</b></div>

            <div class="col-md-12" style="margin-top: 8px"><b>N.B There are some required fields (*) and optional
                    fields. Please try to fill up these fields carefully.</b></div>
            <div class="col-md-5 pull-right" style="margin-top: 15px">
                <button type="button" class="btn btn-info js-goto-studyplan">Go To Study Plan</button>
            </div>
        </div>

        <div role="tabpanel" class="js-main-content" style="<?php echo $display_tab_con?> margin-top:15px;">

        <!-- Nav tabs -->
        <ul class="nav nav-tabs js-nav" role="tablist">
            <li role="presentation"
                id="tab_1" <?php if ($tab == 1) echo "class=active"; else echo "class=disabledTab" ?>><a
                    href="#home" aria-controls="home" role="tab"
                    data-toggle="tab">Semester 1</a></li>
            <li role="presentation"
                id="tab_2" <?php if ($tab == 2) echo "class=active"; else echo "class=disabledTab" ?>><a
                    href="#profile" aria-controls="profile" role="tab"
                    data-toggle="tab">Semester 2</a></li>
            <li role="presentation"
                id="tab_3" <?php if ($tab == 3) echo "class=active"; else echo "class=disabledTab" ?>><a
                    href="#messages" aria-controls="messages"
                    role="tab" data-toggle="tab">Semester 3</a>
            </li>
            <li role="presentation"
                id="tab_4" <?php if ($tab == 4) echo "class=active"; else echo "class=disabledTab" ?>><a
                    href="#settings" aria-controls="settings"
                    role="tab" data-toggle="tab">Semester 4</a>
            </li>
            <li role="presentation" class="disabledTab" id="tab_5"><a href="#settings" aria-controls="settings"
                                                                      role="tab" data-toggle="tab">Plan
                    Details</a>
            </li>
            <!-- <li role="presentation" class="disabledTab" id="tab_6"><a href="#settings" aria-controls="settings"
                                                                      role="tab"
                                                                      data-toggle="tab">Finish</a>
            </li> -->
        </ul>

        <!-- Tab panes -->
        <div class="tab-content my-tab">
        
        <!--<div class="alert alert-success js-alert-danger" style="text-align: center; display:none; background-color:#AEDB43 !important; margin-top: 10px; padding-top: 15px;">
            Please select any Course.
        </div>-->
        <div role="tabpanel" class="tab-pane <?php if ($tab == 1) echo "active"; ?>" id="home">
            <form class="form-inline" method="post" action="studyplan" id="study-plan">
                <div class="form-group">
                    <input type="hidden" name="tab" value="1">
                    <!-- <select name="semester_1" class="form-control input-sm js-semester" id="sem_1_ses">
                        <option
                            value="SoSe 2012" <?php if ($semester_1_name == 'SoSe 2012') echo "selected=selected" ?>>
                            2012 SS
                        </option>
                        <option
                            value="WiSe 2012/13" <?php if ($semester_1_name == 'WiSe 2012/13') echo "selected=selected" ?>>
                            2012 WS
                        </option>
                        <option
                            value="SoSe 2013" <?php if ($semester_1_name == 'SoSe 2013') echo "selected=selected" ?>>
                            2013 SS
                        </option>
                        <option
                            value="WiSe 2013/14" <?php if ($semester_1_name == 'WiSe 2013/14') echo "selected=selected" ?>>
                            2013 WS
                        </option>
                        <option
                            value="SoSe 2014" <?php if ($semester_1_name == 'SoSe 2014') echo "selected=selected" ?>>
                            2014 SS
                        </option>
                        <option
                            value="WiSe 2014/15" <?php if ($semester_1_name == 'WiSe 2014/15') echo "selected=selected" ?>>
                            2014 WS
                        </option>
                        <option
                            value="SoSe 2015" <?php if ($semester_1_name == 'SoSe 2015') echo "selected=selected" ?>>
                            2015 SS
                        </option>
                        <option
                            value="WiSe 2015/16" <?php if ($semester_1_name == 'WiSe 2015/16') echo "selected=selected" ?>>
                            2015 WS
                        </option>
                        <option
                            value="SoSe 2016" <?php if ($semester_1_name == 'SoSe 2016') echo "selected=selected" ?>>
                            2016 SS
                        </option>
                        <option
                            value="WiSe 2016/17" <?php if ($semester_1_name == 'WiSe 2016/17') echo "selected=selected" ?>>
                            2016 WS
                        </option>
                        <option
                            value="SoSe 2017" <?php if ($semester_1_name == 'SoSe 2017') echo "selected=selected" ?>>
                            2017 SS
                        </option>
                        <option
                            value="WiSe 2017/18" <?php if ($semester_1_name == 'WiSe 2017/18') echo "selected=selected" ?>>
                            2017 WS
                        </option>
                        <option
                            value="SoSe 2018" <?php if ($semester_1_name == 'SoSe 2018') echo "selected=selected" ?>>
                            2018 SS
                        </option>
                        <option
                            value="WiSe 2018/19" <?php if ($semester_1_name == 'WiSe 2018/19') echo "selected=selected" ?>>
                            2018 WS
                        </option>
                    </select> -->
                    <input type="hidden" id="sem_1_ses" value="<?php echo $semester_1_name;?>">
                    <?php $c_s_name = convertSemesterName($semester_1_name); echo "<span class=\"form-control input-sm js-semester\">{$c_s_name}</span>";?>
                </div>
            </form>
            <form id="form_1">
                <table class="table table-bordered">
                    <tr>
                        <th>Category</th>
                        <th>Module</th>
                        <th>Exam ID</th>
                        <th>Course<span class="red">(*)</span></th>
                    </tr>
                    <?php
                    ################################################ Dynamic table create for courses ###################################################
                    if (!empty($data)) {
                        foreach ($data as $key => $modules) {
                            #BaseController::_setTrace($modules);
                            ?>
                            <tr>
                                <td class="category">
                                    <?php
                                    echo strlen($modules[0][0]['category_name']) > 60 ? HelperActions::makeMultipleLineString($modules[0][0]['category_name'], 40) : $modules[0][0]['category_name'];
                                    ?>
                                </td>
                                <td class="category">
                                    <?php
                                    $items = array();
                                    foreach ($modules as $item) {
                                        $module_name = strlen($item[0]['module']) > 60 ? HelperActions::makeMultipleLineString($item[0]['module'], 40) : $item[0]['module'];
                                        array_push($items, $module_name);
                                        //echo '<span class="category">'.$item[0]['module'].'</span><hr>';
                                    }
                                    echo implode('<hr>', $items);
                                    ?>
                                </td>
                                <td>
                                    <?php
                                    foreach ($modules as $key_exam => $item) {
                                        $hr1 = '';
                                        $hr2 = '';
                                        if($key_exam < count($modules) - 1) $hr1 = '<hr>';
                                        foreach ($item as $key_item => $new) {
                                            if($key_item < count($item) - 1) $hr2 = '<hr>';
                                            echo $new['moduleexamid'];
                                            echo $hr2;
                                        }
                                        echo $hr1;
                                    }
                                    ?>
                                </td>
                                <td>
                                    <?php
                                    foreach ($modules as $key2 => $item) {
                                        $hr = '';
                                        if ($key2 < count($modules) - 1) $hr = '<hr>';
                                        ?>
                                        <table class="table table-bordered inner-table">
                                            <?php
                                            foreach ($item as $new) {
                                                ?>
                                                <tr class="info">
                                                    <td rel="<?php echo $new['id']; ?>"  class="course_td"><input type="checkbox"
                                                                                               name="course" class="course_1"
                                                                                               id="course_1" required
                                                                                               value="<?php echo $new['module_id'] ?>">
                                                        <?php
                                                        echo strlen($new['course']) > 60 ? HelperActions::makeMultipleLineString($new['course'], 40) : $new['course'];
                                                        ?>
                                                    </td>
                                                    <td rel="<?php echo $new['credits']; ?>" class="text-center">
                                                        <?php
                                                        echo $new['credits'];
                                                        ?>
                                                    </td>
                                                </tr>
                                            <?php
                                            }
                                            ?>
                                        </table>
                                        <?php
                                        echo $hr;
                                    }
                                    ?>
                                </td>
                            </tr>
                        <?php
                        }
                    } else {
                        ?>
                        <tr class="warning">
                            <td>No Result Found.</td>
                            <td>No Result Found.</td>
                            <td>No Result Found.</td>
                            <td>No Result Found.</td>
                        </tr>
                    <?php
                    }
                    ?>
                </table>
            </form>
            <table class="table table-bordered">
                <tr>
                    <td>
                        <div><strong>Soft skill(optional)</strong></div>
                        <div class="js-soft-skill-1">
                            <div class="row js-single-skill-1">
                                <div class="col-xs-3">
                                    {{ Form::select('site_1[]', Site::makeSiteArray(),
                                    null, array('class' =>'form-control','id'=> 'site_1')) }}
                                </div>
                                <div class="col-xs-6">
                                    <input type="text" class="form-control" placeholder="Course Title" id="soft_skill_1"
                                           name="soft_skill_1[]">
                                </div>
                                <div class="col-xs-2">
                                    <select id="credit_1" class="form-control" name="credit_1[]">
                                        <option value="">Credit</option>
                                        <?php
                                        for ($i = 1; $i <= 8; $i++) {
                                            ?>
                                            <option value="<?php echo $i ?>"><?php echo $i ?> </option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="col-xs-1">
                                    {{ Form::button('Add', ['class' => 'btn btn-primary btn-sm add-btn
                                    js-add-new-btn-1']) }}
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
            <div class="pull-right">
                <?php if (!empty($data)) { ?>
                <p>{{ Form::button('Next', ['class' => 'btn btn-primary btn-sm js-step']) }}</p>
                <?php } else { ?>
                <p>{{ Form::button('Skip', ['class' => 'btn btn-primary btn-sm js-step']) }}</p>
                <?php } ?>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane <?php if ($tab == 2) echo "active"; ?>" id="profile">
            <form class="form-inline" method="post" action="studyplan" id="study-plan">
                <div class="form-group">
                    <input type="hidden" name="tab" value="2">
                    <!-- <select name="semester_2" class="form-control input-sm js-semester" id="sem_2_ses">
                        <option
                            value="SoSe 2012" <?php if ($semester_2_name == 'SoSe 2012') echo "selected=selected" ?>>
                            2012 SS
                        </option>
                        <option
                            value="WiSe 2012/13" <?php if ($semester_2_name == 'WiSe 2012/13') echo "selected=selected" ?>>
                            2012 WS
                        </option>
                        <option
                            value="SoSe 2013" <?php if ($semester_2_name == 'SoSe 2013') echo "selected=selected" ?>>
                            2013 SS
                        </option>
                        <option
                            value="WiSe 2013/14" <?php if ($semester_2_name == 'WiSe 2013/14') echo "selected=selected" ?>>
                            2013 WS
                        </option>
                        <option
                            value="SoSe 2014" <?php if ($semester_2_name == 'SoSe 2014') echo "selected=selected" ?>>
                            2014 SS
                        </option>
                        <option
                            value="WiSe 2014/15" <?php if ($semester_2_name == 'WiSe 2014/15') echo "selected=selected" ?>>
                            2014 WS
                        </option>
                        <option
                            value="SoSe 2015" <?php if ($semester_2_name == 'SoSe 2015') echo "selected=selected" ?>>
                            2015 SS
                        </option>
                        <option
                            value="WiSe 2015/16" <?php if ($semester_2_name == 'WiSe 2015/16') echo "selected=selected" ?>>
                            2015 WS
                        </option>
                        <option
                            value="SoSe 2016" <?php if ($semester_2_name == 'SoSe 2016') echo "selected=selected" ?>>
                            2016 SS
                        </option>
                        <option
                            value="WiSe 2016/17" <?php if ($semester_2_name == 'WiSe 2016/17') echo "selected=selected" ?>>
                            2016 WS
                        </option>
                        <option
                            value="SoSe 2017" <?php if ($semester_2_name == 'SoSe 2017') echo "selected=selected" ?>>
                            2017 SS
                        </option>
                        <option
                            value="WiSe 2017/18" <?php if ($semester_2_name == 'WiSe 2017/18') echo "selected=selected" ?>>
                            2017 WS
                        </option>
                        <option
                            value="SoSe 2018" <?php if ($semester_2_name == 'SoSe 2018') echo "selected=selected" ?>>
                            2018 SS
                        </option>
                        <option
                            value="WiSe 2018/19" <?php if ($semester_2_name == 'WiSe 2018/19') echo "selected=selected" ?>>
                            2018 WS
                        </option>
                    </select> -->
                    <input type="hidden" id="sem_2_ses" value="<?php echo $semester_2_name;?>">
                    <?php $c_s_name = convertSemesterName($semester_2_name); echo "<span class=\"form-control input-sm js-semester\">{$c_s_name}</span>";?>
                </div>
            </form>
            <form id="form_2">
                <table class="table table-bordered">
                    <tr>
                        <th>Category</th>
                        <th>Module</th>
                        <th>Exam ID</th>
                        <th>Course<span class="red">(*)</span></th>
                    </tr>
                    <?php
                    ################################################ Dynamic table create for courses ###################################################
                    if (!empty($semester_2_data)) {
                        foreach ($semester_2_data as $key => $modules) {
                            #BaseController::_setTrace($modules);
                            ?>
                            <tr>
                                <td class="category">
                                    <?php
                                    echo strlen($modules[0][0]['category_name']) > 60 ? HelperActions::makeMultipleLineString($modules[0][0]['category_name'], 40) : $modules[0][0]['category_name'];
                                    ?>
                                </td>
                                <td class="category">
                                    <?php
                                    $items = array();
                                    foreach ($modules as $item) {
                                        $module_name = strlen($item[0]['module']) > 60 ? HelperActions::makeMultipleLineString($item[0]['module'], 40) : $item[0]['module'];
                                        array_push($items, $module_name);
                                    }
                                    echo implode('<hr>', $items);
                                    ?>
                                </td>
                                <td>
                                    <?php
                                    foreach ($modules as $key_exam => $item) {
                                        $hr1 = '';
                                        $hr2 = '';
                                        if($key_exam < count($modules) - 1) $hr1 = '<hr>';
                                        foreach ($item as $key_item => $new) {
                                            if($key_item < count($item) - 1) $hr2 = '<hr>';
                                            echo $new['moduleexamid'];
                                            echo $hr2;
                                        }
                                        echo $hr1;
                                    }
                                    ?>
                                </td>
                                <td>
                                    <?php
                                    foreach ($modules as $key2 => $item) {
                                        $hr = '';
                                        if ($key2 < count($modules) - 1) $hr = '<hr>';
                                        ?>
                                        <table class="table table-bordered inner-table">
                                            <?php
                                            foreach ($item as $new) {
                                                ?>
                                                <tr class="info">
                                                    <td rel="<?php echo $new['id']; ?>" class="course_td"><input type="checkbox"
                                                                                               name="course" class="course_2"
                                                                                               id="course_2" required
                                                                                               value="<?php echo $new['module_id'] ?>">
                                                        <?php echo strlen($new['course']) > 60 ? HelperActions::makeMultipleLineString($new['course'], 40) : $new['course']; ?>
                                                    </td>
                                                    <td rel="<?php echo $new['credits'] ?>" class="text-center">
                                                        <?php
                                                        echo $new['credits'];
                                                        ?>
                                                    </td>
                                                </tr>
                                            <?php
                                            }
                                            ?>
                                        </table>
                                        <?php
                                        echo $hr;
                                    }
                                    ?>
                                </td>
                            </tr>
                        <?php
                        }
                    } else {
                        ?>
                        <tr class="warning">
                            <td>No Result Found.</td>
                            <td>No Result Found.</td>
                            <td>No Result Found.</td>
                            <td>No Result Found.</td>
                        </tr>
                    <?php
                    }
                    ?>
                </table>
            </form>
            <table class="table table-bordered">
                <tr>
                    <td>
                        <div><strong>Soft skill(optional)</strong></div>
                        <div class="js-soft-skill-2">
                            <div class="row js-single-skill-2">
                                <div class="col-xs-3">
                                    {{ Form::select('site_2[]', Site::makeSiteArray(),
                                    null, array('class' =>'form-control','id'=> 'site_2')) }}
                                </div>
                                <div class="col-xs-6">
                                    <input type="text" class="form-control" placeholder="Course Title" id="soft_skill_2"
                                           name="soft_skill_2[]">
                                </div>
                                <div class="col-xs-2">
                                    <select id="credit_2" class="form-control" name="credit_2[]">
                                        <option value="">Credit</option>
                                        <?php
                                        for ($i = 1; $i <= 8; $i++) {
                                            ?>
                                            <option value="<?php echo $i ?>"><?php echo $i ?> </option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="col-xs-1">
                                    {{ Form::button('Add', ['class' => 'btn btn-primary btn-sm add-btn
                                    js-add-new-btn-2']) }}
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
            <table class="table table-bordered">
                <tr>
                    <td>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Project Title</label>
                            <input type="text" class="form-control" id="semester-2-project"
                                   placeholder="Enter Your Project Title, It's optional">
                        </div>
                    </td>
                </tr>
            </table>
            <div class="pull-left">
                <p>{{ Form::button('Back', ['class' => 'btn btn-primary btn-sm js-back-btn']) }}</p>
            </div>
            <div class="pull-right">
                <?php if (!empty($semester_2_data)) { ?>
                <p>{{ Form::button('Next', ['class' => 'btn btn-primary btn-sm js-step']) }}</p>
                <?php } else { ?>
                <p>{{ Form::button('Skip', ['class' => 'btn btn-primary btn-sm js-step']) }}</p>
                <?php } ?>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane <?php if ($tab == 3) echo "active"; ?>" id="messages">
            <form class="form-inline" method="post" action="studyplan" id="study-plan">
                <div class="form-group">
                    <input type="hidden" name="tab" value="3">
                    <!-- <select name="semester_3" class="form-control input-sm js-semester" id="sem_3_ses">
                        <option
                            value="SoSe 2012" <?php if ($semester_1_name == 'SoSe 2012') echo "selected=selected" ?>>
                            2012 SS
                        </option>
                        <option
                            value="WiSe 2012/13" <?php if ($semester_1_name == 'WiSe 2012/13') echo "selected=selected" ?>>
                            2012 WS
                        </option>
                        <option
                            value="SoSe 2013" <?php if ($semester_1_name == 'SoSe 2013') echo "selected=selected" ?>>
                            2013 SS
                        </option>
                        <option
                            value="WiSe 2013/14" <?php if ($semester_1_name == 'WiSe 2013/14') echo "selected=selected" ?>>
                            2013 WS
                        </option>
                        <option
                            value="SoSe 2014" <?php if ($semester_1_name == 'SoSe 2014') echo "selected=selected" ?>>
                            2014 SS
                        </option>
                        <option
                            value="WiSe 2014/15" <?php if ($semester_1_name == 'WiSe 2014/15') echo "selected=selected" ?>>
                            2014 WS
                        </option>
                        <option
                            value="SoSe 2015" <?php if ($semester_1_name == 'SoSe 2015') echo "selected=selected" ?>>
                            2015 SS
                        </option>
                        <option
                            value="WiSe 2015/16" <?php if ($semester_1_name == 'WiSe 2015/16') echo "selected=selected" ?>>
                            2015 WS
                        </option>
                        <option
                            value="SoSe 2016" <?php if ($semester_1_name == 'SoSe 2016') echo "selected=selected" ?>>
                            2016 SS
                        </option>
                        <option
                            value="WiSe 2016/17" <?php if ($semester_1_name == 'WiSe 2016/17') echo "selected=selected" ?>>
                            2016 WS
                        </option>
                        <option
                            value="SoSe 2017" <?php if ($semester_1_name == 'SoSe 2017') echo "selected=selected" ?>>
                            2017 SS
                        </option>
                        <option
                            value="WiSe 2017/18" <?php if ($semester_1_name == 'WiSe 2017/18') echo "selected=selected" ?>>
                            2017 WS
                        </option>
                        <option
                            value="SoSe 2018" <?php if ($semester_1_name == 'SoSe 2018') echo "selected=selected" ?>>
                            2018 SS
                        </option>
                        <option
                            value="WiSe 2018/19" <?php if ($semester_1_name == 'WiSe 2018/19') echo "selected=selected" ?>>
                            2018 WS
                        </option>
                    </select> -->
                    <input type="hidden" id="sem_3_ses" value="<?php echo $semester_3_name;?>">
                    <?php $c_s_name = convertSemesterName($semester_3_name); echo "<span class=\"form-control input-sm js-semester\">{$c_s_name}</span>";?>
                </div>
            </form>
            <form id="form_3">
                <table class="table table-bordered">
                    <tr>
                        <th>Category</th>
                        <th>Module</th>
                        <th>Exam ID</th>
                        <th>Course</th>
                    </tr>
                    <?php
                    ################################################ Dynamic table create for courses ###################################################
                    if (!empty($semester_3_data)) {
                        foreach ($semester_3_data as $key => $modules) {
                            #BaseController::_setTrace($modules);
                            ?>
                            <tr>
                                <td class="category">
                                    <?php
                                    echo strlen($modules[0][0]['category_name']) > 60 ? HelperActions::makeMultipleLineString($modules[0][0]['category_name'], 40) : $modules[0][0]['category_name'];
                                    ?>
                                </td>
                                <td class="category">
                                    <?php
                                    $items = array();
                                    foreach ($modules as $item) {
                                        $module_name = strlen($item[0]['module']) > 60 ? HelperActions::makeMultipleLineString($item[0]['module'], 40) : $item[0]['module'];
                                        array_push($items, $module_name);
                                    }
                                    echo implode('<hr>', $items);
                                    ?>
                                </td>
                                <td>
                                    <?php
                                    foreach ($modules as $key_exam => $item) {
                                        $hr1 = '';
                                        $hr2 = '';
                                        if($key_exam < count($modules) - 1) $hr1 = '<hr>';
                                        foreach ($item as $key_item => $new) {
                                            if($key_item < count($item) - 1) $hr2 = '<hr>';
                                            echo $new['moduleexamid'];
                                            echo $hr2;
                                        }
                                        echo $hr1;
                                    }
                                    ?>
                                </td>
                                <td>
                                    <?php
                                    foreach ($modules as $key2 => $item) {
                                        $hr = '';
                                        if ($key2 < count($modules) - 1) $hr = '<hr>';
                                        ?>
                                        <table class="table table-bordered inner-table">
                                            <?php
                                            foreach ($item as $new) {
                                                ?>
                                                <tr class="info">
                                                    <td rel="<?php echo $new['credits'] ?>" class="course_td"><input type="checkbox"
                                                                                                   name="course" class="course_3"
                                                                                                   id="course_3"
                                                                                                   value="<?php echo $new['module_id'] ?>">
                                                        <?php
                                                        echo strlen($new['course']) > 60 ? HelperActions::makeMultipleLineString($new['course'], 40) : $new['course'];
                                                        ?>
                                                    </td>
                                                    <td rel="<?php echo $new['credits'] ?>" class="text-center">
                                                        <?php
                                                        echo $new['credits'];
                                                        ?>
                                                    </td>
                                                </tr>
                                            <?php
                                            }
                                            ?>
                                        </table>
                                        <?php
                                        echo $hr;
                                    }
                                    ?>
                                </td>
                            </tr>
                        <?php
                        }
                    } else { ?>
                        <tr class="warning">
                            <td>No Result Found.</td>
                            <td>No Result Found.</td>
                            <td>No Result Found.</td>
                            <td>No Result Found.</td>
                        </tr>
                    <?php } ?>
                </table>
            </form>
            <table class="table table-bordered">
                <tr>
                    <td>
                        <div><strong>Soft skill(Optional)</strong></div>
                        <div class="js-soft-skill-3">
                            <div class="row js-single-skill-3">
                                <div class="col-xs-3">
                                    {{ Form::select('site_3[]', Site::makeSiteArray(),
                                    null, array('class' =>'form-control','id'=> 'site_3')) }}
                                </div>
                                <div class="col-xs-6">
                                    <input type="text" class="form-control" placeholder="Course Title" id="soft_skill_3"
                                           name="soft_skill_3[]">
                                </div>
                                <div class="col-xs-2">
                                    <select id="credit_3" class="form-control" name="credit_3[]">
                                        <option value="">Credit</option>
                                        <?php
                                        for ($i = 1; $i <= 8; $i++) {
                                            ?>
                                            <option value="<?php echo $i ?>"><?php echo $i ?> </option>
                                        <?php
                                        }
                                        ?>

                                    </select>
                                </div>
                                <div class="col-xs-1">
                                    {{ Form::button('Add', ['class' => 'btn btn-primary btn-sm add-btn
                                    js-add-new-btn-3']) }}
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
            <table class="table table-bordered">
                <tr>
                    <td>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Project Title</label>
                            <input type="text" class="form-control" id="project_name_1"
                                   placeholder="Enter Your Project Title">
                        </div>
                    </td>
                </tr>
            </table>

            <div class="pull-left">
                <p>{{ Form::button('Back', ['class' => 'btn btn-primary btn-sm js-back-btn']) }}</p>
            </div>
            <div class="pull-right">
                <?php if (!empty($semester_3_data)) { ?>
                <p>{{ Form::button('Next', ['class' => 'btn btn-primary btn-sm js-step']) }}</p>
                <?php } else { ?>
                <p>{{ Form::button('Skip', ['class' => 'btn btn-primary btn-sm js-step']) }}</p>
                <?php } ?>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane <?php if ($tab == 4) echo "active"; ?>" id="settings">
            <table class="table table-bordered">
                <tr>
                    <td>
                        <form id="form_4">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Theses Title</label>
                                <input type="text" class="form-control" id="project_name_2"
                                       placeholder="Enter Your Theses Title">
                            </div>
                        </form>
                    </td>
                </tr>
            </table>
            <div class="pull-left">
                <p>{{ Form::button('Back', ['class' => 'btn btn-primary btn-sm js-back-btn']) }}</p>
            </div>
            <div class="pull-right">
                <p>{{ Form::button('Skip', ['class' => 'btn btn-primary btn-sm js-step', 'id' => 'js-step-4']) }}</p>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane <?php if ($tab == 5) echo "active"; ?>" id="plan_details">
            <div class="row" style="padding-bottom: 10px">
                <div class="col-sm-12 text-center"><u><strong>Individual Study Plan for the Master’s Program “Internet
                            Technologies
                            and Information Systems“(ITIS)</strong></u></div>
                <?php
                $user = User::getUserInfo(Auth::id());
                ?>
                <div class="col-sm-3"><strong>Student Name:</strong></div>
                <div
                    class="col-sm-9"><?php echo $user['title'] . '. ' . $user['first_name'] . ' ' . $user['last_name'] ?></div>
                <div class="col-sm-3"><strong>Hannover (!) Matriculation Number:</strong></div>
                <div class="col-sm-9">xxxxxxxxxxxxxxxx</div>
                <div class="col-sm-3"><strong>University:</strong></div>
                <div class="col-sm-9"><?php echo $user['university'] ?></div>
                <div class="col-sm-3"><strong>E-Mail:</strong></div>
                <div class="col-sm-9"><?php echo $user['email'] ?></div>
                <div class="col-sm-3"><strong>Mentor Name:</strong></div>
                <div class="col-sm-9">
                    Please contact with administration for selecting your mentor name.
                </div>
            </div>
            <table class="table table-bordered js-plan-details">
                <!--    Dynamic Plan details table created here from Js -->
            </table>
            <div class="pull-left">
                <p>{{ Form::button('Back', ['class' => 'btn btn-primary btn-sm js-back-btn']) }}</p>
            </div>
            <div class="pull-right">
                <p>{{ Form::button('Save & Submit', ['class' => 'btn btn-primary btn-sm js-step']) }}</p>
            </div>
        </div>
        <!-- <div role="tabpanel" class="tab-pane <?php if ($tab == 6) echo "active"; ?>" id="finish">
            <div class="pull-left">
                <p>{{ Form::button('Back', ['class' => 'btn btn-primary btn-sm js-back-btn']) }}</p>
            </div>
        </div> -->
        </div>
        </div>
    <?php
    } else {
        #BaseController::_setTrace($is_exist);
        ?>
        <div role="tabpanel" class="tab-pane" id="plan_details" style="margin-top: 10px;">
            <div class="row" style="padding-bottom: 10px">
                <div class="col-sm-12 text-center"><u><strong>Individual Study Plan for the Master’s Program “Internet
                            Technologies
                            and Information Systems“(ITIS)</strong></u></div>
                <?php
                $user = User::getUserInfo(Auth::id());
                ?>
                <div class="col-sm-3"><strong>Student Name:</strong></div>
                <div
                    class="col-sm-9"><?php echo $user['title'] . '. ' . $user['first_name'] . ' ' . $user['last_name'] ?></div>
                <div class="col-sm-3"><strong>Hannover (!) Matriculation Number:</strong></div>
                <div class="col-sm-9">xxxxxxxxxxxxxxxx</div>
                <div class="col-sm-3"><strong>University:</strong></div>
                <div class="col-sm-9"><?php echo $user['university'] ?></div>
                <div class="col-sm-3"><strong>E-Mail:</strong></div>
                <div class="col-sm-9"><?php echo $user['email'] ?></div>
                <?php
                if(!empty($is_exist['mentor_name'])){
                  echo '<div class="col-sm-3"><strong>Mentor Name:</strong></div><div class="col-sm-9">'.$is_exist['mentor_name'].'</div>';
                }
                ?>

            </div>
            <table class="table table-bordered js-plan-details">
                
                <script type="text/javascript">
                    var modules_all = <?php echo json_encode($modules_all)?>
                </script>
                <script type="text/javascript">
                    var semester_1 = <?php echo json_encode($course_sem_1)?>
                </script>
                <script type="text/javascript">
                    var semester_1_new = <?php echo json_encode($course_sem_1)?>
                </script>
                <script type="text/javascript">
                    var semester_2 = <?php echo json_encode($course_sem_2)?>
                </script>
                <script type="text/javascript">
                    var semester_2_new = <?php echo json_encode($course_sem_2)?>
                </script>
                <script type="text/javascript">
                    var semester_3_course = <?php echo json_encode($course_sem_3)?>
                </script>
                <script type="text/javascript">
                    var semester_3_new = <?php echo json_encode($course_sem_3)?>
                </script>

                <script type="text/javascript">
                    var skill_1 = <?php echo json_encode($my_softskill_sem_1)?>
                </script>
                <script type="text/javascript">
                    var skill_2 = <?php echo json_encode($my_softskill_sem_2)?>
                </script>
                <script type="text/javascript">
                    var skill_3 = <?php echo json_encode($my_softskill_sem_3)?>
                </script>

                <script type="text/javascript">
                    var credit_1 = <?php echo json_encode($credit_1)?>
                </script>
                <script type="text/javascript">
                    var credit_2 = <?php echo json_encode($credit_2)?>
                </script>
                <script type="text/javascript">
                    var credit_3 = <?php echo json_encode($credit_3)?>
                </script>              

                <script type="text/javascript">
                    var skill_1 = <?php echo json_encode($my_softskill_sem_1)?>
                </script>
                <script type="text/javascript">
                    var skill_2 = <?php echo json_encode($my_softskill_sem_2)?>
                </script>
                <script type="text/javascript">
                    var skill_3 = <?php echo json_encode($my_softskill_sem_3)?>
                </script>
                <script type="text/javascript">
                    localStorage.setItem('semester_1', JSON.stringify(semester_1_new));
                    //localStorage.setItem('session_1', JSON.stringify(session_1));
                    localStorage.setItem('credits_1', JSON.stringify(credit_1));
                    localStorage.setItem('modules_1', JSON.stringify(modules_1_new));
                    localStorage.setItem('semester_2', JSON.stringify(semester_2_new));
                    //localStorage.setItem('session_2', JSON.stringify(session_2));
                    localStorage.setItem('credits_2', JSON.stringify(credit_2));
                    localStorage.setItem('modules_2', JSON.stringify(modules_2_new));
                    localStorage.setItem('semester_3', JSON.stringify(semester_3_new));
                    //localStorage.setItem('session_3', JSON.stringify(session_3));
                    localStorage.setItem('credits_3', JSON.stringify(credit_3));
                    console.log(localStorage);
                </script>
                <!--    Dynamic Plan details table created here from Js -->

                <script type="text/javascript">
                    var semester_2_project = "<?php echo $my_project_2?>";
                    var semester_3 = "<?php echo $my_project_3?>";
                    var semester_4 = "<?php echo $theses_name?>";
                </script>
                <script type="text/javascript">
                    var user_id = <?php echo $is_exist['id']?>
                </script>

                <!--<tbody>
                <tr class="style6">
                    <th class="text-center category">Semester</th>
                    <th colspan="5" class="text-center category">Core Areas (40 C – 50 C)</th>
                    <th colspan="2" class="text-center category">Research Focus (45 C – 50 C)</th>
                    <th class="text-center category">Master Theses (30 C)</th>
                </tr>
                <tr>
                    <td></td>
                    <td class="category">Theoretical Foundations(min. 10 C)</td>
                    <td class="category">Data Information(min. 10 C)</td>
                    <td class="category">Networking and Communication(min. 10 C)</td>
                    <td class="category">Business and Law(min. 5 C)</td>
                    <td class="category">Soft Skills( 5 C)</td>
                    <td class="category">Additional Modules on Research Focus(15 C – 20 C)</td>
                    <td class="category">Individual Research Project(s)(30 C)</td>
                    <td class="category">Master Theses (30 C)</td>
                </tr>
                <tr>
                    <th class="category">1.Semester</th>
                    ' + html_1 + html_2 + html_3 + html_4 + '
                    <td class="category">' + soft_skill + '</td>
                    <td class="category"></td>
                    <td class="category">' + project_Title + '</td>
                    <td class="category"></td>
                </tr>
                </tbody>-->

            </table>
        </div>
    <?php
    }
    ?>
    </div>
    <div class="col-sm-1"></div>
    <div class="overlay">
        <div class="please_wait">Loading</div>
        <div id="loader_icon"></div>
    </div>
<?php
}
?>
@stop
<style type="text/css">
    .navbar{
         margin-bottom: 0px !important;
    }
</style>