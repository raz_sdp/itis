@extends('layouts.main')

@section('content')
@include('layouts.navBar')

@if($successMsg)
	<div class="col-sm-12 col-md-12 col-lg-12 alert alert-success" style="background-color: #AEDB43;padding: 0;margin-top: -20;">
		<p>
			<p> <b><center>
        <span class="glyphicon glyphicon-ok"></span>
				success
			</center></b> </p>
			<p><center>Your Changes updated successfully :)</center></p>
		</p>
	</div>
@endif 

<div class = "container">

<div class = "row">
	<div class = "col-sm-9"></div>
</div>

<div class="raw" id="no-more-tables">
	<!-- @if(isset($str))
		<h4 style = "color: #686868;"> {{$str}} </h4>
	@endif -->
	<table class="col-md-12  col-lg-12 table table-hover cf">
		<thead class="cf">
			<tr>
				<td><b>Change</b></td>
				<td><b>Table</b></td>
				<td><b>Field</b></td>
				<td><b>Done By</b></td>
				<td><b>Happened</b></td>
			</tr>
		</thead>
		<tbody>

				@foreach ($logs as $log)
					<tr>
							<td data-title="Change">
							{{ strlen($log->change_string) > 40 ? HelperActions::makeMultipleLineString($log->change_string, 40) : $log->change_string }}
							</td>
							<td data-title="Table">
								{{ $log->table_name }}
							</td>
							<td data-title="Field">
								{{ $log->field_name }}
							</td>
							<td data-title="Done By">
								{{ $log->done_by }}
							</td>
							<td data-title="Happened">
								{{ $log->created_at }}
							</td>
						
							@if(!$log->created && !$log->deleted && $log->raw_data_string)
								<td style = "padding-right: 0px;">
									{{--*/ $vurl = 'viewlog/'.$log->id.'/'.$log->raw_id_in_table.'/'.$log->table_name.'/'.HelperActions::getPageNumber($_SERVER['REQUEST_URI']) /*--}}
									{{ Form::open(array('url' => $vurl)) }}
									  <button type="submit" class="btn btn-default btn-xs">
									  	<span class="glyphicon glyphicon-eye-open"></span>
									  	View
									  </button>
									{{ Form::close() }}
								</td>

								<td>
									{{--*/ $unurl = 'undologs/'.$log->id.'/'.$log->raw_id_in_table.'/'.$log->table_name.'/'.HelperActions::getPageNumber($_SERVER['REQUEST_URI']) /*--}}
									{{ Form::open(array('url' => $vurl)) }}
									  <button type="submit" class="btn btn-default btn-xs">
									  	<span class="glyphicon glyphicon-repeat"></span>
									  	Undo
									  </button>
									{{ Form::close() }}
								</td>
							@elseif($log->deleted)
								<td class = "padding-left: 2px;">
									<center>
									{{--*/ $udurl = 'undodeletebylogs/'.$log->raw_id_in_table.'/'.$log->table_name.'/'.HelperActions::getPageNumber($_SERVER['REQUEST_URI']) /*--}}
									{{ Form::open(array('url' => $udurl)) }}
									  <button type="submit" class="btn btn-default btn-xs">
									  	<span class="glyphicon glyphicon-refresh"></span>
									  		Undo Delete
									  </button>
									{{ Form::close() }}
									</center>
								</td>
							@elseif($log->created)
								<td class = "padding-left: 2px;">
									<center>
										{{--*/ $deurl = 'deletebylogs/'.$log->raw_id_in_table.'/'.$log->table_name.'/'.HelperActions::getPageNumber($_SERVER['REQUEST_URI']) /*--}}
										{{ Form::open(array('url' => $deurl)) }}
										  <button type="submit" class="btn btn-default btn-xs">
										  	<span class="glyphicon glyphicon-remove"></span>
										  		Delete
										  </button>
										{{ Form::close() }}
									</center>
								</td>							
							@endif
					</tr>
				@endforeach
		
		</tbody>
	</table>
	
	</div> 
	<div class = "raw">
		<?php echo $logs->links(); ?>
	</div>
</div> 
@stop
