@extends('layouts.main')

@section('content')
@include('layouts.navBar')
<div class = "col-sm-2"></div>
<div class = "col-sm-8">
	<div class = "col-sm-8">
		<h3>Detaill View of Log</h3>
		@foreach ($array as $key => $value)
		 
			<table class="table">
				<tr >
					<td class = "col-sm-4"> {{ $key }} </td>
					
					<td class = "col-sm-4"> 
					       
					       @if(($key)=='site')

                           {{Site::getSiteName($value)}}

                           @elseif(($key)=='site_2')
                      
                           {{Site::getSiteName($value)}}

                           @elseif(($key)=='university')

                           {{Site::getSiteName($value)}}

                           @elseif(($key)=='topic_category')

                           {{Category::getCategoryName($value)}}

                           @else

                           {{$value}}

                           @endif

		             </td>
		           
				</tr>
			</table>
			
		@endforeach

		<span class = "pull-right" style = "padding-right: 1px;">
			{{--*/ $lurl = 'undologs/'.$id.'/'.$cid.'/'.$table.'/0' /*--}}
			{{ Form::open(array('url' => $lurl)) }}
			  <button type="submit" class="btn btn-primary btn-sm">
			  	<span class="glyphicon glyphicon-cloud-upload"></span>
			  	Undo to this Stage
			  </button>
			{{ Form::close() }}
		</span>

	</div>
</div>
<div class = "col-sm-2"></div>
@stop
