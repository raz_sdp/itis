@extends('layouts.main')

@section('content')
@include('layouts.navBar')

{{--*/ $msg = HelperActions::getMessageToSession(); /*--}}
@if($msg)
  <div class="col-sm-12 col-md-12 col-lg-12 alert alert-danger" style="background-color: #FF6E6E; padding: 6px; margin-top: -20;">
    <p>
      <p> <b><center>
        <span class="glyphicon glyphicon-remove"></span>
        Not success
      </center></b> </p>
      <p><center>{{ $msg }}</center></p>
    </p>
  </div>
@endif

 <div class = "col-sm-3"></div>
  <div class = "col-sm-6">

    <h3 style = "padding-left: 32px;">Edit Password</h3>
    {{ Form::open(array('url' => 'updatepassword', 'method' => 'post', 'class' => 'form-horizontal')) }}

      <div class="form-group ">
        <label for="" class = "col-sm-3 control-label">Old Password</label>
        <div class="col-sm-8">
          {{ Form::password('password', array('class' => 'form-control')) }}
            @if ($errors->has('password')) <p class="text-danger"><b>{{ $errors->first('password') }}</b></p> @endif
        </div>
      </div>

      <div class="form-group ">
        <label for="" class = "col-sm-3 control-label">New Password</label>
        <div class="col-sm-8">
          {{ Form::password('newPassword', array('class' => 'form-control')) }}
            @if ($errors->has('newPassword')) <p class="text-danger"><b>{{ $errors->first('newPassword') }}</b></p> @endif
        </div>
      </div>

      <div class="form-group ">
        <label for="" class = "col-sm-3 control-label">ReType Password</label>
        <div class="col-sm-8">
          {{ Form::password('conformPassword', array('class' => 'form-control')) }}
            @if ($errors->has('conformPassword')) <p class="text-danger"><b>{{ $errors->first('conformPassword') }}</b></p> @endif
        </div>
      </div> 

      <button type="submit" class="btn btn-primary btn-sm pull-right" style = "margin-right: 57px;">
        <span class="glyphicon glyphicon-cloud-upload"></span>
        Update Password
      </button>           

    {{ Form::close() }}
  </div>
  <div class = "col-sm-3"></div>
  @stop