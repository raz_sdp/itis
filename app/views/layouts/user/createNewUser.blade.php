@extends('layouts.main')

@section('content')
@include('layouts.navBar')
<div class = "container">

<div class = "raw">

  <h3> <center>Add New User</center></h3>
  <hr/>

  {{ Form::open(array('url' => 'createuser', 'method' => 'post', 'class' => 'form-horizontal' )) }}
    <div class="form-group">
      <label for="" class = "col-sm-3 control-label">Title</label>
      <div class="col-sm-8">
      {{ Form::text('title', Input::old('title'), array('placeholder' => '', 'class' =>'form-control ')) }}
         @if ($errors->has('title')) <p class="text-danger"><b>{{ $errors->first('title') }}</b></p> @endif
      </div>
    </div>

    <div class="form-group ">
      <label for="" class = "col-sm-3 control-label">First Name</label>
      <div class="col-sm-8">
        {{ Form::text('first_name', Input::old('first_name'), array('placeholder' => '', 'class' =>'form-control ')) }}
           @if ($errors->has('first_name')) <p class="text-danger"><b>{{ $errors->first('first_name') }}</b></p> @endif
      </div>
    </div>

    <div class="form-group">
      <label for="" class = "col-sm-3 control-label">Last Name</label>
      <div class="col-sm-8">
        {{ Form::text('last_name', Input::old('last_name'), array('placeholder' => '', 'class' =>'form-control ')) }}
           @if ($errors->has('last_name')) <p class="text-danger"><b>{{ $errors->first('last_name') }}</b></p> @endif
      </div>
    </div>


    <div class="form-group">
      <label for="" class = "col-sm-3 control-label">Email</label>
      <div class="col-sm-8">
      {{ Form::text('email', Input::old('email'), array('placeholder' => '', 'class' =>'form-control ')) }}
         @if ($errors->has('email')) <p class="text-danger"><b>{{ $errors->first('email') }}</b></p> @endif
      </div>
    </div>

    <div class="form-group">
      <label for="" class = "col-sm-3 control-label">Telephone Number</label>
      <div class="col-sm-8">
      {{ Form::text('telephone', Input::old('telephone'), array('placeholder' => '', 'class' =>'form-control ')) }}
         @if ($errors->has('telephone')) <p class="text-danger"><b>{{ $errors->first('telephone') }}</b></p> @endif
      </div>
    </div>

    <div class="form-group">
      <label for="" class = "col-sm-3 control-label">Institute</label>
      <div class="col-sm-8">
      {{ Form::text('institute', Input::old('institute'), array('placeholder' => '', 'class' =>'form-control ')) }}
         @if ($errors->has('institute')) <p class="text-danger"><b>{{ $errors->first('institute') }}</b></p> @endif
      </div>
    </div>

    <div class="form-group ">
      <label for="" class = "col-sm-3 control-label">Role Type</label>
      <div class="col-sm-8">
          {{ Form::select('role_id', Roole::getRooles(),
                          null, array('class' =>'form-control input-sm')) }}
          @if ($errors->has('role_id')) <p class="text-danger"><b>{{ $errors->first('role_id') }}</b></p> @endif                                              
      </div>
    </div>

    <div class="form-group ">
      <label for="" class = "col-sm-3 control-label">University</label>
      <div class="col-sm-8">
          {{ Form::select('university', Site::makeSiteArray(),
                            null, array('class' =>'form-control input-sm')) }}
           @if ($errors->has('university')) <p class="text-danger"><b>{{ $errors->first('university') }}</b></p> @endif                                          
      </div>
    </div> 

    <div class="form-group">
      <label for="" class = "col-sm-3 control-label">Institute Website</label>
      <div class="col-sm-8">
      {{ Form::text('institute_url', Input::old('institute_url'), array('placeholder' => '', 'class' =>'form-control ')) }}
         @if ($errors->has('institute_url')) <p class="text-danger"><b>{{ $errors->first('institute_url') }}</b></p> @endif
      </div>
    </div>

    <div class="form-group">
      <label for="" class = "col-sm-3 control-label">Personal Website</label>
      <div class="col-sm-8">
      {{ Form::text('personal_website_url', Input::old('personal_website_url'), array('placeholder' => '', 'class' =>'form-control ')) }}
         @if ($errors->has('personal_website_url')) <p class="text-danger"><b>{{ $errors->first('personal_website_url') }}</b></p> @endif
      </div>
    </div>

    <div class="form-group">
      <label for="" class = "col-sm-3 control-label">Photo URL</label>
      <div class="col-sm-8">
      {{ Form::text('photo_url', Input::old('photo_url'), array('placeholder' => '', 'class' =>'form-control ')) }}
         @if ($errors->has('photo_url')) <p class="text-danger"><b>{{ $errors->first('photo_url') }}</b></p> @endif
      </div>
    </div>

    <div class="form-group">
      <label for="" class = "col-sm-3 control-label">Location</label>
      <div class="col-sm-8">
      {{ Form::text('location', Input::old('location'), array('placeholder' => '', 'class' =>'form-control ')) }}
         @if ($errors->has('location')) <p class="text-danger"><b>{{ $errors->first('location') }}</b></p> @endif
      </div>
    </div>

    <div class="form-group">
      <label for="" class = "col-sm-3 control-label">Academic Career</label>
      <div class="col-sm-8"rows="5">
      {{ Form::textarea('academic_career', Input::old('academic_career'), array('placeholder' => '', 'class' =>'form-control ','size' => '30x4')) }}
         @if ($errors->has('academic_career')) <p class="text-danger"><b>{{ $errors->first('academic_career') }}</b></p> @endif
      </div>
    </div>

    <div class="form-group">
      <label for="" class = "col-sm-3 control-label">Occupation</label>
      <div class="col-sm-8">
      {{ Form::text('occupation', Input::old('occupation'), array('placeholder' => '', 'class' =>'form-control ')) }}
         @if ($errors->has('occupation')) <p class="text-danger"><b>{{ $errors->first('occupation') }}</b></p> @endif
      </div>
    </div>

    <div class="form-group">
      <label for="" class = "col-sm-3 control-label">Research Development Projects</label>
      <div class="col-sm-8">
      {{ Form::textarea('research_development_projects', Input::old('research_development_projects'), array('placeholder' => '', 'class' =>'form-control ','size' => '30x4')) }}
         @if ($errors->has('research_development_projects')) <p class="text-danger"><b>{{ $errors->first('research_development_projects') }}</b></p> @endif
      </div>
    </div>

    <div class="form-group">
      <label for="" class = "col-sm-3 control-label">Cooperation Practice</label>
      <div class="col-sm-8">
      {{ Form::textarea('Cooperation_practice', Input::old('Cooperation_practice'), array('placeholder' => '', 'class' =>'form-control ','size' => '30x4')) }}
         @if ($errors->has('Cooperation_practice')) <p class="text-danger"><b>{{ $errors->first('Cooperation_practice') }}</b></p> @endif
      </div>
    </div>

    <div class="form-group">
      <label for="" class = "col-sm-3 control-label">Patents Intellectual Property</label>
      <div class="col-sm-8">
      {{ Form::textarea('Patents_intellectual_property', Input::old('Patents_intellectual_property'), array('placeholder' => '', 'class' =>'form-control ','size' => '30x4')) }}
         @if ($errors->has('Patents_intellectual_property')) <p class="text-danger"><b>{{ $errors->first('Patents_intellectual_property') }}</b></p> @endif
      </div>
    </div>

    <div class="form-group">
      <label for="" class = "col-sm-3 control-label">Publications</label>
      <div class="col-sm-8">
      {{ Form::textarea('num_of_publications', Input::old('num_of_publications'), array('placeholder' => '', 'class' =>'form-control ','size' => '30x4')) }}
         @if ($errors->has('num_of_publications')) <p class="text-danger"><b>{{ $errors->first('num_of_publications') }}</b></p> @endif
      </div>
    </div>

    <div class="form-group">
      <label for="" class = "col-sm-3 control-label">Selected Publications</label>
      <div class="col-sm-8">
      {{ Form::textarea('selected_publications', Input::old('selected_publications'), array('placeholder' => '', 'class' =>'form-control ','size' => '30x4')) }}
         @if ($errors->has('selected_publications')) <p class="text-danger"><b>{{ $errors->first('selected_publications') }}</b></p> @endif
      </div>
    </div>

    <div class="form-group">
      <label for="" class = "col-sm-3 control-label">Activity</label>
      <div class="col-sm-8">
      {{ Form::textarea('activity', Input::old('activity'), array('placeholder' => '', 'class' =>'form-control ','size' => '30x4')) }}
         @if ($errors->has('activity')) <p class="text-danger"><b>{{ $errors->first('activity') }}</b></p> @endif
      </div>
    </div>

    <div class="form-group">
      <label for="" class = "col-sm-3 control-label">Post Address</label>
      <div class="col-sm-8">
      {{ Form::textarea('post_address', Input::old('post_address'), array('placeholder' => '', 'class' =>'form-control ','size' => '30x4')) }}
         @if ($errors->has('post_address')) <p class="text-danger"><b>{{ $errors->first('post_address') }}</b></p> @endif
      </div>
    </div>

<!--       <div class="form-group ">
        <label for="" class = "col-sm-3 control-label">Password</label>
        <div class="col-sm-8">
          {{ Form::password('newPassword', array('class' => 'form-control')) }}
            @if ($errors->has('password')) <p class="text-danger"><b>{{ $errors->first('password') }}</b></p> @endif
        </div>
      </div>

      <div class="form-group ">
        <label for="" class = "col-sm-3 control-label">ReType Password</label>
        <div class="col-sm-8">
          {{ Form::password('conformPassword', array('class' => 'form-control')) }}
            @if ($errors->has('conformPassword')) <p class="text-danger"><b>{{ $errors->first('conformPassword') }}</b></p> @endif
        </div>
      </div>  -->

      <button type="submit" class="btn btn-primary pull-right" style = "margin-left: 11px;margin-right: 100px;margin-bottom: 11px;">
        <span class="glyphicon glyphicon-ok"></span>
        Create New User
      </button>

      <button type="reset" action = "" class="btn btn-warning pull-right">
        <span class="glyphicon glyphicon-refresh"></span>
        Reset
      </button>
  {{ Form::close() }}
  </div>
</div>
@stop