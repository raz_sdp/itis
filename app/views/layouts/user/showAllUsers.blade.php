@extends('layouts.main')

@section('content')
@include('layouts.navBar')

{{--*/ $msg = HelperActions::getMessageToSession(); /*--}}
@if($msg)
	<div class="col-sm-12 col-md-12 col-lg-12 alert alert-success" style="background-color: #AEDB43;padding: 0;margin-top: -20;">
		<p>
			<p> <b><center>
        <span class="glyphicon glyphicon-ok"></span>
				success
			</center></b> </p>
			<p><center>{{ $msg }}</center></p>
		</p>
	</div>
@endif  


	
	<div class="raw">
			
			
		<div class="col-sm-12 col-md-1 col-lg-1">
			{{ Form::open(array('url' => 'createuser', 'method' => 'get')) }}
	      <button type="submit" class="btn btn-default btn-sm">
	        <span class="glyphicon glyphicon-plus"></span> Add User
	      </button>
	    {{ Form::close() }}
		</div>

		<div class="col-sm-12 col-md-3 col-lg-3">
			{{ Form::open(array('url' => 'showallusers', 'class' => 'form-inline', 'method' => 'post')) }}
		    <div class="input-group">
		      <span class="input-group-btn">
		        <button class="btn btn-default disabled btn-sm"> <b>Users of ITIS</b> </button>
		      </span>		    	
		      {{ Form::text('searchInput', null, array('placeholder' => '', 'class' =>'form-control input-sm')) }}
		      <span class="input-group-btn">
		        <button class="btn btn-default btn-sm" type="submit">
		        	<span class="glyphicon glyphicon-search"> </span>&nbsp;
		        </button>
		      </span>	
		    </div>
	    {{ Form::close() }}
		</div>


		<div class="col-sm-12 col-md-3 col-lg-3">
			{{ Form::open(array('url' => 'sortbyrequser', 'class' => 'form-inline', 'method' => 'post')) }}
				<div class="form-group">
	          {{ Form::select('siteName', Site::makeSiteArray(),
                          null, array('class' =>'form-control input-sm')) }}
			  </div>
			  <button type="submit" class="btn btn-default btn-sm">
			  	<span class = "glyphicon glyphicon-refresh"></span>&nbsp;
			  </button>		  
	    {{ Form::close() }}
		</div>

		<div class="col-sm-12 col-md-2 col-lg-2">
			{{ Form::open(array('url' => 'sortbyrequser', 'class' => 'form-inline', 'method' => 'post')) }}
				<div class="form-group">
					{{ Form::select('roleType', Roole::getRooles(),
                          null, array('class' =>'form-control input-sm')) }}	                         
			  </div>
			  <button type="submit" class="btn btn-default btn-sm">
			  	<span class = "glyphicon glyphicon-refresh"></span>&nbsp;
			  </button>		  
	    {{ Form::close() }}
		</div>

		<div class="col-sm-12 col-md-2 col-lg-2">
			{{ Form::open(array('url' => 'exportuser', 'class' => 'form-inline', 'method' => 'post')) }}
        <div class="form-group">
          {{ Form::select('exportType', array(
                                            ''  => 'Export',
                                            'xml' => 'XML',
                                            'csv' => 'CSV',
                                            'pdf' => 'PDF',
                                        ), 
                           null, array('class' =>'form-control input-sm')) }}
        </div>
        <button type="submit" class="btn btn-default btn-sm">
          <span class = "glyphicon glyphicon-cloud-download"></span>&nbsp;
        </button>     
	    {{ Form::close() }}
	   </div>

	   <div class="col-sm-12 col-md-1 col-lg-1">
			@if(Session::get(Auth::id()) == Roole::getRoleId('Studiendekan'))
				{{ Form::open(array('url' => 'import', 'method' => 'get')) }}
	         <button type="submit" class="btn btn-default btn-sm">
	           <span class="glyphicon glyphicon-cloud-upload"></span>&nbsp;
	             Import
	         </button>
	    	{{ Form::close() }}
      @endif
		</div>

	</div>

	<div class = "container">
	<div class="raw" id="no-more-tables">
		@if (HelperActions::getViewMode())
			@foreach ($users as $user)
				@if (($user->roole_id) == Roole::getRoleId('Studiendekan'))
					{{--*/ $cls = "pink-background"; /*--}}
					{{--*/ $roleToDisplay = "Super Admin"; /*--}}
				@elseif (($user->roole_id) == Roole::getRoleId('Admin'))
					{{--*/ $cls = "purple-background"; /*--}}
					{{--*/ $roleToDisplay = "Admin"; /*--}}
				@elseif (($user->roole_id) == Roole::getRoleId('Coordinator'))
					{{--*/ $cls = "orange-background"; /*--}}
					{{--*/ $roleToDisplay = "Coordinator"; /*--}}
				@elseif (($user->roole_id) == Roole::getRoleId('Professor'))
					{{--*/ $cls = "red-background"; /*--}}
					{{--*/ $roleToDisplay = "Professor"; /*--}}
				@elseif (($user->roole_id) == Roole::getRoleId('Assistant'))
					{{--*/ $cls = "yellow-background"; /*--}}
					{{--*/ $roleToDisplay = "Assistant"; /*--}}
				@elseif (($user->roole_id) == Roole::getRoleId('Student'))
					{{--*/ $cls = "green-background"; /*--}}
					{{--*/ $roleToDisplay = "Student"; /*--}}
				@elseif (($user->roole_id) == Roole::getRoleId('Candidate'))
					{{--*/ $cls = "blue-background"; /*--}}
					{{--*/ $roleToDisplay = "Candidate"; /*--}}							
				@else						
					{{--*/ $cls = "gray-background"; /*--}}
					{{--*/ $roleToDisplay = ""; /*--}}
				@endif

				@if (($user->visible) == 1)
					{{--*/ $strikecls = ""; /*--}}
				@else
					{{--*/ $strikecls = "strike-text"; /*--}}
				@endif

			<table class="col-md-12 col-lg-12 table table-hover cf">
					<tr>
						<td class="col-md-3 col-lg-3 {{$strikecls}} {{$cls}}" data-title="Role"> Role </td>
						<td class="col-md-9 col-lg-9 {{$strikecls}} {{$cls}}"> {{ $roleToDisplay }} </td>
					</tr>
					<tr>
						<td data-title="Title">Title</td>
						<td class="{{$strikecls}}">{{ $user->title }}</td>
					</tr>
					<tr>
						<td data-title="First Name">First Name</td>
						<td class="{{$strikecls}}">{{ $user->first_name }}</td>
					</tr>
					<tr>
						<td data-title="Last Name">Last Name</td>
						<td class="{{$strikecls}}">{{ $user->last_name }}</td>
					</tr>
					<tr>
						<td data-title="Email">Email</td>
						<td class="{{$strikecls}}">{{ $user->email }}</td>
					</tr>
					<tr>
						<td data-title="University">University</td>
						<td class="{{$strikecls}}">{{ Site::getSiteName($user->university) }}</td>
					</tr>
					<tr>
						<td data-title="Intitute">Institute</td>
						<td class="{{$strikecls}}">{{ $user->institute }}</td>
					</tr>
					<tr>
						<td data-title="Institute Url">Institute Url</td>
						<td class="{{$strikecls}}">{{ $user->institute_url }}</td>
					</tr>
					<tr>
						<td data-title="Personal Website Url">Personal Website Url</td>
						<td class="{{$strikecls}}">{{ $user->personal_website_url }}</td>
					</tr>
					<tr>
						<td data-title="Location">Location</td>
						<td class="{{$strikecls}}">{{ $user->location }}</td>
					</tr>
					<tr>
						<td data-title="Academic Career">Academic Career</td>
						<td class="{{$strikecls}}">{{ $user->academic_career }}</td>
					</tr>
					<tr>
						<td data-title="Occupation">Occupation</td>
						<td class="{{$strikecls}}">{{ $user->occupation }}</td>
					</tr>
					<tr>
						<td data-title="Research Development Projects">Research Development Projects</td>
						<td class="{{$strikecls}}">{{ $user->research_development_projects }}</td>
					</tr>
					<tr>
						<td data-title="Cooperation Practice">Cooperation Practice</td>
						<td class="{{$strikecls}}">{{ $user->Cooperation_practice }}</td>
					</tr>
					<tr>
						<td data-title="Patents Intellectual Property">Patents Intellectual Property</td>
						<td class="{{$strikecls}}">{{ $user->Patents_intellectual_property }}</td>
					</tr>
					<tr>
						<td data-title="Number of Publications">Number of Publications</td>
						<td class="{{$strikecls}}">{{ $user->num_of_publications }}</td>
					</tr>
					<tr>
						<td data-title="Selected Publications">Selected Publications</td>
						<td class="{{$strikecls}}">{{ $user->selected_publications }}</td>
					</tr>
					<tr>
						<td data-title="Activity">Activity</td>
						<td class="{{$strikecls}}">{{ $user->activity }}</td>
					</tr>
					<tr>
						<td data-title="Post Address">Post Address</td>
						<td class="{{$strikecls}}">{{ $user->post_address }}</td>
					</tr>
					<tr>
						<td data-title="Telephone">Telephone</td>
						<td class="{{$strikecls}}">{{ $user->telephone }}</td>
					</tr>
			</table>
			






			<table class="col-md-12 col-lg-12 table table-hover cf">
					<tr>
						@if (Session::get(Auth::id()) <= $user->roole_id)
						<td class="col-md-1 col-lg-1">
							{{--*/ $editUrl = 'edituser/'.$user->id.'/'.HelperActions::getPageNumber($_SERVER['REQUEST_URI']) /*--}}
							{{ Form::open(array('url' => $editUrl, 'method' => 'post')) }}
						  <button type="submit" class="btn btn-default btn-xs">
						  	<span class="glyphicon glyphicon-edit" type="submit" ></span>
						  	Edit
						  </button>
						  {{ Form::close() }}
						</td>
						<td class="col-md-1 col-lg-1">
							{{--*/ $logsUrl = 'historylogs/'.$user->id.'/users/'.User::userMitName($user->id) /*--}}
							{{ Form::open(array('url' => $logsUrl, 'method' => 'post')) }}
							  <button type="submit" class="btn btn-default btn-xs">
							  	<span class="glyphicon glyphicon-time"></span>
							  	Logs
							  </button>
							{{ Form::close() }}
						</td>
						@endif
						@if (($user->visible) == 1 && (Session::get(Auth::id()) <= Roole::getRoleId('Admin')) && (Auth::id() != $user->id))
						<td class="col-md-1 col-lg-1">
							{{--*/ $deleteUrl = 'deleteuser/'.$user->id /*--}}
							{{ Form::open(array('url' => $deleteUrl, 'method' => 'post')) }}
                  <button type="submit" class="btn btn-default pull-right btn-xs">
                   <span class="glyphicon glyphicon-remove"></span>
                     Delete
                  </button>
							 {{ Form::close() }}
						</td>
						@elseif (($user->visible) == 0 && Session::get(Auth::id()) <= Roole::getRoleId('Admin'))
						<td class="col-md-1 col-lg-1">
							{{--*/ $unduDeleteUrl = 'undodeleteeduser/'.$user->id /*--}}
							{{ Form::open(array('url' => $unduDeleteUrl, 'method' => 'post')) }}
                 <button type="submit" class="btn btn-default pull-right btn-xs">
                   <span class="glyphicon glyphicon-repeat"></span>
                     Undo Delete 
                 </button>
							 {{ Form::close() }}
						</td>
						@endif
						<td class="col-md-1 col-lg-1"></td>
						<td class="col-md-1 col-lg-1"></td>
						<td class="col-md-1 col-lg-1"></td>
						<td class="col-md-1 col-lg-1"></td>
						<td class="col-md-1 col-lg-1"></td>
						<td class="col-md-1 col-lg-1"></td>
						<td class="col-md-1 col-lg-1"></td>
						<td class="col-md-1 col-lg-1"></td>
						<td class="col-md-1 col-lg-1"></td>
					</tr>
				</table>



			@endforeach
		@else
		<table class="col-md-12 col-lg-12 table table-hover cf">
			<thead class="cf">
				<tr>
					<td><b>Role</b></td>
					<td><b>Title</b></td>
					<td><b>First Name</b></td>
					<td><b>Last Name</b></td>
					<td><b>Email</b></td>
					<td><b>Telephone</b></td>
					<td><b>University</b></td>
					<td><b>Institute URL</b></td>
				</tr>
			</thead>
			<tbody>
				@foreach ($users as $user)
						@if (($user->roole_id) == Roole::getRoleId('Studiendekan'))
							{{--*/ $cls = "pink-background"; /*--}}
							{{--*/ $roleToDisplay = "Super Admin"; /*--}}
						@elseif (($user->roole_id) == Roole::getRoleId('Admin'))
							{{--*/ $cls = "purple-background"; /*--}}
							{{--*/ $roleToDisplay = "Admin"; /*--}}
						@elseif (($user->roole_id) == Roole::getRoleId('Coordinator'))
							{{--*/ $cls = "orange-background"; /*--}}
							{{--*/ $roleToDisplay = "Coordinator"; /*--}}
						@elseif (($user->roole_id) == Roole::getRoleId('Professor'))
							{{--*/ $cls = "red-background"; /*--}}
							{{--*/ $roleToDisplay = "Professor"; /*--}}
						@elseif (($user->roole_id) == Roole::getRoleId('Assistant'))
							{{--*/ $cls = "yellow-background"; /*--}}
							{{--*/ $roleToDisplay = "Assistant"; /*--}}
						@elseif (($user->roole_id) == Roole::getRoleId('Student'))
							{{--*/ $cls = "green-background"; /*--}}
							{{--*/ $roleToDisplay = "Student"; /*--}}
						@elseif (($user->roole_id) == Roole::getRoleId('Candidate'))
							{{--*/ $cls = "blue-background"; /*--}}
							{{--*/ $roleToDisplay = "Candidate"; /*--}}							
						@else						
							{{--*/ $cls = "gray-background"; /*--}}
							{{--*/ $roleToDisplay = ""; /*--}}
						@endif

						@if (($user->visible) == 1)
							{{--*/ $strikecls = ""; /*--}}
						@else
							{{--*/ $strikecls = "strike-text"; /*--}}
						@endif
						
						<tr class= '{{$strikecls}} {{$cls}}'>
							<td data-title="Role">
								{{ $roleToDisplay }}
							</td>
							<td data-title="Title">
								{{ $user->title }}
							</td>
							<td data-title="First Name">
								{{ $user->first_name }}
							</td>
							<td data-title="Last Name">
								{{ $user->last_name }}
							</td>
							<td data-title="Email">
								{{ $user->email }}
							</td>
						    <td data-title="Telephone Number">
								{{ $user->telephone }}
							</td>
							<td data-title="University">
								{{ Site::getSiteName($user->university)}}
							</td>
							<td data-title="Institute URL">
					      {{HTML::link(($user->institute_url), 'Official Website')}}
							</td>
                           
							@if (Session::get(Auth::id()) <= $user->roole_id)
								<td>
									{{--*/ $editUrl = 'edituser/'.$user->id.'/'.HelperActions::getPageNumber($_SERVER['REQUEST_URI']) /*--}}
									{{ Form::open(array('url' => $editUrl, 'method' => 'post')) }}
									  <button type="submit" class="btn btn-default btn-xs">
									  	<span class="glyphicon glyphicon-edit" type="submit" ></span>
									  	Edit
									  </button>
									  {{ Form::close() }}
								</td>


								<td>
									{{--*/ $logsUrl = 'historylogs/'.$user->id.'/users/'.User::userMitName($user->id) /*--}}
									{{ Form::open(array('url' => $logsUrl, 'method' => 'post')) }}
									  <button type="submit" class="btn btn-default btn-xs">
									  	<span class="glyphicon glyphicon-time"></span>
									  	Logs
									  </button>
									{{ Form::close() }}
								</td>
							@endif

              @if (($user->visible) == 1 && (Session::get(Auth::id()) <= Roole::getRoleId('Admin')) && (Auth::id() != $user->id))
              <td>
							{{--*/ $deleteUrl = 'deleteuser/'.$user->id /*--}}
							{{ Form::open(array('url' => $deleteUrl, 'method' => 'post')) }}
                  <button type="submit" class="btn btn-default pull-right btn-xs">
                   <span class="glyphicon glyphicon-remove"></span>
                     Delete
                  </button>
							 {{ Form::close() }}
              </td>
              @elseif (($user->visible) == 0 && Session::get(Auth::id()) <= Roole::getRoleId('Admin'))
              <td>
							{{--*/ $unduDeleteUrl = 'undodeleteeduser/'.$user->id /*--}}
							{{ Form::open(array('url' => $unduDeleteUrl, 'method' => 'post')) }}
                 <button type="submit" class="btn btn-default pull-right btn-xs">
                   <span class="glyphicon glyphicon-repeat"></span>
                     Undo Delete 
                 </button>
							 {{ Form::close() }}
              </td>
              @endif
					</tr>
       	@endforeach
								
			</tbody>
		</table>
		@endif
	</div>
	<br/>
	<div class = "raw">
		<?php echo $users->links(); ?>
	</div>
  </div>



@stop



		