@extends('layouts.main')

@section('content')
@include('layouts.navBar')

  <div class="raw">

		<div class="col-sm-12 col-md-3 col-lg-3">
			{{ Form::open(array('url' => 'showallcontacts', 'method' => 'post', 'class' => 'form-inline')) }}
		    <div class="input-group">
		      <span class="input-group-btn">
		        <button class="btn btn-default disabled btn-sm"> <b>Contacts of ITIS</b> </button>
		      </span>		    	
		      {{ Form::text('searchInput', null, array('placeholder' => '', 'class' =>'form-control input-sm')) }}
		      <span class="input-group-btn">
		        <button class="btn btn-default btn-sm" type="submit">
		        	<span class="glyphicon glyphicon-search"> </span>&nbsp;
		        </button>
		      </span>	
		    </div>
			{{ Form::close() }}
		</div>


		<div class="col-sm-12 col-md-3 col-lg-3">
			{{ Form::open(array('url' => 'sortcontactsbyrequser', 'method' => 'post', 'class' => 'form-inline')) }}
				<div class="form-group">
	          {{ Form::select('siteName', Site::makeSiteArray(),
                          null, array('class' =>'form-control input-sm')) }}
			  </div>
			  <button type="submit" class="btn btn-default btn-sm">
			  	<span class = "glyphicon glyphicon-refresh"></span>&nbsp;
			  </button>		  
			{{ Form::close() }}
		</div>

		<div class="col-sm-12 col-md-2 col-lg-2">
			{{ Form::open(array('url' => 'sortcontactsbyrequser', 'method' => 'post', 'class' => 'form-inline')) }}
				<div class="form-group">
	        {{ Form::select('roleType', array(
																						''  => 'Role Type',
                                            '1' => 'Super Admin',
                                            '2' => 'Admin',
                                            '3' => 'Professor', 
                                            '4' => 'Assistant',
                                            '5' => 'Student'
	                                      ), 
	                         null, array('class' =>'form-control input-sm')) }}
			  </div>
			  <button type="submit" class="btn btn-default btn-sm">
			  	<span class = "glyphicon glyphicon-refresh"></span>&nbsp;
			  </button>		  
			{{ Form::close() }}
		</div>

	</div>

<div class = "container">
	<div class="raw" id="no-more-tables">
			
		<table class="col-md-12 col-lg-12 table table-hover cf">
			<thead class="cf">
				<tr>
					
					<td><b>Title</b></td>
					<td><b>First Name</b></td>
					<td><b>Last Name</b></td>
					<td><b>Email</b></td>
					<td><b>Telephone</b></td>
					<td><b>University</b></td>
					<td><b>Institute URL</b></td>
				</tr>
			</thead>
			<tbody>
				@foreach ($users as $user)
				@if(Session::get(Auth::id()) <= Roole::getRoleId('Candidate'))
				@if (($user->visible) == 1)
							{{--*/ $strikecls = ""; /*--}}
						@else
							{{--*/ $strikecls = "strike-text"; /*--}}
						@endif
				
						
						<tr class= '{{$strikecls}}'>

							<td data-title="Title">
								{{ $user->title }}
							</td>
							<td data-title="First Name">
								{{ $user->first_name }}
							</td>
							<td data-title="Last Name">
								{{ $user->last_name }}
							</td>
							<td data-title="Email">
								{{ $user->email }}
							</td>
							<td data-title="Telephone">
								{{ $user->telephone }}
							</td>
							<td data-title="University">
								{{ Site::getSiteName($user->university)}}
							</td>
							<td data-title="Institute URL">
					      {{HTML::link(($user->institute_url), 'Official Website')}}
							</td>
                           
					</tr>
                
                 @endif
           	@endforeach
								
			</tbody>
		</table>
	</div>

	<br/>
	<div class = "raw">
		<?php echo $users->links(); ?>
	</div>
</div>


@stop