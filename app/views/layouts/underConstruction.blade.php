@extends('layouts.main')

@section('content')
<div class = "col-sm-1"></div>
<div class = "col-sm-10 alert alert-success" role="alert" style = "margin-top: 17%;">
	<center>
		<h1>
			Page Under Construction! Please Check Back Soon
		</h1>
		<h2>
			<form action="/itis_db/showallcourses">
				<button href="" type = "submit" class="btn btn-link">
	    		<span class = "glyphicon glyphicon-home"></span> Take Me To Home
				</button>
			</form>
		</h2>
	</center>
</div>
<div class = "col-sm-1"></div>
@stop
