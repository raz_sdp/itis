<?php
 
class RooleTableSeeder extends Seeder {
 
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $vader = DB::table('rooles')->insert([
         'role'=> 'Studiendekan'
         // 1
        ]);

        DB::table('rooles')->insert([
         'role'=> 'Admin'
         // 2
        ]);

        DB::table('rooles')->insert([
         'role'=> 'Coordinator'
         // 3
        ]);

        DB::table('rooles')->insert([
         'role'=> 'Professor'
         // 4
        ]);

        DB::table('rooles')->insert([
         'role'=> 'Assistant'
         // 5
        ]);

        DB::table('rooles')->insert([
         'role'=> 'Student'
         // 6
        ]);

        DB::table('rooles')->insert([
         'role'=> 'Candidate'
         // 7
        ]);

    }
}