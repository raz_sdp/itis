<?php
 
class CategoryTableSeeder extends Seeder {
 
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $vader = DB::table('categories')->insert([
         'category_name'=> 'Theoretical Foundations'
        ]);

        DB::table('categories')->insert([
         'category_name'=> 'Data and Information'
        ]);

        DB::table('categories')->insert([
         'category_name'=> 'Networking and Communication'
        ]);

        DB::table('categories')->insert([
         'category_name'=> 'Business and Law'
        ]);

        DB::table('categories')->insert([
         'category_name'=> 'Soft Skills'
        ]);
                       
    }
}