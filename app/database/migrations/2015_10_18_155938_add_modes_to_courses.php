<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddModesToCourses extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::table('courses', function(Blueprint $table) {
			$table->boolean('video_recording')->nullable();
			$table->boolean('lecture_hall_transmission')->nullable();
			$table->boolean('live_stream')->nullable();
			$table->boolean('interactive_live_stream')->nullable();
			$table->boolean('attendance_required')->nullable();
			$table->boolean('partial_attendance_required')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
