<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResearchProjectsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('research_projects', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id')->nullable();
		    $table->string('research_projects_name',1000)->nullable();
		    $table->float('credit')->nullable();
		    $table->integer('semester')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('research_projects');
	}

}
