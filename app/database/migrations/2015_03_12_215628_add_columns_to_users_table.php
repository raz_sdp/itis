<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('users', function(Blueprint $table)
		{
		 
		 $table->string('institute_url',2083)->nullable();
         $table->string('personal_website_url',2083)->nullable();
         $table->string('photo_url',2083)->nullable();
         $table->text('location')->nullable();
         $table->text('academic_career')->nullable();
         $table->text('occupation')->nullable();
         $table->text('research_development_projects')->nullable();
         $table->text('Cooperation_practice')->nullable();
         $table->text('Patents_intellectual_property')->nullable();
         $table->text('num_of_publications')->nullable();
         $table->text('selected_publications')->nullable();
         $table->text('activity')->nullable();
         $table->text('post_address')->nullable();
         $table->string('telephone',255)->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('users', function(Blueprint $table)
		{
			//
		});
	}

}
