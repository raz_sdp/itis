<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToCoursesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('courses', function(Blueprint $table)
		{
			 $table->string('lecturer',255);
			 $table->string('second_lecturer',255)->nullable();
			 $table->string('lecturer_url',2083)->nullable();
			 $table->string('second_lecturer_url',2083)->nullable();
			 $table->string('time',255)->nullable();
			 $table->string('time_2',255)->nullable();
			 $table->string('room',255)->nullable();
			 $table->string('room_2',255)->nullable();
			 $table->string('partial_compact_course',255)->nullable();
			 $table->string('has_video_recording',100)->nullable();
			 $table->string('video_recording_download_url',2083)->nullable();
			 $table->string('live_stream_offered',100)->nullable();
			 $table->string('live_web_feed_url',2083)->nullable();
			 $table->string('interactive_live_stream',100)->nullable();
			 $table->string('transmission_is_possible',100)->nullable();
			 $table->string('transmission_room_clausthal',255)->nullable();
			 $table->string('transmission_room_clausthal_2',255)->nullable();
			 $table->string('transmission_room_hannover',255)->nullable();
			 $table->string('transmission_room_hannover_2',255)->nullable();
			 $table->string('transmission_room_göttingen',255)->nullable();
			 $table->string('transmission_room_göttingen_2',255)->nullable();
			 $table->string('transmission_room_braunschweig',255)->nullable();
			 $table->string('transmission_room_braunschweig_2',255)->nullable();
			 $table->string('assistant_braunschweig',255)->nullable();
			 $table->string('assistant_göttingen',255)->nullable();
			 $table->string('assistant_hannover',255)->nullable();
			 $table->string('assistant_clausthal',255)->nullable();
			 $table->text('comment')->nullable();
			 
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('courses', function(Blueprint $table)
		{
			//
		});
	}

}
