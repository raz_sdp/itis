<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropModesColumnFromCourses extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::table('courses', function(Blueprint $table) {
			  $table->dropColumn(['partial_compact_course', 'has_video_recording', 'live_stream_offered', 'interactive_live_stream', 'transmission_is_possible']);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
