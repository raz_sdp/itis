<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()	{
		Schema::create('logs', function(Blueprint $table) {
         $table->increments('id');
         $table->string('table_name');
         $table->integer('raw_id_in_table');
         $table->string('change_string');
         $table->string('raw_data_string');
         $table->string('done_by');
         $table->integer('done_by_id');
         $table->boolean('created');
		 		 $table->boolean('deleted');
         $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()	{
		Schema::drop('logs');
	}

}