<?php

class HelperController extends BaseController {

  //thids using to switch the view mode
  // its cal another function to do this 
  // finally redirect the all request to showallcourses
  // done by thishanth thevarajah
  public function changeViewMode() {
    HelperActions::swtichViewMode();
    return Redirect::to('/showallcourses');
  }

 //Controller function used to call model fuctions which are used to export XML and CSv files
 //done by Fatema Tuj Johora
 public function getFile(){
    if(Request::url() === URL::route('moduleCsv')|| Request::url() === URL::route('courseCsv')||Request::url() === URL::route('userCsv')||Request::url() === URL::route('logCsv'))
      {
        $output= HelperActions::csvFileMaker();
      }
    elseif(Request::url() === URL::route('moduleXml')||Request::url() === URL::route('courseXml')||Request::url() === URL::route('userXml')||Request::url() === URL::route('logXml'))
      {
        $output= HelperActions::xmlFileMaker();
      }

        $filename= HelperActions::identifyFileName();
        header('Content-type: application/csv');
        header('Content-Disposition: attachment; filename='.$filename);

        return($output);
    }
  
  //Controller function used to call model fuctions which are used to export PDF files
  //done by Fatema Tuj Johora
  public function getPdfFile(){

  $html = HelperActions::test();

  if(Request::url() === URL::route('coursePdf'))
  {
   return PDF::load($html, 'A4','portrait')->download('course_pdf');
  }
  elseif(Request::url() === URL::route('logPdf'))
  {
    return PDF::load($html, 'A4','portrait')->download('logs_pdf');
  }

  elseif(Request::url() === URL::route('userPdf'))
  {
    return PDF::load($html, 'A4','portrait')->download('user_pdf');
  }
  elseif(Request::url() === URL::route('modulePdf'))
  {
    return PDF::load($html, 'A4','portrait')->download('module_pdf');
  }
  
}
    
}