<?php

class UserController extends BaseController {

	// its show all user with pagination or without pagination when user search
	public function index()	{
		if (Input::get('searchInput')) {
			$page = 150;
		} else {
			$page = 6;
		}

		$users = User::readUserMitPagination($page);
		return View::make('layouts.user.showAllUsers', array('users' => $users));
	}

	// its show all contacts with pagination or without pagination when user search
	public function studentContacts()	{
		if (Input::get('searchInput')) {
			$page = 150;
		} else {
			$page = 6;
		}

		$users = User::readUserMitPagination($page);
		return View::make('layouts.user.showAllContacts', array('users' => $users));
	}

	//function for send courses with success message
	public function indexWithPage()	{
		// this is for read the courses from database
		$users = User::readUserMitPagination(6);
    // this is for make View
    return View::make('layouts.user.showAllUsers', ['users' => $users]);
	}	
    
    // its show users all details with pagination or without pagination when user search
	public function details()	{
		if (Input::get('searchInput')) {
			$page = 150;
		} else {
			$page = 6;
		}

		$users = User::readUserMitPagination($page);
		return View::make('layouts.user.showDetails', array('users' => $users));
	}

	// this function is for sord user by university & sort by user role type
	public function sortReq()	{
		if (Input::get('roleType')) {
			$users = DB::table('users_rooles')
               	  ->join('users', 'users.id', '=', 'users_rooles.user_id' )
                	->where('users_rooles.roole_id', '=', Input::get('roleType'))
									->orderBy('users.updated_at', 'DESC')
									->paginate(50);

		}	elseif (Input::get('siteName')) {
			$users = DB::table('users_rooles')
               	  ->join('users', 'users.id', '=', 'users_rooles.user_id' )
                	->where('university', '=', Input::get('siteName'))
									->orderBy('users.updated_at', 'DESC')
									->paginate(50);

		}else {
			$users = DB::table('users_rooles')
         	  ->join('users', 'users.id', '=', 'users_rooles.user_id' )
						->orderBy('users.updated_at', 'DESC')
						->paginate(50);
		}
		//return View::make('layouts.user.showAllUsers', array('users' => $users));
		return $users;
	}
    
    //use for calling sortReq function to show all sorted users
	public function sortByReq()	{
		$users= UserController::sortReq();
		return View::make('layouts.user.showAllUsers', array('users' => $users));

	}
    
    //use for calling sortReq function to show all sorted contact person for students
	public function sortContactByReq()	{
		$users= UserController::sortReq();
		return View::make('layouts.user.showAllContacts', array('users' => $users));

	}

	// this is for show login form when needs
	public function showLogin(){
			echo View::make('layouts.login');
	}

	// this is make view for edit a user
	public function edit($uId, $pageNumber = '1')	{
		$user= new User;
    $edit=$user->where('id',array($uId))->first();
    $editUser=array($edit);

    if ((Session::get(Auth::id()) > Roole::getRoleId('Admin')) && (Auth::id() != $uId)) {
    	return Login::logOut();
    }

    HelperActions::putPageNumberToSession($pageNumber);

		$editUer = (DB::table('users_rooles')
		             	  ->join('users', 'users.id', '=', 'users_rooles.user_id' )
		              	->where('users.id', '=', $uId)->first());
		$editUer = (array) $editUer;
    // print_r($editUer);

		
		return View::make('layouts.user.editUser', array('user' => $editUer));
	}

	// this is make view for create a user
	public function create(){
		echo View::make('layouts.user.createNewUser');
	}

	// its for store detail for newly created user
	public function store()	{

		$rules					 = User::validationRuleMakerForCreate();
		$validator 			 = Validator::make(Input::all(), $rules);
		$randomPassword  = User::randomPassword();

		if ($validator->fails()) {
			// get the error messages from the validator
			$messages = $validator->messages();
			// redirect our user back to the form with the errors from the validator
			return Redirect::to('/createuser')
				->withErrors($validator)->withInput();
	  } else {

			$user = new User;
 
        $user->title                               =  Input::get('title');
        $user->first_name                          =  Input::get('first_name');
        $user->last_name                           =  Input::get('last_name');
        $user->email                               =  Input::get('email');
        $user->university                          =  Input::get('university');
        $user->visible                             =  1;
        $user->institute                           =  Input::get('institute');
        $user->institute_url                       =  Input::get('institute_url');
        $user->personal_website_url                =  Input::get('personal_website_url');
        $user->photo_url                           =  Input::get('photo_url');
        $user->location                            =  Input::get('location');
        $user->academic_career                     =  Input::get('academic_career');
        $user->occupation                          =  Input::get('occupation');
        $user->research_development_projects       =  Input::get('research_development_projects');
        $user->Cooperation_practice                =  Input::get('Cooperation_practice');
        $user->Patents_intellectual_property       =  Input::get('Patents_intellectual_property');
        $user->num_of_publications                 =  Input::get('num_of_publications');
        $user->selected_publications               =  Input::get('selected_publications');
        $user->activity                            =  Input::get('activity');
        $user->post_address                        =  Input::get('post_address');
        $user->password                            =  Hash::make($randomPassword);
        $user->telephone                           =  Input::get('telephone');

        
        
        if(User::passwordMacher()) {
	         $user->save();
	         $idToSend = DB::getPdo()->lastInsertId();

	         $user = User::find($idToSend);
					 $user->rooles()->attach(Input::get('role_id')); //this executes the insert-query
	         Session::put($idToSend, Input::get('role_id'));

	         Logs::saveLog("users", Input::get('title')." ".Input::get('first_name')." ".Input::get('last_name'). " User Created", $idToSend, "",0, 1, 0);

					$name 								= User::userMitName($idToSend);
					$to 									= Input::get('email');
					$array 								= array('password' => $randomPassword, 'name' => $name, 'to' => $to);
          SendMail::sendEmail('layouts.mails.password', $array, $to, 'Your new ITIS password');

           HelperActions::putMessageToSession("User created successfully :)");
           return Redirect::to('/showalluserswith?page=1');
        } else {
           return Redirect::to('/createuser')
										->withErrors($validator)->withInput();
        }
	  }
	}

	// this is for store the edited information
	public function update()	{
		$editId 		 = Input::get('id');
		Logs::prepUserLog($editId);
		$pageNumber = HelperActions::getPageNumberFromSession();

		if (Session::get(Auth::id()) <= Roole::getRoleId('Admin')) {
		  $user = User::find($editId);
			$user->rooles()->detach(); 
			$user->rooles()->attach(Input::get('role_id')); //this executes the insert-query
	    Session::set($editId, Input::get('role_id'));
		}

		$rules = User::casualValidationRuleMaker();
		$updateArray = User::updateArrayMaker();

		$validator = Validator::make(Input::all(), $rules);
		if ($validator->fails() || !User::isUniqueEmail(Input::get('email'), $editId )) {
			// get the error messages from the validator
			$messages = $validator->messages();

			// redirect our user back to the form with the errors from the validator
			return Redirect::to('/edituser/'.$editId.'/'.$pageNumber)
				->withErrors($validator)->withInput(); 
	  }

		$user = new User;
	  $user->where('id', $editId)
	        ->update($updateArray);
	  HelperActions::putMessageToSession("Your Changes updated successfully :)");
    return $pageNumber > 0 ? Redirect::to('/showalluserswith?page='.$pageNumber) : Redirect::to('/showallcourseswith');
		//echo View::make('layouts.messages.successMessage');
	}

	// this is make view for edit password
	public function editpassword() {
		echo View::make('layouts.user.editPassword');
	}

	// this is for store the update password
	public function updatepassword() {
		$user= new User; 

		$rules = User::passwordValidationRuleMaker(Input::all());
		$validator = Validator::make(Input::all(), $rules);

		if ($validator->fails()) {

			// get the error messages from the validator
			$messages = $validator->messages();

			return Redirect::to('/editpassword')
				->withErrors($validator)->withInput();
	  } else {

	  	if (User::passwordMacher() && User::oldPasswordChecker()) {
	  		$passwordUpdateArray = User::passwordUpdateArrayMaker();
	  		$user ->where('id', Auth::id())
              ->update($passwordUpdateArray);
        Logs::saveLog("users", User::userMitName(Auth::id()). "'s Password Updated", Auth::id(), "", "Password", 0, 0);
      	//echo View::make('layouts.messages.successMessage');
	  		HelperActions::putMessageToSession("Yor password updated successfully :)");
      	return Redirect::to('/showallcourses');
	  	} else {
	  		HelperActions::putMessageToSession("1. Please enter your correct old passwor <br/> 2. Please use same password for New Password and ReType Password <br/> <br/> If you forgot your old password, please talk with ITISDB admin");
				return Redirect::to('/editpassword');
	  	}
	  }
	}

	// this is for delete a user
	public function deleteUser($deleteUser) {
  	User::deleteU($deleteUser);
  	HelperActions::putMessageToSession("User deleted successfully :)");
  	return Redirect::to('/showalluserswith?page='.HelperActions::getPageNumberFromSession());
	}

	// this is for retrive a user from delete
	public function undoDeleteUser($undoUser) {
  	User::undoDelete($undoUser);
  	HelperActions::putMessageToSession("User retrieved successfully :)");
  	return Redirect::to('/showalluserswith?page='.HelperActions::getPageNumberFromSession());
	}	

	// this is for change the view of user role
	// example admin to student
	public function viewAs() {
		$id = Input::get('viewAs') ? Input::get('viewAs') : 5 ;
    Session::set(Auth::id(), $id);
		return Redirect::to('/showallcourses');              
	}

	public function changeLanguage(){
		$key = "languageOf".Auth::id();
		User::changeUserLanguage($key);
		$lang = User::getUserLanguage($key);
		$url = '/showallcourses';
		return Redirect::to($url);              
	}

	// it will generate random password and send it to proper user
	// done by Thishanth Thevarajah
	public function generatePasswordAndSend($userId) {
		$generatedPassword  	= User::randomPassword();
		$name 								= User::userMitName($userId);
		$to 									= User::getEmail($userId);
		$array 								= array('password' => $generatedPassword, 'name' => $name, 'to' => $to);

		$generatedPassword 		= Hash::make($generatedPassword);
		$passwordUpdateArray 	= array('password' => $generatedPassword);
		User::updatePassword($userId, $passwordUpdateArray);

		SendMail::sendEmail('layouts.mails.password', $array, $to, 'Your new ITIS password');
		HelperActions::putMessageToSession("Password generated and sent to user successfully :)");
		return Redirect::to('/showalluserswith?page=1');
	}
	//reminder mail for all students who didn't submit studyplan
	public function sendStudyplanReminder() {
    	$mailuserlistquery= DB::select(DB::raw("select email,id from users where id in( Select user_id from users_rooles where roole_id in (select id from rooles where role='Student')) and users.id not in (select user_id from study_plans)") );

		$mailuserlist = json_decode(json_encode($mailuserlistquery), true);
		#BaseController::_setTrace($mailuserlist);
		if(empty($mailuserlist))
			return Redirect::to('studyplan')->with('mail_success', false);
		
		foreach ($mailuserlist as $mailuser) {
			$data = [
				'title'=>"Hello Everyone", 
				'destinaire'=>" Submit your study plan as soon as possible. "
			];
			Mail::queue('layouts.mails.mail', $data, function($message) use ($mailuser) {
				$message
				//->from('itis.mail.test@gmail.com', 'ITIS')
				->to($mailuser['email'])
				->subject('Study Plan Notification');
			});
		}

		return Redirect::to('studyplan')->with('mail_success', true);
	}

  //To understand which export format user need
  public function export()
  {   
  if (Input::get('exportType')==='xml')
    {return Redirect::to('/downloaduserxml');}
  elseif(Input::get('exportType')==='csv')
    {return Redirect::to('/downloadusercsv');}
  else
    {return Redirect::to('/downloaduserpdf');}
  }

	

// -----------------------------------------------------------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------------------------------------------------------
// --------------------------

	//detele this function after install the app!
	//first time use only!
	public function createSuperAdmin(){
		return View::make('layouts.user.createSuperAdminOneUSeOnly');
	}

	//detele this function after install the app!
	//first time use only!
	public function storeSuperAdmin()	{

		if (DB::table("users")->count() != 0) {
			return UserController::showLogin();
		}

			$user = new User;
      $user->title           =  Input::get('title');
      $user->first_name      =  Input::get('first_name');
      $user->last_name       =  Input::get('last_name');
      $user->email           =  Input::get('email');
      $user->university      =  Input::get('university');
      $user->visible         =  1;
      $user->institute       =  Input::get('institute');
      $user->password        =  Hash::make(Input::get('newPassword'));
      
      
      if(User::passwordMacher()) {
         $user->save();
         $idToSend = DB::getPdo()->lastInsertId();
         $firstRoleId = Law::getFirstRoleId();

         Session::put($idToSend, $firstRoleId);

         $user = User::find($idToSend);
				 $user->rooles()->attach($firstRoleId); //this executes the insert-query

         $successMsg = Settings::superAdminCreatedMsg();
         $msg = array($successMsg, "green-background", "show_login");
         return View::make('layouts.settings.install', ['msg' => $msg]);
      } 
	}

// -----------------------------------------------------------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------------------------------------------------------
}