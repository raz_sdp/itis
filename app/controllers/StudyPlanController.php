<?php
class StudyPlanController extends \BaseController {
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function createStudyPlan()
    {
        $is_first = 1;
        $month = intval(date('m'));
        $year = intval(date('Y'));
        $year_1 = intval(date('y'));
        if($month >= 4 && $month <=9) {
            $semester = 'SoSe '.$year;
            $semester_2 = 'WiSe '. ($year-1).'/'.$year_1;
            $semester_3 = 'SoSe '. ($year);
            $semester_4 = 'WiSe '. ($year-1).'/'. $year_1;
        } else {
            $semester = 'WiSe '. ($year).'/'.($year_1+1);          
            $semester_2 = 'SoSe '. ($year+1);
            $semester_3 = 'WiSe '. ($year+1).'/'.($year_1+2);   
            $semester_4 = 'SoSe '. ($year);
        }
        Session::put('sem_1', $semester);
        Session::put('sem_2', $semester_2);
        Session::put('sem_3', $semester_3);
        Session::put('sem_4', $semester_4);

        $request = Request::input();
        $tab = 1;
        if (!empty($request)) {
            $tab = Request::input('tab');
            $is_first = 0;
            $semester_input = array_pop($request);
            switch($tab){
                case 4:{
                    Session::put('sem_4', $semester_input);
                    break;
                }
                case 3:{
                    Session::put('sem_3', $semester_input);
                    break;
                }
                case 2:{
                    Session::put('sem_2', $semester_input);
                    break;
                }
                case 1:{
                    Session::put('sem_1', $semester_input);
                    break;
                }
            }
            $semester = Session::get('sem_1');
            $semester_2 = Session::get('sem_2');
            $semester_3 = Session::get('sem_3');
            #$semester_4 = Session::get('sem_4');
            #BaseController::_setTrace($semester);
        }
        ########################### ########################################################
        $user_id = Auth::id();
        $is_exist = StudyPlan::getIndividualPlan($user_id);
        if(!empty($is_exist)){
            #BaseController::_setTrace($semester_2);
            $modules_1 = json_decode(json_encode(Module::getModules($semester)), true);
            $modules_2 = json_decode(json_encode(Module::getModules($semester_2)), true);
            $modules_all = json_decode(json_encode(Module::getAllModules()), true);
            #######################################  My Course Info ##################################################
            //$course_sem_1 = array_column(json_decode(json_encode(DB::table('semester_courses')->select('course_id')->where('user_id', '=', $user_id)->where('semester', '=', 1)->get()),true), 'course_id');
            //$course_sem_2 = array_column(json_decode(json_encode(DB::table('semester_courses')->select('course_id')->where('user_id', '=', $user_id)->where('semester', '=', 2)->get()),true), 'course_id');
            //$course_sem_3 = array_column(json_decode(json_encode(DB::table('semester_courses')->select('course_id')->where('user_id', '=', $user_id)->where('semester', '=', 3)->get()),true), 'course_id');
			$course_sem_1 = array_column(json_decode(json_encode(DB::table('semester_courses')->select('course_id')->where('user_id', '=', $user_id)->where('semester', '=', 1)->get()),true), 'course_id');       
        ########    get module id wrt course id    #######
        $module_sem_1 = array();
        $module_sem_2 = array();
        $module_sem_3 = array();
        if(!empty($course_sem_1)){            
            foreach ($course_sem_1 as $key => $course_id) { 
              $module_test_1=DB::table('courses')->select('module_id')->where('id', '=', $course_id)->first();
              if(!empty($module_test_1)){
                $module_sem_1[] = strval($module_test_1->module_id);
                #BaseController::_setTrace(DB::table('courses')->select('module_id')->where('id', '=', $course_id)->first(),false);           
            }
            } 
             #BaseController::_setTrace($module_sem_1);            
        }        
        $course_sem_2 = array_column(json_decode(json_encode(DB::table('semester_courses')->select('course_id')->where('user_id', '=', $user_id)->where('semester', '=', 2)->get()),true), 'course_id');
        
        if(!empty($course_sem_2)){            
            foreach ($course_sem_2 as $key => $course_id) {                
                $module_test_2=DB::table('courses')->select('module_id')->where('id', '=', $course_id)->first();
              if(!empty($module_test_2))
                $module_sem_2[] = strval($module_test_2->module_id);
            }            
        }   
        $course_sem_3 = array_column(json_decode(json_encode(DB::table('semester_courses')->select('course_id')->where('user_id', '=', $user_id)->where('semester', '=', 3)->get()),true), 'course_id');
        if(!empty($course_sem_3)){            
            foreach ($course_sem_3 as $key => $course_id) { 
            $module_test_3=DB::table('courses')->select('module_id')->where('id', '=', $course_id)->first();
              if(!empty($module_test_3))
                $module_sem_3[] = strval($module_test_3->module_id);               
            }            
        } 
            #BaseController::_setTrace($course_sem_2);

            ####################################### My Softskill########################################################
            $my_softskill_sem_1 = array_column(json_decode(json_encode(DB::table('soft_skills')->select('soft_skills_name')->where('user_id', '=', $user_id)->where('semester', '=', 1)->get()),true), 'soft_skills_name');
            $credit_1 = array_column(json_decode(json_encode(DB::table('soft_skills')->select('credit')->where('user_id', '=', $user_id)->where('semester', '=', 1)->get()),true), 'credit');
            $my_softskill_sem_2 = array_column(json_decode(json_encode(DB::table('soft_skills')->select('soft_skills_name')->where('user_id', '=', $user_id)->where('semester', '=', 2)->get()),true), 'soft_skills_name');
            $credit_2 = array_column(json_decode(json_encode(DB::table('soft_skills')->select('credit')->where('user_id', '=', $user_id)->where('semester', '=', 2)->get()),true), 'credit');
            $my_softskill_sem_3 = array_column(json_decode(json_encode(DB::table('soft_skills')->select('soft_skills_name')->where('user_id', '=', $user_id)->where('semester', '=', 3)->get()),true), 'soft_skills_name');
            $credit_3 = array_column(json_decode(json_encode(DB::table('soft_skills')->select('credit')->where('user_id', '=', $user_id)->where('semester', '=', 3)->get()),true), 'credit');
            #BaseController::_setTrace($credit_1);

            ########################################## My Research Project & Theses ##################################################
            $my_research_project = json_decode(json_encode(DB::table('research_projects')->where('user_id', '=', $user_id)->first()),true);
            $my_project_2 = null;
            $my_project_3 = null;
            if($my_research_project['semester']==2){
                $my_project_2 = $my_research_project['research_projects_name'];
            }else{
                $my_project_3 = $my_research_project['research_projects_name'];
            }
            $my_master_theses = json_decode(json_encode(DB::table('master_theses')->select('master_theses_name')->where('user_id', '=', $user_id)->first()),true);
            $theses_name = $my_master_theses['master_theses_name'];
            #BaseController::_setTrace($my_master_theses);
            return View::make('layouts.studyplan.createStudyPlan',
                array(
                    'modules_1' => $modules_1,
                    'modules_2' => $modules_2,
                    'modules_all' => $modules_all,
                    'course_sem_1' => $module_sem_1,     
                    'course_sem_2' => $module_sem_2,
                    'course_sem_3' => $module_sem_3,
                    'credit_1' => $credit_1,
                    'credit_2' => $credit_2,
                    'credit_3' => $credit_3,
                    'my_softskill_sem_1' => $my_softskill_sem_1,            
                    'my_softskill_sem_2' => $my_softskill_sem_2,            
                    'my_softskill_sem_3' => $my_softskill_sem_3,  
                    'my_project_2' => $my_project_2,
                    'my_project_3' => $my_project_3,          
                    'my_research_project' => $my_research_project,
                    'theses_name' => $theses_name,                 
                )
            );
        } else{
            $study_plans = array();
            $data = Course::getCourseData($semester);
            $keyword = Request::input('keyword');
            #BaseController::_setTrace($keyword);
            $study_plans = StudyPlan::getAllPlans($keyword);
            #BaseController::_setTrace($study_plans);
            ####################  make modules as array  #####################################
            $modules_1 = json_decode(json_encode(Module::getModules($semester)), true);
            $modules_2 = json_decode(json_encode(Module::getModules($semester_2)), true);
            #BaseController::_setTrace($modules_1);
            $semester_2_data = Course::getCourseData($semester_2);
            $semester_3_data = Course::getCourseData($semester_3);
            /*$semester_4_data = Course::getCourseData($semester_4);*/
            #BaseController::_setTrace($data);
            return View::make('layouts.studyplan.createStudyPlan',
                array(
                    'data' => $data,
                    'semester_2_data'=> $semester_2_data, 
                    'modules_1' => $modules_1,
                    'modules_2' => $modules_2,
                    'semester_3_data' => $semester_3_data,
                    #'semester_4_data'=> $semester_4_data,
                    'tab' => $tab,
                    'is_first' => $is_first,
                    'semester_1_name' => $semester,
                    'semester_2_name' => $semester_2,
                    'semester_3_name' => $semester_3,                    
                    'study_plans' => $study_plans
                )
            );
        }
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function showNetworkingcourse()
    {               
        return View::make('layouts.studyplan.showNetworkingcourse');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $request = Input::get('course');
        BaseController::_setTrace($request);
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($user_id = null)
    { 
        /*
            $month = intval(date('m'));
            $year = intval(date('Y'));
            $year_1 = intval(date('y'));
            if($month >= 4 && $month <=9){
                $semester = 'SoSe '.$year;
                $semester_2 = 'WiSe '. ($year-1).'/'.$year_1;
                $semester_3 = 'SoSe '. ($year);
                $semester_4 = 'WiSe '. ($year-1).'/'. $year_1;
            }   else{
                $semester = 'WiSe '. ($year-1).'/'.$year_1;
            }
            Session::put('sem_1', $semester);
            Session::put('sem_2', $semester_2);
            Session::put('sem_3', $semester_3);
            Session::put('sem_4', $semester_4);

            $request = Request::input();
            $tab = 1;
            if (!empty($request)) {
                $tab = Request::input('tab');
                $semester_input = array_pop($request);
                switch($tab){
                    case 4:{
                        Session::put('sem_4', $semester_input);
                        break;
                    }
                    case 3:{
                        Session::put('sem_3', $semester_input);
                        break;
                    }
                    case 2:{
                        Session::put('sem_2', $semester_input);
                        break;
                    }
                    case 1:{
                        Session::put('sem_1', $semester_input);
                        break;
                    }
                }
                $semester = Session::get('sem_1');
                $semester_2 = Session::get('sem_2');
                $semester_3 = Session::get('sem_3');
                #$semester_4 = Session::get('sem_4');
            }
        */
        $month = intval(date('m'));
        $year = intval(date('Y'));
        $year_1 = intval(date('y'));
        if($month >= 4 && $month <=9){
            $semester = 'SoSe '.$year;
            $semester_2 = 'WiSe '. ($year-1).'/'.$year_1;
            $semester_3 = 'SoSe '. ($year);
            $semester_4 = 'WiSe '. ($year-1).'/'. $year_1;
        }   else{
            $semester = 'WiSe '. ($year).'/'.($year_1+1);          
            $semester_2 = 'SoSe '. ($year);
            $semester_3 = 'WiSe '. ($year).'/'.($year_1+1);   
            $semester_4 = 'SoSe '. ($year);
        }
        $minute = 60;
        Cache::put('sem_1', $semester, $minute);
        Cache::put('sem_2', $semester_2, $minute);
        Cache::put('sem_3', $semester_3, $minute);
        Cache::put('sem_4', $semester_4, $minute);

        /*
            Session::put('sem_1', $semester);
            Session::put('sem_2', $semester_2);
            Session::put('sem_3', $semester_3);
            Session::put('sem_4', $semester_4);
        */

        $request = Request::input();
        $tab = 1;
        if (!empty($request)) {
            $tab = Request::input('tab');
            $semester_input = array_pop($request);
            switch($tab){
                case 4:{
                    Cache::put('sem_4', $semester_input,$minute);
                    break;
                }
                case 3:{
                    Cache::put('sem_3', $semester_input,$minute);
                    //Session::put('sem_3', $semester_input);
                    break;
                }
                case 2:{
                    Cache::put('sem_2', $semester_input, $minute);
                    //Session::put('sem_2', $semester_input);
                    break;
                }
                case 1:{
                    Cache::put('sem_1', $semester_input,$minute);
                    //Session::put('sem_1', $semester_input);
                    break;
                }
            }
            $semester = Cache::pull('sem_1');
            $semester_2 = Cache::pull('sem_2');
            $semester_3 = Cache::pull('sem_3');
            #$semester_4 = Session::get('sem_4');
            #BaseController::_setTrace($tab);
        }

        $data = Course::getCourseData($semester);
        #BaseController::_setTrace($data);
        ####################  make modules as array  #####################################
        $modules_1 = json_decode(json_encode(Module::getModules($semester)), true);
        $modules_2 = json_decode(json_encode(Module::getModules($semester_2)), true);
        $modules_all = json_decode(json_encode(Module::getAllModules()), true);
        #BaseController::_setTrace($modules_1);
        $semester_2_data = Course::getCourseData($semester_2);
        $semester_3_data = Course::getCourseData($semester_3);
        /*$semester_4_data = Course::getCourseData($semester_4);*/
        #BaseController::_setTrace($semester_3_data);
        #######################################  My Course Info ##################################################
        $course_sem_1 = array_column(json_decode(json_encode(DB::table('semester_courses')->select('course_id')->where('user_id', '=', $user_id)->where('semester', '=', 1)->get()),true), 'course_id');       
        ########    get module id wrt course id    #######
        $module_sem_1 = array();
        $module_sem_2 = array();
        $module_sem_3 = array();
        if(!empty($course_sem_1)){            
            foreach ($course_sem_1 as $key => $course_id) {                
                $module_sem_1[] = strval(DB::table('courses')->select('module_id')->where('id', '=', $course_id)->first()->module_id);
            }            
        }        
        $course_sem_2 = array_column(json_decode(json_encode(DB::table('semester_courses')->select('course_id')->where('user_id', '=', $user_id)->where('semester', '=', 2)->get()),true), 'course_id');
        
        if(!empty($course_sem_2)){            
            foreach ($course_sem_2 as $key => $course_id) {                
                $module_sem_2[] = strval(DB::table('courses')->select('module_id')->where('id', '=', $course_id)->first()->module_id);
            }            
        }   
        $course_sem_3 = array_column(json_decode(json_encode(DB::table('semester_courses')->select('course_id')->where('user_id', '=', $user_id)->where('semester', '=', 3)->get()),true), 'course_id');
        if(!empty($course_sem_3)){            
            foreach ($course_sem_3 as $key => $course_id) {                
                $module_sem_3[] = strval(DB::table('courses')->select('module_id')->where('id', '=', $course_id)->first()->module_id);
            }            
        }   

        #BaseController::_setTrace($course_sem_1);

        $credit_1 = array_column(json_decode(json_encode(DB::table('soft_skills')->select('credit')->where('user_id', '=', $user_id)->where('semester', '=', 1)->get()),true), 'credit');
        $credit_2 = array_column(json_decode(json_encode(DB::table('soft_skills')->select('credit')->where('user_id', '=', $user_id)->where('semester', '=', 2)->get()),true), 'credit');
        $credit_3 = array_column(json_decode(json_encode(DB::table('soft_skills')->select('credit')->where('user_id', '=', $user_id)->where('semester', '=', 3)->get()),true), 'credit');

        ####################################### My Softskill########################################################
        $my_softskill_sem_1 = json_decode(json_encode(DB::table('soft_skills')->where('user_id', '=', $user_id)->where('semester', '=', 1)->get()),true);
        $my_softskill_sem_2 = json_decode(json_encode(DB::table('soft_skills')->where('user_id', '=', $user_id)->where('semester', '=', 2)->get()),true);
        $my_softskill_sem_3 = json_decode(json_encode(DB::table('soft_skills')->where('user_id', '=', $user_id)->where('semester', '=', 3)->get()),true);
        #BaseController::_setTrace($my_softskill_sem_1);

        ########################################## My Research Project & Theses ##################################################
        $my_research_project = json_decode(json_encode(DB::table('research_projects')->where('user_id', '=', $user_id)->first()),true);
        $my_master_theses = json_decode(json_encode(DB::table('master_theses')->where('user_id', '=', $user_id)->first()),true);
        $theses_name = $my_master_theses['master_theses_name'];
        $my_research_project = json_decode(json_encode(DB::table('research_projects')->where('user_id', '=', $user_id)->first()),true);
        $my_project_2 = null;
        $my_project_3 = null;
        if($my_research_project['semester']==2){
            $my_project_2 = $my_research_project['research_projects_name'];
        }else{
            $my_project_3 = $my_research_project['research_projects_name'];
        }

        $session_1 = array();
        $session_2 = array();
        $session_3 = array();
        $session_1 = array_column(json_decode(json_encode(DB::table('semester_courses')->select('session')->where('user_id', '=', $user_id)->where('semester', '=', 1)->get()),true), 'session');
        $session_2 = array_column(json_decode(json_encode(DB::table('semester_courses')->select('session')->where('user_id', '=', $user_id)->where('semester', '=', 2)->get()),true), 'session');
        $session_3 = array_column(json_decode(json_encode(DB::table('semester_courses')->select('session')->where('user_id', '=', $user_id)->where('semester', '=', 3)->get()),true), 'session');
        /*foreach ($module_sem_1 as $key => $value) {
            $session_1[$key] = strval(DB::table('semester_courses')->select('session')->where('id', '=', $value)->first()->session);
        }
        foreach ($module_sem_2 as $key => $value) {
            $session_2[$key] = strval(DB::table('semester_courses')->select('session')->where('id', '=', $value)->first()->session);
        }
        foreach ($module_sem_3 as $key => $value) {
            $session_3[$key] = strval(DB::table('semester_courses')->select('session')->where('id', '=', $value)->first()->session);
        }*/

        #BaseController::_setTrace($session_1);
        return View::make('layouts.studyplan.editStudyPaln',
            array(
                'data' => $data,
                'semester_2_data'=> $semester_2_data, 
                'modules_1' => $modules_1,
                'modules_2' => $modules_2,
                'modules_all' => $modules_all,
                'semester_3_data' => $semester_3_data,
                #'semester_4_data'=> $semester_4_data,
                'tab' => $tab,
                'semester_1_name' => $semester,
                'semester_2_name' => $semester_2,
                 'semester_3_name' => $semester_3,
                'course_sem_1' => $module_sem_1,        ## passing module id
                'course_sem_2' => $module_sem_2,
                'course_sem_3' => $module_sem_3,
                'credit_1' => $credit_1,
                'credit_2' => $credit_2,
                'credit_3' => $credit_3,
                'session_1' => $session_1,
                'session_2' => $session_2,
                'session_3' => $session_3,
                'my_softskill_sem_1' => $my_softskill_sem_1,            
                'my_softskill_sem_2' => $my_softskill_sem_2,            
                'my_softskill_sem_3' => $my_softskill_sem_3,  
                'my_project_2' => $my_project_2,
                'my_project_3' => $my_project_3,          
                'my_research_project' => $my_research_project,
                'my_master_theses' => $theses_name,
            )
        );         
        /*$editPlan = DB::table('study_plans')->where('user_id', '=', $user_id)->first();
        $editcourse = DB::table('semester_courses')->where('user_id', '=', $user_id)->get();
        BaseController::_setTrace($editcourse);*/
    }

//    public function ajaxPlan($user_id = null){
//        $month = intval(date('m'));
//        $year = intval(date('Y'));
//        $year_1 = intval(date('y'));
//        if($month >= 4 && $month <=9){
//            $semester = 'SoSe '.$year;
//            $semester_2 = 'WiSe '. ($year-1).'/'.$year_1;
//            $semester_3 = 'SoSe '. ($year);
//            $semester_4 = 'WiSe '. ($year-1).'/'. $year_1;
//        }   else{
//            $semester = 'WiSe '. ($year-1).'/'.$year_1;
//        }
//        Cache::put('sem_1', $semester);
//        Cache::put('sem_2', $semester_2);
//        Cache::put('sem_3', $semester_3);
//        Cache::put('sem_4', $semester_4);
//
//        /*Session::put('sem_1', $semester);
//        Session::put('sem_2', $semester_2);
//        Session::put('sem_3', $semester_3);
//        Session::put('sem_4', $semester_4);*/
//
//        $request = Request::input();
//        $tab = 1;
//        if (!empty($request)) {
//            $tab = Request::input('tab');
//            $semester_input = array_pop($request);
//            switch($tab){
//                case 4:{
//                    Cache::put('sem_4', $semester_input);
//                    break;
//                }
//                case 3:{
//                    Cache::put('sem_3', $semester_input);
//                    //Session::put('sem_3', $semester_input);
//                    break;
//                }
//                case 2:{
//                    Cache::put('sem_2', $semester_input);
//                    //Session::put('sem_2', $semester_input);
//                    break;
//                }
//                case 1:{
//                    Cache::put('sem_1', $semester_input);
//                    //Session::put('sem_1', $semester_input);
//                    break;
//                }
//            }
//            $semester = Cache::pull('sem_1');
//            $semester_2 = Cache::pull('sem_2');
//            $semester_3 = Cache::pull('sem_3');
//            #$semester_4 = Session::get('sem_4');
//        }
//        $data = Course::getCourseData($semester);
//        ####################  make modules as array  #####################################
//        $modules_1 = json_decode(json_encode(Module::getModules($semester)), true);
//        $modules_2 = json_decode(json_encode(Module::getModules($semester_2)), true);
//        #BaseController::_setTrace($modules_1);
//        $semester_2_data = Course::getCourseData($semester_2);
//        $semester_3_data = Course::getCourseData($semester_3);
//        /*$semester_4_data = Course::getCourseData($semester_4);*/
//        #BaseController::_setTrace($data);
//        #######################################  My Course Info ##################################################
//        $course_sem_1 = json_decode(json_encode(DB::table('semester_courses')->where('user_id', '=', $user_id)->where('semester', '=', 1)->get()),true);
//        $course_sem_2 = json_decode(json_encode(DB::table('semester_courses')->where('user_id', '=', $user_id)->where('semester', '=', 2)->get()),true);
//        $course_sem_3 = json_decode(json_encode(DB::table('semester_courses')->where('user_id', '=', $user_id)->where('semester', '=', 3)->get()),true);
//        #BaseController::_setTrace($course_sem_3);
//
//        ####################################### My Softskill########################################################
//        $my_softskill_sem_1 = json_decode(json_encode(DB::table('soft_skills')->where('user_id', '=', $user_id)->where('semester', '=', 1)->get()),true);
//        $my_softskill_sem_2 = json_decode(json_encode(DB::table('soft_skills')->where('user_id', '=', $user_id)->where('semester', '=', 2)->get()),true);
//        $my_softskill_sem_3 = json_decode(json_encode(DB::table('soft_skills')->where('user_id', '=', $user_id)->where('semester', '=', 3)->get()),true);
//        #BaseController::_setTrace($my_softskill_sem_1);
//
//        ########################################## My Research Project & Theses ##################################################
//        $my_research_project = json_decode(json_encode(DB::table('research_projects')->where('user_id', '=', $user_id)->first()),true);
//        $my_master_theses = json_decode(json_encode(DB::table('master_theses')->where('user_id', '=', $user_id)->first()),true);
//        #BaseController::_setTrace($my_research_project);
//
//
//    }


    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        //
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    public function selectMentor(){
        $study_plans = array();
        $keyword = Request::input('keyword');
        #BaseController::_setTrace($keyword);
        $study_plans = StudyPlan::allStudyPlans($keyword);
        return View::make('layouts.studyplan.selectMentor',
            array(
                'study_plans' => $study_plans
            )
        );
    }
}
