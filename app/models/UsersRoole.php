<?php

class UsersRoole extends \Eloquent {
	protected $fillable = [];

    /*
     *
     */
     //get role id
    //created by huaun
    public static function getRooleId($user_id = null) {
        $data = DB::table('users_rooles')->where('user_id', $user_id)->get();
        $res = json_decode(json_encode($data), true);
        return array_pop($res);
    }
    //To check the roles of user
    public static function getRool($user_id = null) {
        $data = DB::table('users_rooles')
        ->select('rooles.role')
        ->join('rooles', 'users_rooles.roole_id', '=', 'rooles.id')
        ->where('user_id', $user_id)
        ->first();
        $res = json_decode(json_encode($data), true);
        //BaseController::_setTrace($res);
        return $res['role'];
    }
}