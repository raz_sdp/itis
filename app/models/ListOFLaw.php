<?php
 
class ListOFLaw {

 //list of laws with respected role_id to insert into table
  public static function getLaw() {

    $laws = [
        ['name' => 'Logout', 'controller_and_action' => 'LoginController@doLogout', 'role' => 7],
        ['name' => 'Request Create Course Form', 'controller_and_action' => 'CourseController@create', 'role' => 4],
        ['name' => 'Store New Course', 'controller_and_action' => 'CourseController@store', 'role' => 4],
        ['name' => 'Show All Courses', 'controller_and_action' => 'CourseController@index', 'role' => 7],
        ['name' => 'Sort Course by year and semester', 'controller_and_action' => 'CourseController@sortByReq', 'role' => 7],
        ['name' => 'Request Course Edit Form', 'controller_and_action' => 'CourseController@edit', 'role' => 5],
        ['name' => 'Update Course','controller_and_action' => 'CourseController@update', 'role' => 5],
        ['name' =>  'Delete A Course', 'controller_and_action' => 'CourseController@delete', 'role' => 2],
        ['name' =>  'Undo Delete', 'controller_and_action' => 'CourseController@undoDelete', 'role' => 2],
        ['name' =>  'show all users', 'controller_and_action' => 'UserController@index', 'role' => 3],
        ['name' =>  'sort user by university and role', 'controller_and_action' => 'UserController@sortByReq', 'role' => 3],
        ['name' =>  'request user edit form', 'controller_and_action' => 'UserController@edit', 'role' => 7],
        ['name' =>  'update user', 'controller_and_action' => 'UserController@update', 'role' => 7],
        ['name' =>  'request edit password form', 'controller_and_action' => 'UserController@editpassword', 'role' => 7],
        ['name' =>  'update password', 'controller_and_action' => 'UserController@updatepassword', 'role' => 7],
        ['name' =>  'request create user form', 'controller_and_action' => 'UserController@create', 'role' => 3],
        ['name' =>  'store new user', 'controller_and_action' => 'UserController@store', 'role' => 3],
        ['name' =>  'delete a user', 'controller_and_action' => 'UserController@deleteUser', 'role' => 3],
        ['name' =>  'undo delete a user', 'controller_and_action' => 'UserController@undoDeleteUser', 'role' => 3],
        ['name' =>  'view as', 'controller_and_action' => 'UserController@viewAs', 'role' => 7],
        ['name' =>  'show all logs', 'controller_and_action' => 'LogController@index', 'role' => 3],
        ['name' =>  'undo anything from log', 'controller_and_action' => 'LogController@undo', 'role' => 3],
        ['name' =>  'view full detail of log', 'controller_and_action' => 'LogController@viewLog', 'role' => 3],
        ['name' =>  'delete anything by log', 'controller_and_action' => 'LogController@checkDelete', 'role' => 3],
        ['name' =>  'undo delete anything by log', 'controller_and_action' => 'LogController@checkUndoDelete', 'role' => 3],
        ['name' =>  'view the history of the log', 'controller_and_action' => 'LogController@history', 'role' => 3],
        ['name' =>  'request create module form', 'controller_and_action' => 'ModuleController@create', 'role' => 3],
        ['name' =>  'save a module', 'controller_and_action' => 'ModuleController@store', 'role' => 3],
        ['name' =>  'show all modules', 'controller_and_action' => 'ModuleController@index', 'role' => 5],
        ['name' =>  'sort modules', 'controller_and_action' => 'ModuleController@sortByReq', 'role' => 5],
        ['name' =>  'request edit form to a module', 'controller_and_action' => 'ModuleController@edit', 'role' => 3],
        ['name' =>  'update a module', 'controller_and_action' => 'ModuleController@update', 'role' => 3],
        ['name' =>  'delete a module', 'controller_and_action' => 'ModuleController@destroy', 'role' => 3],
        ['name' =>  'undo delete a module', 'controller_and_action' => 'ModuleController@undoDelete', 'role' => 3],

        
        // ['name' =>  '', 'controller_and_action' => , 'role' => ],
        // ['name' =>  '', 'controller_and_action' => , 'role' => ],
        // ['name' =>  '', 'controller_and_action' => , 'role' => ],
        // new rules
        // ---------------------------------------------------------------------------------
        ['name' =>  'Language change', 'controller_and_action' => 'UserController@changeLanguage', 'role' => 7],
        ['name' =>  'change the mode of view', 'controller_and_action' => 'HelperController@changeViewMode', 'role' => 7],
        ['name' =>  'Show al courses with page number', 'controller_and_action' => 'CourseController@indexWithPage', 'role' => 7],
        ['name' =>  'Show all user with page number', 'controller_and_action' => 'UserController@indexWithPage', 'role' => 3],
        // laws from fatema
        ['name' =>  'Show user with detail', 'controller_and_action' => 'UserController@details', 'role' => 3],
        ['name' =>  'show logs with page number', 'controller_and_action' => 'LogController@indexWithPage', 'role' => 3],
        ['name' =>  'show module with page number', 'controller_and_action' => 'ModuleController@indexWithPage', 'role' => 3],
        ['name' =>  'export module', 'controller_and_action' => 'ModuleController@export', 'role' => 3],
        ['name' =>  'show the help', 'controller_and_action' => 'HelpController@index', 'role' => 7],
        ['name' =>  'import form', 'controller_and_action' => 'ImportController@index', 'role' => 2],
        ['name' =>  'uploading file to import operation', 'controller_and_action' => 'ImportController@uploader', 'role' => 2],
        ['name' =>  'study plan form', 'controller_and_action' => 'StudyPlanController@createStudyPlan', 'role' => 6],
        ['name' =>  'user export', 'controller_and_action' => 'UserController@export', 'role' => 3],
        ['name' =>  'Log export', 'controller_and_action' => 'LogController@export', 'role' => 3],
        ['name' =>  'course export', 'controller_and_action' => 'CourseController@export', 'role' => 3],
        ['name' =>  'download xml file', 'controller_and_action' => 'HelperController@getFile', 'role' => 3],
        ['name' =>  'download pdf file', 'controller_and_action' => 'HelperController@getPdfFile', 'role' => 3],
        ['name' =>  'List of contact of user', 'controller_and_action' => 'UserController@studentContacts', 'role' => 7],
        ['name' =>  'Sort the list of contact', 'controller_and_action' => 'UserController@studentContacts', 'role' => 7],
        ['name' =>  'Course store in study place', 'controller_and_action' => 'SemesterCoursesController@courseStore', 'role' => 6],
        ['name' =>  'Create study plan by a student', 'controller_and_action' => 'StudyPlanController@createStudyPlan', 'role' => 6],
        ['name' =>  'Select a mentor', 'controller_and_action' => 'StudyPlanController@selectMentor', 'role' => 2],
        ['name' =>  'Update a mentor', 'controller_and_action' => 'SemesterCoursesController@updateMentor', 'role' => 2],
        ['name' =>  'Edit a study plan', 'controller_and_action' => 'StudyPlanController@edit', 'role' => 2],
        ['name' =>  'Edit courses', 'controller_and_action' => 'SemesterCoursesController@editCourse', 'role' => 2],
        ['name' =>  'Get latest courses', 'controller_and_action' => 'SemesterCoursesController@getLatestCourses', 'role' => 2],
        ['name' =>  'Sort the contacts', 'controller_and_action' => 'UserController@sortContactByReq', 'role' => 7],
        ['name' =>  'Generate password and send to user', 'controller_and_action' => 'UserController@generatePasswordAndSend', 'role' => 2]
      ];
    return $laws;
  }
}