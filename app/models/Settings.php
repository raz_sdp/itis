<?php
 
class Settings extends Eloquent {

  public static function listOfTables() {
    return array("categories", "courses", "laws", "laws_rooles", "logs", "master_theses", "migrations", "modules", "research_projects", "rooles", "semester_courses", "sites", "soft_skills", "study_plans", "users", "users_rooles");
  }



  public static function isMigrationSuccess() {
    $tables = Settings::listOfTables();
    while (sizeof($tables) != 0) {
      $table = array_shift($tables);
      $query = 'select * from '. $table;
      $results = DB::select($query);
    }
  }

  public static function successMessageString() {
    return "<b><h3>Welcome to ITIS DB </h3><hr/></b><br/><h4>1. All tables created Successfully <br/><br/>2. Tables seeded Successfully <br/><br/>3. Laws inserted Successfully </h4>";
  }


  public static function sizeOfTable($table) {
    return sizeof(DB::table($table)->get());
  }

  public static function superAdminCreatedMsg() {
    return Settings::successMessageString() ."<h4><br/>4. Super Admin Created successfully<br/><br/></h4><h2><b><hr/>Installation Completed Successfully</b></h2>";
  }



  public static function isSeedingAndInsertSuccess() {
    $seededTables = array("categories", "rooles", "sites", "laws", "laws_rooles");
    $successMsg = Settings::successMessageString();
    $messageAndClass = array($successMsg, "green-background", "super_admin_create");

    while (sizeof($seededTables) != 0) {
      $table = array_shift($seededTables);
      $results = Settings::sizeOfTable($table);

      if ($results == 0 && $table == "laws") {
        Law::insertIntoLaw();
        $results = Settings::sizeOfTable($table);
      }

      if ($results <= 0) {
        $errorMsg = "<b><h3>Welcome to ITIS DB </h3><hr/></b><br/><h4> You have Error in Seeding or Inserting Law </b><br/><br/> Please correct them and try again </h4>";
        $messageAndClass = array($errorMsg, "red-background", "error");
      }
    }

    if (Settings::sizeOfTable("users") >= 1) {
      $successMsg = settings::superAdminCreatedMsg();
      $messageAndClass = array($successMsg, "green-background", "show_login");
    }
    return $messageAndClass;
  }

}