<?php

class StudyPlan extends \Eloquent {
	protected $fillable = [];
    protected $table = 'study_plans';
    //blacklist in import
    protected $guarded = array('id', 'created_at', 'updated_at');

    public static function getAllPlans($keyword,$page=10){
        if(!empty($keyword)){
            $data = DB::table('study_plans')
                ->rightjoin('users', 'study_plans.user_id', '=', 'users.id')
                ->select('study_plans.*', 'users.first_name','users.last_name','users.id as user_table_id')
                ->orWhere('matriculation', 'LIKE', '%' . $keyword . '%')
                ->orWhere('mentor_name', 'LIKE', '%' . $keyword . '%')
                ->orWhere('users.first_name', 'LIKE', '%' . $keyword . '%')
                ->orWhere('users.last_name', 'LIKE', '%' . $keyword . '%')
                ->orderBy('study_plans.id','ASC')
                ->paginate($page);
        } else{
            $data = DB::table('study_plans')
                ->rightjoin('users', 'study_plans.user_id', '=', 'users.id')
                ->select('study_plans.*', 'users.first_name','users.last_name', 'users.id as user_table_id')
                //->get();
                ->orderBy('study_plans.id','ASC')
                ->paginate($page);
        }
        //$data = json_decode(json_encode($data), true);
        return $data;
    }

    public static function allStudyPlans($keyword=null, $page=10){
        if(!empty($keyword)){
            $data = DB::table('study_plans')
                ->join('users', 'study_plans.user_id', '=', 'users.id')
                ->select('study_plans.*', 'users.first_name','users.last_name')
                ->orWhere('matriculation', 'LIKE', '%' . $keyword . '%')
                ->orWhere('mentor_name', 'LIKE', '%' . $keyword . '%')
                ->orWhere('users.first_name', 'LIKE', '%' . $keyword . '%')
                ->orWhere('users.last_name', 'LIKE', '%' . $keyword . '%')
                ->paginate($page);
        } else{
            $data = DB::table('study_plans')
                ->join('users', 'study_plans.user_id', '=', 'users.id')
                ->select('study_plans.*', 'users.first_name','users.last_name')
                //->get();
                ->paginate($page);
        }
        //$data = json_decode(json_encode($data), true);
        return $data;
    }

    public static function getIndividualPlan($user_id = null) {
        $data = DB::table('study_plans')            
            ->select('study_plans.id','study_plans.mentor_name')
            ->where('user_id', '=', $user_id)
            ->first();
        $data = json_decode(json_encode($data), true);
        return $data;
    }

    //this function is used for getting the last updated columns in module table
    public static function getEditedColumn($mId){   

        $output="";
        $tables= DB::table('study_plans')->where('id', $mId)->get();
        #BaseController::_setTrace($tables);
        foreach ($tables as $table) {

            foreach ($table as $key => $value) {

                if($key=='topic_category') {
                    $i=input::get('topicCategory');
                } else {
                    $i=input::get($key);
                }

                if($value!=$i&& $key!='updated_at'&&$key!='created_at'&&$key!='visible') { 
                    $output .=$key.',';
                }
            }
        }
        return($output);
    }
}