<?php
 
class SendMail extends Eloquent {

  public static function sendEmail($view, $array, $to, $subject) {
    // echo "passsword  ". $view ."  to email  ". $to ."   subject  ". $subject ."<br/><br/><br/>";
    // print_r($array);
     Mail::send($view, $array, function($message)  use ($to, $subject) {
      $message->to($to)->subject($subject);
    });
  }

}