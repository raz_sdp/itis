<?php

class ResearchProject extends \Eloquent {
	protected $fillable = [];
    protected $table = 'research_projects';
    //blacklist in import
    protected $guarded = array('id', 'created_at', 'updated_at');
}