<?php
 
class Import extends Eloquent {

  //upload the file to server, save in location 'app/docs' and check the file format csv or not
  public static function storeFile() {
    $destinationPath = 'app/docs'; // upload path
    $file = Input::file('toImport');
    $fileName = $file->getClientOriginalName();

    $file->move($destinationPath, $fileName); // moving file to given path
    $ext = pathinfo($fileName, PATHINFO_EXTENSION);

    if ($ext == "csv") {
      Import::csvToArray($fileName);
    } else {
      echo $ext ." file not acceptable ! <br /> csv file only acceptable";
    }
  }




  // identify the table to store using file name and call the function to store
  public static function storeToDB($fileName, $bigArray) {

    foreach ($bigArray as $arrayToStore) {
      switch ($fileName) {
        case 'users.csv':
          $password = $arrayToStore['password'];
          $arrayToStore['password'] = Hash::make($password);
          Import::storeWithUsersRoolesPivot($arrayToStore, $password);
          break;

        case 'modules.csv':
          Module::create($arrayToStore);
          break;

        case 'courses.csv':
          Course::create($arrayToStore);
          break;                
        
        default:
          // nothing to do
          break;
      }
    }
  }




  public static function checkValidation($fileName, $arrayToCheck, $rowNr) {
    switch ($fileName) {
      case 'users.csv':
        $rules = User::validationRuleMakerForUpload();
        $validator = Validator::make($arrayToCheck, $rules);
        return Import::doValidation($arrayToCheck, $rules,  $rowNr);
        break;

      case 'modules.csv':
        $rules = Module::validationRuleMaker($arrayToCheck);
        return Import::doValidation($arrayToCheck, $rules,  $rowNr);
        break;

      case 'courses.csv':
        $rules = Course::validationRuleMaker($arrayToCheck);
        return Import::doValidation($arrayToCheck, $rules,  $rowNr);
        break;                
      
      default:
        echo "<center><br /> <br /> file name not correct! ".$fileName; echo "<br /> Please read readme.txt file to name your csv file"; echo "<br />";
        echo HTML::link('/showallcourses','Back');
        echo "</center>";
        exit();
        break;
    }    

  }




  public static function doValidation($arrayToCheck, $rules, $rowNr) {
    $validator = Validator::make($arrayToCheck, $rules);
    if ($validator->fails()) {
      return $validator->messages()->tojson();
    } else { return 0; }
  }




  //make array to store from csv file
  public static function csvToArray($fileName, $delimiter=',') {
    $rowNr                = 0;
    $errorMessages['']    = '';
    $emails['']           = '';
    $location             = 'app/docs/'.$fileName;
    $file                 = fopen($location, "r");
    $header               = fgetcsv($file, 0, $delimiter);
    $arrayToStore         = array();
    $modulesInCourse['']  = '';

    while (($line = fgetcsv($file, 0, $delimiter)) != FALSE) {
      foreach ($line as $index => $column) {
        if ($header[$index] != '') {

          if ($fileName == "modules.csv" && $header[$index] == "topic_category") {
            $column = Category::getTopicCategoryId($column);

          } elseif (($fileName == "modules.csv" || $fileName == "courses.csv") && ($header[$index] == "site" || $header[$index] == "site_2")) {
            $column = Site::getSiteId($column);

          } elseif ($fileName == "users.csv" && $header[$index] == "email") {
            $emails[$rowNr] = $column;

          } elseif ($fileName == "users.csv" && $header[$index] == "role_id") {
            $column = Roole::getRoleId($column);

          } elseif ($fileName == "users.csv" && $header[$index] == "university") {
            $column = Site::getSiteId($column);
          
          } elseif ($fileName == "users.csv" && $header[$index] == "password") {
            $column = User::randomPassword();

          } elseif ($fileName == "courses.csv" && $header[$index] == "module") {
            $moduleName               = $column;
            $modulesInCourse[$rowNr]  = $column;

          } elseif ($fileName == "courses.csv" && $header[$index] == "module_id") {
            $column = Module::getModuleId($moduleName);
          }
          
          $row[$header[$index]] = $column;
          
        }
      }
      $errorMessages[$rowNr++] = Import::checkValidation($fileName, $row, $rowNr);
      array_push($arrayToStore, $row);
    }

    $errorMessages = Import::makeErrorViewArray($errorMessages);

    if ($fileName == "users.csv") {
      $emailErrors = Import::findDuplicateEmailError(array_filter($emails));
      $errorMessages = array_merge($emailErrors, $errorMessages);
    }

    if ($fileName == "courses.csv") {
      $modulesNameError = Import::findModuleNameErrors($modulesInCourse); 
      $errorMessages = array_merge($modulesNameError, $errorMessages);
    }

    $noOfE = sizeof(array_filter($errorMessages));
    if ($noOfE == 0) {
      Import::storeToDB($fileName, $arrayToStore);
      echo View::make('layouts.messages.successMessage', ['fileName' => $fileName]);
    } else {
      echo View::make('layouts.settings.importError', ['errors' => $errorMessages]);
    }
    fclose($file);
  }




  public static function findModuleNameErrors($modules) {
    $bigArray     = array();
    $smallArray   = array();
    $modules      = array_filter($modules);

    foreach ($modules as $key => $value) {
      if (Module::isModuleExistByModuleName($value)) {
        $rowNr = $key +2;
        $smallArray = [$rowNr, 'Module', 'Module name does not exist on Module table'];
        array_push($bigArray, $smallArray);
      }
    }
    return $bigArray;
  }


  

  public static function findDuplicateEmailError($emails) {
    $smallArray = array();
    $bigArray = array();
    foreach ($emails as $key => $value) {
      foreach ($emails as $k => $v) {
        if ($value == $v && $key != $k) {
          $rowNr = $key +2;
          $smallArray = [$rowNr, 'email', 'Duplication found in users.cvf file. Email must be unique'];
          array_push($bigArray, $smallArray);
        }
      }
    }
    return $bigArray;
  }




  public static function makeErrorViewArray($errors) {
    $errorsArray = array();
    foreach ($errors as $key => $value) {
      if (!empty($value)) {
        $value = (array) $value;
        foreach ($value as $k => $v) {
          $v = explode( ',', $v);
          foreach ($v as $a) {
            $a = explode( ':', $a);
            $a[0] = preg_replace('/[^A-Za-z0-9\-]/', '', $a[0]);
            $a[1] = preg_replace('/[^A-Za-z0-9\-]/', ' ', $a[1]);
            $row = array(($key + 2), $a[0], $a[1]);
            array_push($errorsArray, $row);
          }
        }
      }
    }
    return $errorsArray;
  }



  //this function only for user table because its need to update the user_roles pivot tabble and create session
  public static function storeWithUsersRoolesPivot($arrayToStore, $password){
    $roleId = $arrayToStore['role_id'];
    unset($arrayToStore['role_id']);

    User::create($arrayToStore);
    $userId = DB::getPdo()->lastInsertId();
    $user = User::find($userId);
    $user->rooles()->attach($roleId); 
    Session::put($userId, $roleId);

    // send auto generated password to user email
    $name                 = User::userMitName($userId);
    $array                = array('password' => $password, 'name' => $name, 'to' => $arrayToStore['email']);
    SendMail::sendEmail('layouts.mails.password', $array, $arrayToStore['email'], 'Your new ITIS password');
  }  
}