<?php
 
class Category extends Eloquent {
    
  protected $table = 'categories';

  public static function getFirstTpoicCategoryId() {
    $category = DB::table('categories')->first();
    return $category->id;
  }
  
  public static function getTopicCategoryId($topicCategory){
  	$firstTpoicCategoryId = Category::getFirstTpoicCategoryId();
  	switch ($topicCategory) {
  		case 'Theoretical Foundations':
  			return $firstTpoicCategoryId;
  			break;

  		case 'Data and Information':
  			return $firstTpoicCategoryId + 1;
  			break;
  		
  		case 'Networking and Communication':
  			return $firstTpoicCategoryId + 2;
  			break;
  		
  		case 'Business and Law':
  			return $firstTpoicCategoryId + 3;
  			break;
  		
  		case 'Soft Skills':
  			return $firstTpoicCategoryId + 4;
  			break;
  		
  		default:
  			return $firstTpoicCategoryId;
  			break;
  	}
  }

  // Get name of the Category using its id
  public static function getCategoryName($id) {
    return Category::where('id', $id)->pluck('category_name');
	}
    
  // To take all category names into an array from category table
  // this array display in a dropdown list 
	public static function makeCategoryArray() {
		
		$categories = Category::all();
		$categoryToPush[''] = 'Topic Category';

		foreach ($categories as $category) {
			$categoryToPush[$category->id] = $category->category_name;
		}
		return $categoryToPush;
	}
}