/**
 * Created by nayan on 4/21/15.
 */

$(function () {  
    var categories_1_total_credit = 0;
    var categories_2_total_credit = 0;
    var categories_3_total_credit = 0;
    var categories_4_total_credit = 0;
    var edit_handle = {
        1: 'home',
        2: 'profile',
        3: 'messages',
        4: 'settings',
        5: 'plan_details',
        6: 'finish'
    };

    function disselect_all(){
        $.each($('input[name="course"]'), function(key,item){
            $(item).parent().parent().removeClass('success').addClass('info');
            $(item).prop("checked",false);
        });      
    }

    if($(".js-nav li.active").prop('id')){
        var active_tab = $(".js-nav li.active").prop('id').split('_').pop();
        select_from_local(active_tab);                        
    }  


// To keep selected course after giving page refresh of back
    function select_from_local(active_tab) {
        console.log(active_tab);    
        var  local_semester_1 = JSON.parse(localStorage.getItem('semester_1')); 
        //console.log(local_semester_1);  
        if(!local_semester_1){
            local_semester_1 = [];
        }
        var local_semester_2 = JSON.parse(localStorage.getItem('semester_2'));
        if(!local_semester_2){
            local_semester_2 = [];
        }
        var local_semester_3 = JSON.parse(localStorage.getItem('semester_3'));
        if(!local_semester_3){
            local_semester_3 = [];
        }   
        var local_session_1 = JSON.parse(localStorage.getItem('session_1'));
        if(!local_session_1){
            local_session_1 = [];
        }                 
        var local_session_2 = JSON.parse(localStorage.getItem('session_2'));
        if(!local_session_2){
            local_session_2 = [];
        }
        var local_session_3 = JSON.parse(localStorage.getItem('session_3'));
        if(!local_session_3){
            local_session_3 = [];
        }
        //console.log(local_semester_1);
        //console.log(local_semester_2);
        //console.log(local_semester_3);

        //$.each($('input[name="course"]'), function(key,item){
            //var active_tab = $(".js-nav li.active").prop('id').split('_').pop();
            //console.log(active_tab)
            if(active_tab==1) {
                if(local_semester_1) {
                    //console.log(local_session_1);
                    //console.log($('#sem_1_ses').val());
                    //console.log($('.course_1'))
                    $.each($('.course_1'), function(key,item){                        
                        var found = $.inArray($(item).val(),local_semester_1);
                        //console.log(found);
                        if(found != -1 ) {
                            if( local_session_1[found] == $('#sem_1_ses').val() ) {                                                       
                                $(item).parent().parent().removeClass('info').addClass('success');
                                $(item).prop("checked",true);                            
                            }
                        }    
                    }); 
                }
            } else if(active_tab==2) {
                if(local_semester_2) {
                    console.log(local_session_2);
                    console.log($('#sem_2_ses').val());
                    //console.log($('.course_2'))
                    $.each($('.course_2'), function(key,item){                        
                        var found = $.inArray($(item).val(),local_semester_2);
                        if(found != -1 ) {
                            if( local_session_2[found] == $('#sem_2_ses').val() ) {                        
                                $(item).parent().parent().removeClass('info').addClass('success');
                                $(item).prop("checked",true);                                                    
                            }
                        }
                    }); 
                }
            } else if(active_tab==3){
                if(local_semester_3) {
                    $.each($('.course_3'), function(key,item){
                        var found = $.inArray($(item).val(),local_semester_3);
                        if(found != -1 ) {
                            if( local_session_3[found] == $('#sem_3_ses').val() ) {                                 
                                $(item).parent().parent().removeClass('info').addClass('success');
                                $(item).prop("checked",true);                                                                        
                            }
                        }  
                    });                
                }
            }
        //});
    }


    function delete_prev_ses(x){
        var semsester = "semester_" + x;
        var session = "session_" + x;
        var credit = "credits_" + x;
        var local_semester = JSON.parse(localStorage.getItem(semsester));
        var local_session = JSON.parse(localStorage.getItem(session));
        var local_credit = JSON.parse(localStorage.getItem(credit));
        var ses_select = "#sem_" + x + "_ses"; 

        //console.log(local_semester);
        if(local_semester) {
            for(var i=0; i<local_session.length; i++) {
                if(local_session[i] == $(ses_select).val() ) {
                    local_session.splice(i,1);
                    local_semester.splice(i,1);
                    local_credit.splice(i,1);
                    i--;
                }
            }
            localStorage.setItem(semsester, JSON.stringify(local_semester));
            localStorage.setItem(session, JSON.stringify(local_session));
            localStorage.setItem(credit, JSON.stringify(local_credit));
        }
    }

    //for controlling next button disable or enable
    function disable_next_btn(x) {
        /*var semester = "semester_" + x;        
        local_semester = JSON.parse(localStorage.getItem(semester));
        if(!local_semester){
            var local_semester = [];
        }
        if(x < 3) {
            console.log('sdsd',local_semester)
            if(local_semester.length!==0) {
                $('.js-step').removeAttr('disabled');
            } else {
                $('.js-step').attr('disabled',true);
            }
        }*/
    }

    //localStorage.clear();
    //To save checked items in local storage
    $('.save_btn').click(function(){
        var active_tab = $(".js-nav li.active").prop('id').split('_').pop();
        var user_id = location.href.split('/').pop();   //get user id from url
        if(user_id=='studyplan'){
            delete_prev_ses(active_tab);
        }        
        if(active_tab==1) {
            var semester_1 = [];
            semester_1 = $('#course_1:checked').map(function (_, el) {
                return $(el).val();
            }).get();
            var session_1 = semester_1;
            var prev_1 = localStorage.getItem('semester_1');            
            if(prev_1) {
                semester_1 = $.merge(JSON.parse(prev_1),semester_1);
            }
            localStorage.setItem('semester_1', JSON.stringify(semester_1));             
            
            $.each(session_1,function(key,item){
                session_1[key] = $('#sem_1_ses').val();    
            });
            var prev_1_ses = localStorage.getItem('session_1');
            if(prev_1_ses) {
                session_1 = $.merge(JSON.parse(prev_1_ses),session_1);
            }
            localStorage.setItem('session_1', JSON.stringify(session_1)); 

            var credits_1 = [];
            credits_1 = $('#course_1:checked').map(function (_, el) {
                return $(el).closest('td').next('td').attr('rel');
            }).get();
            var prev_1_c = localStorage.getItem('credits_1');
            if(prev_1_c) {
                credits_1 = $.merge(JSON.parse(prev_1_c),credits_1);
            }
            localStorage.setItem('credits_1', JSON.stringify(credits_1));

            var modules_1_new = modules_1;
            var prev_1_m = localStorage.getItem('modules_1');
            console.log(prev_1_m)
            if(prev_1_m) {
                modules_1_new = $.merge(JSON.parse(prev_1_m),modules_1_new);
            }
            localStorage.setItem('modules_1', JSON.stringify(modules_1_new));
        } else if(active_tab==2) {
            var semester_2 = [];
            semester_2 = $('#course_2:checked').map(function (_, el) {
                return $(el).val();
            }).get();
            var session_2 = semester_2;
            var prev_2 = localStorage.getItem('semester_2');
            if(prev_2) {
                semester_2 = $.merge(JSON.parse(prev_2),semester_2);
            }
            localStorage.setItem('semester_2', JSON.stringify(semester_2));
            
            $.each(session_2,function(key,item){
                session_2[key] = $('#sem_2_ses').val();    
            });
            var prev_2_ses = localStorage.getItem('session_2');
            if(prev_2_ses) {
                session_2 = $.merge(JSON.parse(prev_2_ses),session_2);
            }
            localStorage.setItem('session_2', JSON.stringify(session_2)); 

            var credits_2 = [];
            credits_2 = $('#course_2:checked').map(function (_, el) {
                return $(el).closest('td').next('td').attr('rel');
            }).get();
            var prev_2_c = localStorage.getItem('credits_2');
            if(prev_2_c) {
                credits_2 = $.merge(JSON.parse(prev_2_c),credits_2);
            }
            localStorage.setItem('credits_2', JSON.stringify(credits_2));

            var modules_2_new = modules_2;
            var prev_2_m = localStorage.getItem('modules_2');
            if(prev_2_m) {
                modules_2_new = $.merge(JSON.parse(prev_2_m),modules_2_new);
            }
            localStorage.setItem('modules_2', JSON.stringify(modules_2_new));
        } else if(active_tab==3) {
            var semester_3 = [];
            semester_3 = $('#course_3:checked').map(function (_, el) {
                return $(el).val();
            }).get();
            var session_3 = semester_3;
            var prev_3 = localStorage.getItem('semester_3');
            if(prev_3) {
                semester_3 = $.merge(JSON.parse(prev_3),semester_3);
            }
            localStorage.setItem('semester_3', JSON.stringify(semester_3));

            $.each(session_3,function(key,item){
                session_3[key] = $('#sem_3_ses').val();    
            });
            var prev_3_ses = localStorage.getItem('session_3');
            if(prev_3_ses) {
                session_3 = $.merge(JSON.parse(prev_3_ses),session_3);
            }
            localStorage.setItem('session_3', JSON.stringify(session_3));

            var credits_3 = [];
            credits_3 = $('#course_3:checked').map(function (_, el) {
                return $(el).closest('td').next('td').attr('rel');
            }).get();
            var prev_3_c = localStorage.getItem('credits_3');
            if(prev_3_c) {
                credits_3 = $.merge(JSON.parse(prev_3_c),credits_3);
            }
            localStorage.setItem('credits_3', JSON.stringify(credits_3));
        }

        console.log(localStorage);
        if(user_id=='studyplan'){
            disable_next_btn(active_tab);
        }
    });

    $('.js-goto-studyplan').on('click', function () {
        $('.js-instruction').hide();
        $('.js-main-content').show();
        var active_tab = $(".js-nav li.active").prop('id').split('_').pop();
        disable_next_btn(active_tab);
    });

    $('.js-semester').on('change', function () {
        $(this).parent().parent('form').submit();
    });

    //click next button and tab enable disable functionality
    if($(".js-nav li.active").prop('id'))
        var active_tab = $(".js-nav li.active").prop('id').split('_').pop();
    
    $('.js-step').on('click', function () {
        //console.log(localStorage);
        //disselect_all();        
        active_tab = $(".js-nav li.active").prop('id').split('_').pop();                    
        var next_tab = parseInt(active_tab) + 1;                    
        if (active_tab <= 4) {
            disable_next_btn(next_tab);
            select_from_local(next_tab);
            $('#form_' + active_tab).validate({
                errorPlacement: function (error, element) {
                }
            }).valid();
            if ($('#form_' + active_tab).valid()) {                
                //$(this).submit();
                var count = active_tab;
                if (active_tab > 3) {
                    count = 3;
                }
                var check_skill = '#soft_skill_' + count;
                var check_credit = '#credit_' + count;
                var check_site = '#site_' + count;

                if (($(check_site).val().length && (!$(check_credit).val().length || !$(check_skill).val().length)) || ($(check_skill).val().length && (!$(check_credit).val().length || !$(check_site).val().length)) || ((!$(check_skill).val().length || !$(check_site).val().length) && $(check_credit).val().length)) {
                    $('.js-alert-danger').fadeIn(400).text('Please Provide the both inputs value or none.');
                } else {
                    $('.js-alert-danger').hide();
                    $('.js-nav li#tab_' + active_tab).removeClass('active').addClass('disabledTab');
                    $('.js-nav li#tab_' + next_tab).removeClass('disabledTab').addClass('active');
                    $('.my-tab #' + edit_handle[active_tab]).removeClass('active');
                    $('.my-tab #' + edit_handle[next_tab]).addClass('active');
                    var semester_1 = [];
                    semester_1 = $('#course_1:checked').map(function (_, el) {
                        return $(el).val();
                    }).get();
                    var semester_2 = [];
                    semester_2 = $('#course_2:checked').map(function (_, el) {
                        return $(el).val();
                    }).get();
                    var semester_3_course = [];
                    semester_3_course = $('#course_3:checked').map(function (_, el) {
                        return $(el).val();
                    }).get();
                    var semester_2_project = $('#semester-2-project').val();
                    var semester_3 = $('#project_name_1').val();
                    var semester_4 = $('#project_name_2').val();

                    var skill_1 = $('input[name="soft_skill_1[]"]').map(function () {
                        return $(this).val()
                    }).get();
                    var credit_1 = $('select[name="credit_1[]"]').map(function () {
                        return $(this).val()
                    }).get();
                    var skill_2 = $('input[name="soft_skill_2[]"]').map(function () {
                        return $(this).val()
                    }).get();
                    var credit_2 = $('select[name="credit_2[]"]').map(function () {
                        return $(this).val()
                    }).get();
                    var skill_3 = $('input[name="soft_skill_3[]"]').map(function () {
                        return $(this).val()
                    }).get();
                    var credit_3 = $('select[name="credit_3[]"]').map(function () {
                        return $(this).val()
                    }).get();

                    //Credit Calculation               
                    if (active_tab == 2) {
                        var total_credit = 0;
                        //For Semester 1, 2, 3
                        var semester_1_local = localStorage.getItem('semester_1');
                        if(!semester_1_local) 
                            semester_1_local = JSON.stringify([]);
                        var semester_2_local = localStorage.getItem('semester_2');
                        if(!semester_2_local) 
                            semester_2_local = JSON.stringify([]);
                        modules_1_local = localStorage.getItem('modules_1');
                        modules_2_local = localStorage.getItem('modules_2');
                        
                        //var sem_1_res = creditCalculation(JSON.parse(modules_1_local), JSON.parse(semester_1_local));
                        // if (sem_1_res)
                        //var sem_2_res = creditCalculation(JSON.parse(modules_2_local), JSON.parse(semester_2_local));
                        
                        var sem_1_res = creditCalculation(modules_1, semester_1);
                        var sem_2_res = creditCalculation(modules_2, semester_2);
                        
                        // console.log(sem_1_res):
                        //console.log(sem_2_res);
                        // console.log(semester_2);
                        var total_1 = 0;
                        var total_2 = 0;
                        for (var i = 0; i < credit_1.length; i++) {
                            total_1 += credit_1[i] << 0;
                        }
                        for (var i = 0; i < credit_2.length; i++) {
                            total_2 += credit_2[i] << 0;
                        }
                        total_credit = sem_1_res + sem_2_res + total_1 + total_2;
                        console.log(total_credit);
                        if (total_credit >= 60) {
                            $('.js-alert-danger').show().text('The courses are optional for you in this semester, because you have already fulfilled 60 credits.Now your credit is: ' + total_credit + ' credit and you can go to your next step...');
                        } else {
                            $('.js-alert-danger').show().css({'background': '#DFF0CD'}).text('Now your credit is: ' + total_credit + ' credit and you can go to your next step...');
                            //$('.js-alert-danger').hide();
                        }
                        $("html, body").animate({scrollTop: 0}, '300', 'swing', function () {
                        });
                    }
                    if (active_tab == 4) {
                        categories_1_total_credit = 0;
                        categories_2_total_credit = 0;
                        categories_3_total_credit = 0;
                        categories_4_total_credit = 0;
                        $('.js-plan-details').empty();
                        $('.js-plan-details').append('<tbody><tr class="style6"><th class="text-center category">Semester</th><th colspan="5" class="text-center category">Core Areas (40 C – 50 C)</th><th colspan="2" class="text-center category">Research Focus (45 C – 50 C)</th><th class="text-center category">Master Theses (30 C)</th></tr><tr><td></td><td class="category">Theoretical Foundations(min. 10 C)</td><td class="category">Data Information(min. 10 C)</td><td class="category">Networking and Communication(min. 10 C)</td><td class="category">Business and Law(min. 5 C)</td><td class="category">Soft Skills( 5 C)</td><td class="category">Additional Modules on Research Focus(15 C – 20 C)</td><td class="category">Individual Research Project(s)(30 C)</td><td class="category">Master Theses (30 C)</td></tr></tbody>');

                        //For Semester 1, 2, 3
                        var semester_1_local = localStorage.getItem('semester_1');
                        if(!semester_1_local) 
                            semester_1_local = JSON.stringify([]);
                        var semester_2_local = localStorage.getItem('semester_2');
                        if(!semester_2_local) 
                            semester_2_local = JSON.stringify([]);
                        var semester_3_local = localStorage.getItem('semester_3');
                        if(!semester_3_local) 
                            semester_3_local = JSON.stringify([]);
                        //console.log(semester_1_local,semester_2_local,semester_3_local);

                        modules_1_local = localStorage.getItem('modules_1');
                        modules_2_local = localStorage.getItem('modules_2');

                        //createTable(JSON.parse(modules_1_local), JSON.parse(semester_1_local), 1, '', skill_1, credit_1);
                        //createTable(JSON.parse(modules_2_local), JSON.parse(semester_2_local), 2, semester_2_project, skill_2, credit_2);
                        //createTable(JSON.parse(modules_1_local), JSON.parse(semester_3_local), 3, semester_3, skill_3, credit_3);

                        createTable(modules_1, semester_1, 1, '', skill_1, credit_1);
                        createTable(modules_2, semester_2, 2, semester_2_project, skill_2, credit_2);
                        createTable(modules_1, semester_3_course, 3, semester_3, skill_3, credit_3);


                        var Thesis_Title = '';
                        var soft_skill = '';
                        if (semester_4.length) Thesis_Title = '<strong>Theses Title: </strong>' + semester_4;
                        $('.js-plan-details').append('<tr><th class="category">4.Semester</th><td class="category" colspan="7"></td><td class="category">' + Thesis_Title + '</td></tr>');
                    }
                }
            } else {
                $('.js-alert-danger').fadeIn(400).text('Please select any course.');
            }
        } else {
            if (active_tab == 5) {
                //console.log(categories_2_total_credit)
               // if (categories_1_total_credit >= 10 && categories_2_total_credit >= 10 && categories_3_total_credit >= 10 && categories_4_total_credit >= 5) {
                    $('.overlay').show();
                    /*$('.js-nav li#tab_' + active_tab).removeClass('active').addClass('disabledTab');
                    $('.js-nav li#tab_' + next_tab).removeClass('disabledTab').addClass('active');
                    $('.my-tab #' + edit_handle[active_tab]).removeClass('active');
                    $('.my-tab #' + edit_handle[next_tab]).addClass('active');*/
                    //Save all step data into DB
                    var semester_1_courses = [];
                    semester_1_courses = $('#course_1:checked').map(function (_, el) {
                        //return $(el).val();
                        return $(el).closest('td').attr('rel');         //return course id
                    }).get();
                    var semester_2_courses = [];
                    semester_2_courses = $('#course_2:checked').map(function (_, el) {
                        //return $(el).val();
                        return $(el).closest('td').attr('rel');
                    }).get();
                    var semester_3_courses = [];
                    semester_3_courses = $('#course_3:checked').map(function (_, el) {
                        //return $(el).val();
                        return $(el).closest('td').attr('rel');
                    }).get();

                    //Get Course credits array
                    var credits_1 = [];
                    credits_1 = $('#course_1:checked').map(function (_, el) {
                        return $(el).closest('td').next('td').attr('rel');
                    }).get();
                    var credits_2 = [];
                    credits_2 = $('#course_2:checked').map(function (_, el) {
                        return $(el).closest('td').next('td').attr('rel');
                    }).get();
                    var credits_3 = [];
                    credits_3 = $('#course_3:checked').map(function (_, el) {
                        return $(el).closest('td').next('td').attr('rel');
                    }).get();
                    //Get semester
                    var sem_1_ses = $('#sem_1_ses').val();
                    var sem_2_ses = $('#sem_2_ses').val();
                    var sem_3_ses = $('#sem_3_ses').val();
                    //project or these
                    var semester_2_project = $('#semester-2-project').val();
                    var semester_3 = $('#project_name_1').val();
                    var semester_4 = $('#project_name_2').val();

                    //Soft skill & her credit
                    var skill_1 = $('input[name="soft_skill_1[]"]').map(function () {
                        return $(this).val()
                    }).get();
                    var credit_1 = $('select[name="credit_1[]"]').map(function () {
                        return $(this).val()
                    }).get();
                    var site_1 = $('select[name="site_1[]"]').map(function () {
                        return $(this).val()
                    }).get();
                    var skill_2 = $('input[name="soft_skill_2[]"]').map(function () {
                        return $(this).val()
                    }).get();
                    var credit_2 = $('select[name="credit_2[]"]').map(function () {
                        return $(this).val()
                    }).get();
                    var site_2 = $('select[name="site_2[]"]').map(function () {
                        return $(this).val()
                    }).get();
                    var skill_3 = $('input[name="soft_skill_3[]"]').map(function () {
                        return $(this).val()
                    }).get();
                    var credit_3 = $('select[name="credit_3[]"]').map(function () {
                        return $(this).val()
                    }).get();
                    var site_3 = $('select[name="site_3[]"]').map(function () {
                        return $(this).val()
                    }).get();

                    /*var semester_1_local = localStorage.getItem('semester_1');
                    if(!semester_1_local) 
                        semester_1_local = JSON.stringify([]);
                    var semester_2_local = localStorage.getItem('semester_2');
                    if(!semester_2_local) 
                        semester_2_local = JSON.stringify([]);
                    var semester_3_local = localStorage.getItem('semester_3');
                    if(!semester_3_local) 
                        semester_3_local = JSON.stringify([]);

                    var credits_1_local = localStorage.getItem('credits_1');
                    if(!credits_1_local) 
                        credits_1_local = JSON.stringify([]);
                    var credits_2_local = localStorage.getItem('credits_2');
                    if(!credits_2_local) 
                        credits_2_local = JSON.stringify([]);
                    var credits_3_local = localStorage.getItem('credits_3');
                    if(!credits_3_local) 
                        credits_3_local = JSON.stringify([]);
                    localStorage.clear();*/

                    //Send all data to server
                    $.post('coursestore',
                        {
                            /*semester_1_courses: JSON.parse(semester_1_local),
                            semester_2_courses: JSON.parse(semester_2_local),
                            semester_3_courses: JSON.parse(semester_3_local),
                            credits_1: JSON.parse(credits_1_local),
                            credits_2: JSON.parse(credits_2_local),
                            credits_3: JSON.parse(credits_3_local),*/
                            semester_1_courses: semester_1_courses,
                            semester_2_courses: semester_2_courses,
                            semester_3_courses: semester_3_courses,
                            credits_1: credits_1,
                            credits_2: credits_2,
                            credits_3: credits_3,
                            "sem_1_session": sem_1_ses,
                            "sem_2_session": sem_2_ses,
                            "sem_3_session": sem_3_ses,
                            "semester_2_project": semester_2_project,
                            "semester_3": semester_3,
                            "semester_4": semester_4,
                            skill_1: skill_1,
                            skill_2: skill_2,
                            skill_3: skill_3,
                            credit_1: credit_1,
                            credit_2: credit_2,
                            credit_3: credit_3,
                            site_1: site_1,
                            site_2: site_2,
                            site_3: site_3
                        }, function (response) {
                            //console.log(response)
                            $('.overlay').hide();
                            if (response.success) {
                                //$('.js-alert-danger').show().css({'background': '#DFF0CD'}).text('Successfully saved!. Please contact with administration for selecting your mentor name.')
                                $('.js-back-btn').hide();
                                $('.js-alert-danger').show().removeClass('alert-danger').addClass('alert-success').css({'background': '#AEDB43','padding': '6px'}).html('<span class="glyphicon glyphicon-ok"></span><strong> Success</strong></br>Successfully saved! :)');
                                 $("html, body").animate({scrollTop: 0}, '300', 'swing', function () {
                                    });
                                /*setTimeout(function(){
                                    location.reload();
                                }, 3000);*/
                            }
                        }, 'json');
               /* } else{
                    $('.js-alert-danger').show().css({'background-color': '#f2dede'}).text('You\'ve to complete minimum 10 credits against respectively Theoretical Foundations, Data Information, Networking and Communication categories and 5 credits against Business and Law categories.');
                    $("html, body").animate({scrollTop: 0}, '300', 'swing', function () {
                    });
                    return false;
                }*/
            }
        }
    });
    
    // Add & remove soft skill: 3.6.15
    $('.js-add-new-btn-1,.js-add-new-btn-2,.js-add-new-btn-3').on('click', function () {
        var addRemv4 = $(this).attr('class').split(' ').pop().split('-').pop();
        //console.log($(this).attr('class').split(' ').pop().split('-').pop());
        $('.js-soft-skill-' + addRemv4).append($('.js-single-skill-' + addRemv4 + ':last').clone());    //create copy of first elem.
        $('.js-add-new-btn-' + addRemv4).text('Remove').removeClass('js-add-new-btn-' + addRemv4).addClass('js-remove-btn-' + addRemv4);
        $('input[name="soft_skill_' + addRemv4 + '[]"]').map(function () {
            return $('input[name="soft_skill_' + addRemv4 + '[]"]:last').val('');           
        });
        $('select[name="credit_' + addRemv4 + '[]"]').map(function () {            
            return $('select[name="credit_' + addRemv4 + '[]"]:last option').prop('selected', false);
        });
         $('select[name="site_' + addRemv4 + '[]"]').map(function () {            
            return $('select[name="site_' + addRemv4 + '[]"]:last option').prop('selected', false);
        });
        $(this).text('Add').removeClass('js-remove-btn-' + addRemv4).addClass('js-add-new-btn-' + addRemv4);
    });

    for (var i = 1; i <= 3; i++) {
        $('.js-soft-skill-' + i).on('click', '.js-remove-btn-' + i, function () {
            $(this).parent().parent().remove();
        });
    }

    //Handle skip or next
    $('#project_name_1, #project_name_2').on('keyup', function () {
        if ($('#project_name_1').val().length) {
            $('#js-step-3').text('Next');
        } else {
            $('#js-step-3').text('Skip');
        }
        // For step 4
        if ($('#project_name_2').val().length) {
            $('#js-step-4').text('Next');
        } else {
            $('#js-step-4').text('Skip');
        }
    });
    
    //functionality of back button
    $('.js-back-btn').on('click', function () {        
        $('.js-alert-danger').hide();
        var active_tab = $(".js-nav li.active").prop('id').split('_').pop();
        select_from_local(active_tab - 1);
        disable_next_btn(active_tab - 1);
        $('li#tab_' + active_tab).removeClass('active').addClass('disabledTab');
        $('li#tab_' + parseInt(active_tab - 1)).addClass('active');
        $('.my-tab #' + edit_handle[active_tab]).removeClass('active');
        $('.my-tab #' + edit_handle[parseInt(active_tab - 1)]).addClass('active');
    });

    $('input[name="course"]').on('change', function () {
        if ($(this).is(':checked')) {
            $(this).parent().parent().removeClass('info').addClass('success');
        } else {
            $(this).parent().parent().removeClass('success').addClass('info');
        }
    });

    function creditCalculation(modules_1, semester_1) {
        var result = 0;
        $.each(semester_1, function (key, item) {
            var data = _.findWhere(modules_1, { id: parseInt(item)});
            //   console.log(data.credits);
            //console.log(data);
            if (!$.isEmptyObject(data)) {
                var credit_initial = data.credits ? data.credits : 0;
                result = result + credit_initial;
                //  console.log(credit_initial);
                // console.log(result);

            }
            //var credit_initial=data.credits ? data.credits :0;
            //result +=credit_initial;

        });
        return result;
    }

    function convertSemesterName(value) {   
        switch(value) {
            case 'SoSe 2012':
                return '2012 SS';
            case 'WiSe 2012/13':
                return '2012 WS';
            case 'SoSe 2013':
                return '2013 SS';
            case 'WiSe 2013/14':
                return '2013 WS';
            case 'SoSe 2014':
                return '2014 SS';
            case 'WiSe 2014/15':
                return '2014 WS';
            case 'SoSe 2015':
                return '2015 SS';
            case 'WiSe 2015/16':
                return '2015 WS';
            case 'SoSe 2016':
                return '2016 SS';
            case 'WiSe 2016/17':
                return '2016 WS';
            case 'SoSe 2017':
                return '2017 SS';
            case ' WiSe 2017/18':
                return '2017 WS';
            case 'SoSe 2018':
                return '2018 SS';
            case 'WiSe 2018/19':
                return '2018 WS';
        }
    }

    /**
     * Create Dynamic plan details table which is called from 53,54,55 line
     * @param modules_1
     * @param semester_1
     * @param sem_no
     * @param project
     */
    function createTable(modules_1, semester_1, sem_no, project, skill, credit) {
        console.log(semester_1);
        var result_1 = [];
        $.each(semester_1, function (key, item) {
            var data = _.findWhere(modules_1, { id: parseInt(item)});
            result_1.push(data);
        });
        //console.log(modules_1);
        // Divide the data categories in order to display right place using underscore.js function
        var cat_1 = _.where(result_1, {topic_category: '1'});
        var cat_2 = _.where(result_1, {topic_category: '2'});
        var cat_3 = _.where(result_1, {topic_category: '3'});
        var cat_4 = _.where(result_1, {topic_category: '4'});
        //console.log(cat_1);
        var html_1 = '';
        var html_2 = '';
        var html_3 = '';
        var html_4 = '';
        var str_1 = '';
        var str_2 = '';
        var str_3 = '';
        var str_4 = '';
        $.each(cat_1, function (key, item_1) {
            categories_1_total_credit = categories_1_total_credit + item_1.credits;
            var hr = '';
            if (key < cat_1.length - 1)
                var hr = '<hr>';
            var module_name = item_1.module.match(/.{1,40}/g).join('-<br>');
            var course_name = item_1.course.match(/.{1,40}/g).join('-<br>');
            var semester = convertSemesterName(item_1.semester);
            //console.log(item_1.semester);
            str_1 += '<strong>Session: </strong>' + semester + '</br><strong>Course Name: </strong>' + course_name + '</br><strong>Exam Id: </strong>' + item_1.moduleexamid + '</br><strong>Module Name: </strong>' + module_name + '</br><strong>Credits: </strong>' + item_1.credits + hr;
        });
        html_1 = '<td class="category">' + str_1 + '</td>';

        $.each(cat_2, function (key, item_1) {
            categories_2_total_credit = categories_2_total_credit + item_1.credits;
            var hr = '';
            if (key < cat_2.length - 1)
                var hr = '<hr>';
            var module_name = item_1.module.match(/.{1,40}/g).join('-<br>');
            var course_name = item_1.course.match(/.{1,40}/g).join('-<br>');
            var semester = convertSemesterName(item_1.semester);
            str_2 += '<strong>Session: </strong>' + semester + '</br><strong>Course Name: </strong>' + course_name + '</br><strong>Exam Id: </strong>' + item_1.moduleexamid + '</br><strong>Module Name: </strong>' + module_name + '</br><strong>Credits: </strong>' + item_1.credits + hr;
        });
        html_2 = '<td class="category">' + str_2 + '</td>';

        $.each(cat_3, function (key, item_1) {
            categories_3_total_credit = categories_3_total_credit + item_1.credits;
            var hr = '';
            if (key < cat_3.length - 1)
                var hr = '<hr>';
            var module_name = item_1.module.match(/.{1,40}/g).join('-<br>');
            var course_name = item_1.course.match(/.{1,40}/g).join('-<br>');
            var semester = convertSemesterName(item_1.semester);
            str_3 += '<strong>Session: </strong>' + semester + '</br><strong>Course Name: </strong>' + course_name + '</br><strong>Exam Id: </strong>' + item_1.moduleexamid + '</br><strong>Module Name: </strong>' + module_name + '</br><strong>Credits: </strong>' + item_1.credits + hr;
        });
        html_3 = '<td class="category">' + str_3 + '</td>';

        $.each(cat_4, function (key, item_1) {
            categories_4_total_credit = categories_4_total_credit + item_1.credits;
            var hr = '';
            if (key < cat_4.length - 1)
                var hr = '<hr>';
            var module_name = item_1.module.match(/.{1,40}/g).join('-<br>');
            var course_name = item_1.course.match(/.{1,40}/g).join('-<br>');
            var semester = convertSemesterName(item_1.semester);
            str_4 += '<strong>Session: </strong>' + semester + '</br><strong>Course Name: </strong>' + course_name + '</br><strong>Exam Id: </strong>' + item_1.moduleexamid + '</br><strong>Module Name: </strong>' + module_name + '</br><strong>Credits: </strong>' + item_1.credits + hr;
        });
        html_4 = '<td class="category">' + str_4 + '</td>';
        var skills = [];
        if (skill.length > 0 && credit.length > 0) {
            $.each(skill, function (key, skill) {
                if (skill.length) {
                    skills.push('<strong>Course Name: </strong>' + skill + '</br><strong>Credit: </strong>' + credit[key]);
                }
            });
        }
        var soft_skill = skills.join('<hr>');
        var project_Title = '';
        if (project.length)  project_Title = '<strong>Project Title: </strong>' + project;
        $('.js-plan-details').append('<tr><th class="category">' + sem_no + '.Semester</th>' + html_1 + html_2 + html_3 + html_4 + '<td class="category">' + soft_skill + '</td><td class="category"></td><td class="category">' + project_Title + '</td><td class="category"></td></tr>');
    }

    // plan details for superadmin
    $('#tab_5').on('click', function(){
        console.log(semester_1_local);
        if(!$(this).hasClass('disabledTab')){
            categories_1_total_credit = 0;
            categories_2_total_credit = 0;
            categories_3_total_credit = 0;
            categories_4_total_credit = 0;
            $('.js-plan-details').empty();
            $('.js-plan-details').append('<tbody><tr class="style6"><th class="text-center category">Semester</th><th colspan="5" class="text-center category">Core Areas (40 C – 50 C)</th><th colspan="2" class="text-center category">Research Focus (45 C – 50 C)</th><th class="text-center category">Master Theses (30 C)</th></tr><tr><td></td><td class="category">Theoretical Foundations(min. 10 C)</td><td class="category">Data Information(min. 10 C)</td><td class="category">Networking and Communication(min. 10 C)</td><td class="category">Business and Law(min. 5 C)</td><td class="category">Soft Skills( 5 C)</td><td class="category">Additional Modules on Research Focus(15 C – 20 C)</td><td class="category">Individual Research Project(s)(30 C)</td><td class="category">Master Theses (30 C)</td></tr></tbody>');

            var semester_1_local = localStorage.getItem('semester_1');
            if(!semester_1_local) 
                semester_1_local = JSON.stringify([]);
            var semester_2_local = localStorage.getItem('semester_2');
            if(!semester_2_local) 
                semester_2_local = JSON.stringify([]);
            var semester_3_local = localStorage.getItem('semester_3');
            if(!semester_3_local) 
                semester_3_local = JSON.stringify([]);
            modules_1_local = localStorage.getItem('modules_1');
            modules_2_local = localStorage.getItem('modules_2');

            /*createTable(JSON.parse(modules_1_local), JSON.parse(semester_1_local), 1, '', skill_1, credit_1);
            createTable(JSON.parse(modules_2_local), JSON.parse(semester_2_local), 2, semester_2_project, skill_2, credit_2);
            createTable(JSON.parse(modules_1_local), JSON.parse(semester_3_local), 3, semester_3, skill_3, credit_3);*/

            createTable(modules_all, JSON.parse(semester_1_local), 1, '', skill_1, credit_1);
            createTable(modules_all, JSON.parse(semester_2_local), 2, semester_2_project, skill_2, credit_2);
            createTable(modules_all, JSON.parse(semester_3_local), 3, semester_3, skill_3, credit_3);                       

            var Thesis_Title = '';
            var soft_skill = '';
            if (semester_4.length) Thesis_Title = '<strong>Theses Title: </strong>' + semester_4;
            $('.js-plan-details').append('<tr><th class="category">4.Semester</th><td class="category" colspan="7"></td><td class="category">' + Thesis_Title + '</td></tr>');
        }
    });

    //console.log(user_id) 
    if(user_id && user_id!='studyplan'){
        //localStorage.clear();
        console.log('dsds',user_id)       
        $('.js-plan-details').empty();
        $('.js-plan-details').append('<tbody><tr class="style6"><th class="text-center category">Semester</th><th colspan="5" class="text-center category">Core Areas (40 C – 50 C)</th><th colspan="2" class="text-center category">Research Focus (45 C – 50 C)</th><th class="text-center category">Master Theses (30 C)</th></tr><tr><td></td><td class="category">Theoretical Foundations(min. 10 C)</td><td class="category">Data Information(min. 10 C)</td><td class="category">Networking and Communication(min. 10 C)</td><td class="category">Business and Law(min. 5 C)</td><td class="category">Soft Skills( 5 C)</td><td class="category">Additional Modules on Research Focus(15 C – 20 C)</td><td class="category">Individual Research Project(s)(30 C)</td><td class="category">Master Theses (30 C)</td></tr></tbody>');

        //For Semester 1, 2, 3
        //console.log(modules_2)

        var semester_1_local = localStorage.getItem('semester_1');
        console.log(semester_1_local)
        if(!semester_1_local) 
            semester_1_local = JSON.stringify([]);
        var semester_2_local = localStorage.getItem('semester_2');
        if(!semester_2_local) 
            semester_2_local = JSON.stringify([]);
        var semester_3_local = localStorage.getItem('semester_3');
        if(!semester_3_local) 
            semester_3_local = JSON.stringify([]);
        modules_1_local = localStorage.getItem('modules_1');
        modules_2_local = localStorage.getItem('modules_2');

        createTable(JSON.parse(modules_1_local), JSON.parse(semester_1_local), 1, '', skill_1, credit_1);
        createTable(JSON.parse(modules_2_local), JSON.parse(semester_2_local), 2, semester_2_project, skill_2, credit_2);
        createTable(JSON.parse(modules_1_local), JSON.parse(semester_3_local), 3, semester_3, skill_3, credit_3);

        localStorage.clear();

        var Thesis_Title = '';
        if (semester_4.length) Thesis_Title = '<strong>Theses Title: </strong>' + semester_4;
        $('.js-plan-details').append('<tr><th class="category">4.Semester</th><td class="category" colspan="7"></td><td class="category">' + Thesis_Title + '</td></tr>');
    }


    //Edited by super admin per individual semester
    $('#js-edit-1, #js-edit-2, #js-edit-3, #js-edit-4, #js-edit-5').on('click', function(){
        var semester_no = $(this).prop('id').split('-').pop();
        //select_from_local(semester_no);
        var user_id = location.href.split('/').pop();   //get user id from url
        var data = {
            user_id : user_id,
            semester: semester_no
        };
        if(semester_no == 4) {
            semester_4 = $('#project_name_2').val();
            $.extend(data, {semester_4:semester_4});

        } else if(semester_no == 5) {
            var mentor_name = $("#mentor").val();
            $.extend(data, {mentor : mentor_name});
        }
        else {
            // send ajax request to server for editing data
            var courses = $('#course_' + semester_no + ':checked').map(function (_, el) {
                //return $(el).val();
                return $(el).closest('td').attr('rel');
            }).get();

            var credits = $('#course_'+semester_no+':checked').map(function (_, el) {
                return $(el).closest('td').next('td').attr('rel');
            }).get();

            var soft_skills = $('input[name="soft_skill_'+semester_no+'[]"]').map(function () {
                return $(this).val()
            }).get();
            var soft_credits = $('select[name="credit_'+semester_no+'[]"]').map(function () {
                return $(this).val()
            }).get();
            var soft_sites = $('select[name="site_'+semester_no+'[]"]').map(function () {
                return $(this).val()
            }).get();
            var session = $('#sem_'+semester_no+'_ses').val();

            //project or theses
            semester_2_project = $('#semester-2-project').val();
            semester_3 = $('#project_name_1').val();
            $.extend(data,{
                'courses' : courses,
                'credits' : credits,
                'soft_skills' : soft_skills,
                'soft_credits' : soft_credits,
                'soft_sites' : soft_sites,
                'session' : session,
                "semester_2_project": semester_2_project,
                "semester_3": semester_3
            });
        }
        //console.log(data);
        $.post(ROOT + 'editcourse', data, function(res){
            if(res.success){
                $('.js-alert-info').hide();
                $('.js-alert-danger').show().removeClass('alert-danger').addClass('alert-success').css({'background': '#AEDB43','padding': '6px'}).html('<span class="glyphicon glyphicon-ok"></span><strong> Success</strong></br>Successfully Updated :)');
                $("html, body").animate({scrollTop: 0}, '300', 'swing', function () {
                });
            }
        }, 'json');
    });

	//alert msg hide
	$('.js-nav li').on('click', function(){
        active_tab = $(this).prop('id').split('_').pop();
        select_from_local(active_tab);
		if(!$(this).hasClass('active'))
			$('.js-alert-danger').hide();
	});

    //All course Show/hide func. for one semester
    $('.all-course-btn').on('click', function(){
        $('.js-alert-danger').hide();
        var _this = $(this);
        var user_id = location.pathname.split('/').pop();
        var semester = $(this).prev().prop('name').split('_').pop();
        $.post(ROOT+'getlatestcourses',{user_id : user_id, semester:semester}, function(res){
            //console.log(res)
            _this.closest('.course_menu').hide();
            _this.closest('.course_menu').next('.selected-course-list').html(res).show('fast');
        },'json');
    });

    $('body').on('click','.js-back-course', function(){
        $(this).closest('.selected-course-list').hide();
        $(this).closest('.selected-course-list').prev('.course_menu').show('fast');
    });
    
    /*    
        // Done by Thevarajah Thishanth
        // $("input[name$='noOfSemester']").click(function() {
        //     var test = $(this).val();
        //     $("div.desc").hide();
        //     $("#noOfSemester" + test).show();
        // });

        // $(function() {
        //   $( "#datepickerStart" ).datepicker({ dateFormat: 'yy/mm/dd' }).val();
        // });

        // $(function() {
        //   $( "#datepickerEnd" ).datepicker({ dateFormat: 'yy/mm/dd' }).val();
        // });
    */

});